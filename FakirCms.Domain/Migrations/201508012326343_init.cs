namespace FakirCms.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HaberCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 70, storeType: "nvarchar"),
                        Alias = c.String(nullable: false, maxLength: 70, storeType: "nvarchar"),
                        Description = c.String(maxLength: 160, storeType: "nvarchar"),
                        Tags = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Alias, unique: true);
            
            CreateTable(
                "dbo.Habers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, unicode: false),
                        Alias = c.String(nullable: false, unicode: false),
                        IntroText = c.String(unicode: false),
                        FullText = c.String(unicode: false),
                        Date = c.DateTime(nullable: false, precision: 0),
                        State = c.Boolean(nullable: false),
                        Hits = c.Int(nullable: false),
                        City = c.String(unicode: false),
                        Town = c.String(unicode: false),
                        ImageUrl = c.String(nullable: false, unicode: false),
                        HaberType = c.String(unicode: false),
                        Updated = c.DateTime(nullable: false, precision: 0),
                        Created = c.DateTime(nullable: false, precision: 0),
                        ShowManset = c.Boolean(nullable: false),
                        ShowCategoryManset = c.Boolean(nullable: false),
                        SpotBigTittle = c.String(maxLength: 70, storeType: "nvarchar"),
                        SpotSmallTitle = c.String(maxLength: 90, storeType: "nvarchar"),
                        MetaTitle = c.String(maxLength: 70, storeType: "nvarchar"),
                        MetaDescription = c.String(maxLength: 160, storeType: "nvarchar"),
                        MetaKeywords = c.String(unicode: false),
                        CreatedByID = c.String(maxLength: 128, storeType: "nvarchar"),
                        UpdatedByID = c.String(maxLength: 128, storeType: "nvarchar"),
                        HaberCategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedByID)
                .ForeignKey("dbo.HaberCategories", t => t.HaberCategoryID)
                .ForeignKey("dbo.Users", t => t.UpdatedByID)
                .Index(t => t.CreatedByID)
                .Index(t => t.UpdatedByID)
                .Index(t => t.HaberCategoryID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        RegisterDate = c.DateTime(nullable: false, precision: 0),
                        LastVisitDate = c.DateTime(nullable: false, precision: 0),
                        Active = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256, storeType: "nvarchar"),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(unicode: false),
                        SecurityStamp = c.String(unicode: false),
                        PhoneNumber = c.String(unicode: false),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(precision: 0),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(maxLength: 25, storeType: "nvarchar"),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128, storeType: "nvarchar"),
                        ClaimType = c.String(unicode: false),
                        ClaimValue = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        ProviderKey = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Name = c.String(maxLength: 256, storeType: "nvarchar"),
                        Discriminator = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagId = c.Int(nullable: false, identity: true),
                        Name = c.String(unicode: false),
                        Alias = c.String(maxLength: 70, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.TagId)
                .Index(t => t.Alias, unique: true);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(unicode: false),
                        Alias = c.String(unicode: false),
                        IntroText = c.String(unicode: false),
                        FullText = c.String(unicode: false),
                        State = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                        MetaData = c.String(unicode: false),
                        Hits = c.Int(nullable: false),
                        Access = c.Int(nullable: false),
                        Updated = c.DateTime(nullable: false, precision: 0),
                        Created = c.DateTime(nullable: false, precision: 0),
                        CreatedByID = c.String(maxLength: 128, storeType: "nvarchar"),
                        UpdatedByID = c.String(maxLength: 128, storeType: "nvarchar"),
                        CategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedByID)
                .ForeignKey("dbo.Users", t => t.UpdatedByID)
                .Index(t => t.CreatedByID)
                .Index(t => t.UpdatedByID);
            
            CreateTable(
                "dbo.Pages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 70, storeType: "nvarchar"),
                        Alias = c.String(nullable: false, maxLength: 70, storeType: "nvarchar"),
                        Content = c.String(unicode: false),
                        Updated = c.DateTime(nullable: false, precision: 0),
                        Created = c.DateTime(nullable: false, precision: 0),
                        Hits = c.Int(nullable: false),
                        State = c.Boolean(nullable: false),
                        MetaTitle = c.String(maxLength: 70, storeType: "nvarchar"),
                        MetaDescription = c.String(maxLength: 160, storeType: "nvarchar"),
                        MetaKeywords = c.String(unicode: false),
                        CreatedByID = c.String(maxLength: 128, storeType: "nvarchar"),
                        UpdatedByID = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedByID)
                .ForeignKey("dbo.Users", t => t.UpdatedByID)
                .Index(t => t.Alias, unique: true)
                .Index(t => t.CreatedByID)
                .Index(t => t.UpdatedByID);
            
            CreateTable(
                "dbo.Sections",
                c => new
                    {
                        SectionID = c.Int(nullable: false, identity: true),
                        Title = c.String(unicode: false),
                        Name = c.String(unicode: false),
                        Alias = c.String(unicode: false),
                        Description = c.String(unicode: false),
                        Order = c.Int(nullable: false),
                        State = c.Boolean(nullable: false),
                        SectionAccess = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SectionID);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        RoleId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.post_tags",
                c => new
                    {
                        PostId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostId, t.TagId })
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.PostId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.haber_tags",
                c => new
                    {
                        HaberId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.HaberId, t.TagId })
                .ForeignKey("dbo.Habers", t => t.HaberId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.HaberId)
                .Index(t => t.TagId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pages", "UpdatedByID", "dbo.Users");
            DropForeignKey("dbo.Pages", "CreatedByID", "dbo.Users");
            DropForeignKey("dbo.Habers", "UpdatedByID", "dbo.Users");
            DropForeignKey("dbo.haber_tags", "TagId", "dbo.Tags");
            DropForeignKey("dbo.haber_tags", "HaberId", "dbo.Habers");
            DropForeignKey("dbo.Posts", "UpdatedByID", "dbo.Users");
            DropForeignKey("dbo.post_tags", "TagId", "dbo.Tags");
            DropForeignKey("dbo.post_tags", "PostId", "dbo.Posts");
            DropForeignKey("dbo.Posts", "CreatedByID", "dbo.Users");
            DropForeignKey("dbo.Habers", "HaberCategoryID", "dbo.HaberCategories");
            DropForeignKey("dbo.Habers", "CreatedByID", "dbo.Users");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.Users");
            DropIndex("dbo.haber_tags", new[] { "TagId" });
            DropIndex("dbo.haber_tags", new[] { "HaberId" });
            DropIndex("dbo.post_tags", new[] { "TagId" });
            DropIndex("dbo.post_tags", new[] { "PostId" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.Pages", new[] { "UpdatedByID" });
            DropIndex("dbo.Pages", new[] { "CreatedByID" });
            DropIndex("dbo.Pages", new[] { "Alias" });
            DropIndex("dbo.Posts", new[] { "UpdatedByID" });
            DropIndex("dbo.Posts", new[] { "CreatedByID" });
            DropIndex("dbo.Tags", new[] { "Alias" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.Habers", new[] { "HaberCategoryID" });
            DropIndex("dbo.Habers", new[] { "UpdatedByID" });
            DropIndex("dbo.Habers", new[] { "CreatedByID" });
            DropIndex("dbo.HaberCategories", new[] { "Alias" });
            DropTable("dbo.haber_tags");
            DropTable("dbo.post_tags");
            DropTable("dbo.UserRole");
            DropTable("dbo.Sections");
            DropTable("dbo.Pages");
            DropTable("dbo.Posts");
            DropTable("dbo.Tags");
            DropTable("dbo.Role");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.Users");
            DropTable("dbo.Habers");
            DropTable("dbo.HaberCategories");
        }
    }
}

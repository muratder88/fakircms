namespace FakirCms.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPageColumnCSSandJS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pages", "CustomCss", c => c.String(unicode: false));
            AddColumn("dbo.Pages", "CustomJs", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pages", "CustomJs");
            DropColumn("dbo.Pages", "CustomCss");
        }
    }
}

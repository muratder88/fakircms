namespace FakirCms.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddArticleDateColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "Updated", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.Articles", "Created", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Articles", "Created");
            DropColumn("dbo.Articles", "Updated");
        }
    }
}

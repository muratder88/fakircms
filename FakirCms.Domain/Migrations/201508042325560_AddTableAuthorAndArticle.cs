namespace FakirCms.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableAuthorAndArticle : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, unicode: false),
                        Alias = c.String(nullable: false, unicode: false),
                        Content = c.String(nullable: false, unicode: false),
                        Date = c.DateTime(nullable: false, precision: 0),
                        Hits = c.Int(nullable: false),
                        MetaTitle = c.String(maxLength: 70, storeType: "nvarchar"),
                        MetaDescription = c.String(maxLength: 160, storeType: "nvarchar"),
                        MetaKeywords = c.String(unicode: false),
                        CreatedByID = c.String(maxLength: 128, storeType: "nvarchar"),
                        UpdatedByID = c.String(maxLength: 128, storeType: "nvarchar"),
                        AuthorID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Authors", t => t.AuthorID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.CreatedByID)
                .ForeignKey("dbo.Users", t => t.UpdatedByID)
                .Index(t => t.CreatedByID)
                .Index(t => t.UpdatedByID)
                .Index(t => t.AuthorID);
            
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, unicode: false),
                        Alias = c.String(nullable: false, unicode: false),
                        Description = c.String(unicode: false),
                        Email = c.String(unicode: false),
                        Image = c.String(nullable: false, unicode: false),
                        Updated = c.DateTime(nullable: false, precision: 0),
                        Created = c.DateTime(nullable: false, precision: 0),
                        MetaTitle = c.String(maxLength: 70, storeType: "nvarchar"),
                        MetaDescription = c.String(maxLength: 160, storeType: "nvarchar"),
                        MetaKeywords = c.String(unicode: false),
                        CreatedByID = c.String(maxLength: 128, storeType: "nvarchar"),
                        UpdatedByID = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CreatedByID)
                .ForeignKey("dbo.Users", t => t.UpdatedByID)
                .Index(t => t.CreatedByID)
                .Index(t => t.UpdatedByID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articles", "UpdatedByID", "dbo.Users");
            DropForeignKey("dbo.Articles", "CreatedByID", "dbo.Users");
            DropForeignKey("dbo.Authors", "UpdatedByID", "dbo.Users");
            DropForeignKey("dbo.Authors", "CreatedByID", "dbo.Users");
            DropForeignKey("dbo.Articles", "AuthorID", "dbo.Authors");
            DropIndex("dbo.Authors", new[] { "UpdatedByID" });
            DropIndex("dbo.Authors", new[] { "CreatedByID" });
            DropIndex("dbo.Articles", new[] { "AuthorID" });
            DropIndex("dbo.Articles", new[] { "UpdatedByID" });
            DropIndex("dbo.Articles", new[] { "CreatedByID" });
            DropTable("dbo.Authors");
            DropTable("dbo.Articles");
        }
    }
}

namespace FakirCms.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShowFrontPageColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Habers", "ShowFrontPage", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Habers", "ShowFrontPage");
        }
    }
}

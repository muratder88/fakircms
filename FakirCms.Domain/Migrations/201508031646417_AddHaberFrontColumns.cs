namespace FakirCms.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHaberFrontColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Habers", "ShowFrontPageMain", c => c.Boolean(nullable: false));
            AddColumn("dbo.Habers", "ShowFrontPageAlt", c => c.Boolean(nullable: false));
            AddColumn("dbo.Habers", "ShowFrontPageUst", c => c.Boolean(nullable: false));
            DropColumn("dbo.Habers", "ShowFrontPage");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Habers", "ShowFrontPage", c => c.Boolean(nullable: false));
            DropColumn("dbo.Habers", "ShowFrontPageUst");
            DropColumn("dbo.Habers", "ShowFrontPageAlt");
            DropColumn("dbo.Habers", "ShowFrontPageMain");
        }
    }
}

namespace FakirCms.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHaberCategoryDateColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HaberCategories", "Updated", c => c.DateTime(nullable: false, precision: 0));
            AddColumn("dbo.HaberCategories", "Created", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HaberCategories", "Created");
            DropColumn("dbo.HaberCategories", "Updated");
        }
    }
}

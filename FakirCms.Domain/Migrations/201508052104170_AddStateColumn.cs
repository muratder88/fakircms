namespace FakirCms.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStateColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "State", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Articles", "State");
        }
    }
}

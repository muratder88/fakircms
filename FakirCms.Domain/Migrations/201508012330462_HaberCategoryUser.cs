namespace FakirCms.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HaberCategoryUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HaberCategories", "CreatedByID", c => c.String(maxLength: 128, storeType: "nvarchar"));
            AddColumn("dbo.HaberCategories", "UpdatedByID", c => c.String(maxLength: 128, storeType: "nvarchar"));
            CreateIndex("dbo.HaberCategories", "CreatedByID");
            CreateIndex("dbo.HaberCategories", "UpdatedByID");
            AddForeignKey("dbo.HaberCategories", "CreatedByID", "dbo.Users", "Id");
            AddForeignKey("dbo.HaberCategories", "UpdatedByID", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HaberCategories", "UpdatedByID", "dbo.Users");
            DropForeignKey("dbo.HaberCategories", "CreatedByID", "dbo.Users");
            DropIndex("dbo.HaberCategories", new[] { "UpdatedByID" });
            DropIndex("dbo.HaberCategories", new[] { "CreatedByID" });
            DropColumn("dbo.HaberCategories", "UpdatedByID");
            DropColumn("dbo.HaberCategories", "CreatedByID");
        }
    }
}

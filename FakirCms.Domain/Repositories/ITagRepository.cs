﻿using System;
namespace FakirCms.Domain.Repositories
{
    public interface ITagRepository
    {
        int Add(FakirCms.Domain.Entities.Tag item);
        int Delete(int id);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Tag> Items { get; }
        void Update(FakirCms.Domain.Entities.Tag item);
    }
}

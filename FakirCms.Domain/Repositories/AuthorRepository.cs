﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace FakirCms.Domain.Repositories
{
    public class AuthorRepository : FakirCms.Domain.Repositories.IAuthorRepository
    {
        private EFDbFakirCms _context;
        public AuthorRepository(EFDbFakirCms context)
        {
            _context = context;
        }

        public IEnumerable<Author> Authors
        {
            get
            {
                return _context.Authors.Include(x => x.CreatedBy).Include(x => x.UpdatedBy);
            }
        }

        public bool Add(Author author)
        {
            _context.Entry(author).State = EntityState.Added;
            _context.Authors.Add(author);
            int result = _context.SaveChanges();
            return (result > 0) ? true : false;
        }

        public bool Delete(int id)
        {
            Author author = FindById(id);
            if (author != null)
            {
                _context.Authors.Remove(author);
                return (_context.SaveChanges() > 0) ? true : false;
            }
            else
            {
                throw new Exception(string.Format("Bu {0} numaralı id'ye sahip yazar bulunamadı", id));
            }
        }

        public bool Update(Author author)
        {
            _context.Entry(author).State = EntityState.Modified;
            return (_context.SaveChanges() > 0) ? true : false;
        }

        public Author FindById(int id)
        {
            return _context.Authors.Include(x => x.CreatedBy).Include(x => x.UpdatedBy).Where(x => x.Id == id).FirstOrDefault();
        }

        public Author FindByAlias(string alias)
        {
            return _context.Authors.Where(x => x.Alias == alias).FirstOrDefault();
        }
        public Article GetAurhorPublishLastArticle(Author author)
        {
            return _context.Articles.Include(x => x.Author).Where(x => x.State == true && x.AuthorID == author.Id).FirstOrDefault();
        }
    }
}

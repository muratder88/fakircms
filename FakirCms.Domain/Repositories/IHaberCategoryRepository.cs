﻿using System;
namespace FakirCms.Domain.Repositories
{
    public interface IHaberCategoryRepository
    {
        bool Add(FakirCms.Domain.Entities.HaberCategory category);
        bool Delete(int id);
        FakirCms.Domain.Entities.HaberCategory FindById(int id);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.HaberCategory> HaberCategories { get; }
        bool Update(FakirCms.Domain.Entities.HaberCategory haberCategory);
        FakirCms.Domain.Entities.HaberCategory FindByAlias(string alias);
    }
}

﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace FakirCms.Domain.Repositories
{
    public class ArticleRepository : FakirCms.Domain.Repositories.IArticleRepository
    {
        private EFDbFakirCms _context;
        public ArticleRepository(EFDbFakirCms context)
        {
            _context = context;
        }

        public IEnumerable<Article> Articles
        {
            get
            {
                return _context.Articles.Include(x => x.CreatedBy).Include(x => x.UpdatedBy).Include(x=>x.Author);
            }
        }

        public bool Add(Article article)
        {
            _context.Entry(article).State = EntityState.Added;
            _context.Articles.Add(article);
            int result = _context.SaveChanges();
            return (result > 0) ? true : false;
        }

        public bool Delete(int id)
        {
            Article article = FindById(id);
            if (article != null)
            {
                _context.Articles.Remove(article);
                return (_context.SaveChanges() > 0) ? true : false;
            }
            else
            {
                throw new Exception(string.Format("Bu {0} numaralı id'ye sahip yazı bulunamadı", id));
            }
        }

        public bool Update(Article article)
        {
            _context.Entry(article).State = EntityState.Modified;
            return (_context.SaveChanges() > 0) ? true : false;
        }

        public Article FindById(int id)
        {
            return _context.Articles.Include(x => x.CreatedBy).Include(x => x.UpdatedBy).Where(x => x.Id == id).FirstOrDefault();
        }

        public Article FindByAlias(string alias)
        {
            return _context.Articles.Where(x => x.Alias == alias).FirstOrDefault();
        }
    }
}

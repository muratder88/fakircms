﻿using System;
namespace FakirCms.Domain.Repositories
{
    public interface IArticleRepository
    {
        bool Add(FakirCms.Domain.Entities.Article article);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Article> Articles { get; }
        bool Delete(int id);
        FakirCms.Domain.Entities.Article FindByAlias(string alias);
        FakirCms.Domain.Entities.Article FindById(int id);
        bool Update(FakirCms.Domain.Entities.Article article);
    }
}

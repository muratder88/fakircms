﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace FakirCms.Domain.Repositories
{
    public class HaberCategoryRepository : FakirCms.Domain.Repositories.IHaberCategoryRepository
    {
        private EFDbFakirCms _context;
        public HaberCategoryRepository(EFDbFakirCms context)
        {
            _context = context;
        }

        public IEnumerable<HaberCategory> HaberCategories
        {
            get
            {
                return _context.HaberCategories.Include(x => x.CreatedBy).Include(x => x.UpdatedBy);
            }
        }

        public bool Add(HaberCategory category)
        {
            _context.Entry(category).State = EntityState.Added;
            _context.HaberCategories.Add(category);
            int result = _context.SaveChanges();
            return (result > 0) ? true : false;
        }

        public bool Delete(int id)
        {
            HaberCategory haberCategory = FindById(id);
            if (haberCategory != null)
            {
                _context.HaberCategories.Remove(haberCategory);
                return (_context.SaveChanges() > 0) ? true : false;
            }
            else
            {
                throw new Exception(string.Format("Bu {0} numaralı id'ye sahip haber kategorisi bulunamadı", id));
            }
        }
        public bool Update(HaberCategory haberCategory)
        {
            _context.Entry(haberCategory).State = EntityState.Modified;
            return (_context.SaveChanges() > 0) ? true : false;
        }

        public HaberCategory FindById(int id)
        {
            return _context.HaberCategories.Include(x => x.CreatedBy).Include(x => x.UpdatedBy).Where(x => x.Id == id).FirstOrDefault();
        }

        public HaberCategory FindByAlias(string alias)
        {
            return _context.HaberCategories.Where(x => x.Alias == alias).FirstOrDefault();
        }
    }
}

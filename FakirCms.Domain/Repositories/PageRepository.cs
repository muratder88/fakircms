﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using FakirCms.Domain.Entities;

namespace FakirCms.Domain.Repositories
{
    public class PageRepository : FakirCms.Domain.Repositories.IPageRepository
    {
        private EFDbFakirCms context;
        public PageRepository(EFDbFakirCms context)
        {
            this.context = context;
        }

        public IEnumerable<Page> Pages
        {
            get
            {
                return context.Pages.Include(x => x.CreatedBy).Include(x => x.UpdatedBy);
            }
        }

        public bool Add(Page item)
        {
            context.Entry(item).State = EntityState.Added;
            context.Pages.Add(item);
            int result = context.SaveChanges();
            return (result > 0) ? true : false;
        }

        public bool Delete(int id)
        {
            Page page = FindById(id);
            if (page != null)
            {
                context.Pages.Remove(page);
                return (context.SaveChanges() > 0) ? true : false;
            }
            else
            {
                throw new Exception(string.Format("Bu {0} numaralı id'ye sahip haber bulunamadı", id));
            }
        }

        public bool Update(Page page)
        {
            context.Entry(page).State = EntityState.Modified;
            return (context.SaveChanges() > 0) ? true : false;
        }

        public Page FindById(int id)
        {
            return Pages.Where(x => x.Id == id).FirstOrDefault();
        }
    }
}

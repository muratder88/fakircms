﻿using System;
namespace FakirCms.Domain.Repositories
{
    public interface IPageRepository
    {
        bool Add(FakirCms.Domain.Entities.Page item);
        bool Delete(int id);
        FakirCms.Domain.Entities.Page FindById(int id);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Page> Pages { get; }
        bool Update(FakirCms.Domain.Entities.Page page);
    }
}

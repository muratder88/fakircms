﻿using System;
namespace FakirCms.Domain.Repositories
{
    public interface IHaberRepository
    {
        bool Add(FakirCms.Domain.Entities.Haber item);
        bool Delete(int id);
        FakirCms.Domain.Entities.Haber FindById(int id);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Haber> Habers { get; }
        bool Update(FakirCms.Domain.Entities.Haber haber);
        bool UpdateTags(int id, System.Collections.Generic.List<FakirCms.Domain.Entities.Tag> list);
    }
}

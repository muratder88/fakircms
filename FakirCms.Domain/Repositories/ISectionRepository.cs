﻿using System;
namespace FakirCms.Domain.Repositories
{
    public interface ISectionRepository
    {
        void Add(FakirCms.Domain.Entities.Section item);
        void Delete(int id);
        FakirCms.Domain.Entities.Section GetItem(int id);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Section> Items { get; }
        void Update(FakirCms.Domain.Entities.Section item);
    }
}

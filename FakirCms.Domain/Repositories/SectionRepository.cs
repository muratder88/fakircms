﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Repositories
{
    public class SectionRepository : FakirCms.Domain.Repositories.ISectionRepository
    {

        private EFDbFakirCms context;
        public SectionRepository(EFDbFakirCms context)
        {
            this.context = context;
        }

        public void Add(Section item)
        {
            context.Sections.Add(item);
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            Section section = context.Sections.Where(s => s.SectionID == id).FirstOrDefault();
            if (section != null)
            {
                context.Sections.Remove(section);
                context.SaveChanges();
            }
            else
            {
                throw new Exception(id + "Böyle ID sahip bir bölüm bulunamadı");
            }
        }

        public Section GetItem(int id)
        {
            return context.Sections.Where(s => s.SectionID == id).FirstOrDefault();
        }

        public IEnumerable<Section> Items
        {
            get { return context.Sections; }
        }

        public void Update(Section item)
        {
            context.Entry(item).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}

﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Repositories
{
    public class TagRepository : FakirCms.Domain.Repositories.ITagRepository
    {
        private EFDbFakirCms context;
        public TagRepository(EFDbFakirCms context)
        {
            this.context = context;
        }

        public int Add(Tag item)
        {
            context.Tags.Add(item);
            return context.SaveChanges();
        }

        public int Delete(int id)
        {
            Tag tag = context.Tags.Where(x => x.TagId == id).FirstOrDefault();
            if (tag == null) throw new Exception("Silinmek istenen etiket veri tabanında bulunmuyor");
            context.Tags.Remove(tag);
            return context.SaveChanges();
        }

        public IEnumerable<Tag> Items
        {
            get { return context.Tags; }
        }

        public void Update(Tag item)
        {
            context.Entry(item).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}

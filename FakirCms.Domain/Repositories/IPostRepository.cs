﻿using System;
namespace FakirCms.Domain.Repositories
{
    public interface IPostRepository
    {
        int Add(FakirCms.Domain.Entities.Post item);
        int Delete(int id);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Post> Items { get; }
        int Update(FakirCms.Domain.Entities.Post item);
        int UpdateTags(int postID, System.Collections.Generic.List<FakirCms.Domain.Entities.Tag> list);
    }
}

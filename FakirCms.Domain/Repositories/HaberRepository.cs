﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace FakirCms.Domain.Repositories
{
    public class HaberRepository : FakirCms.Domain.Repositories.IHaberRepository 
    {
        private EFDbFakirCms context;
        public HaberRepository(EFDbFakirCms context)
        {
            this.context = context;
        }

        public IEnumerable<Haber> Habers
        {
            get
            {
                return context.Haberler.Include(x => x.Tags).Include(x => x.CreatedBy).Include(x => x.UpdatedBy).Include(x=>x.HaberCategory);
            }
        }

        public bool Add(Haber item)
        {
            context.Entry(item).State = EntityState.Added;
            context.Haberler.Add(item);
            int result = context.SaveChanges();
            return (result > 0) ? true : false;
        }

        public bool Delete(int id)
        {
            Haber haber = FindById(id);
            if (haber != null)
            {
                context.Haberler.Remove(haber);
                return (context.SaveChanges() > 0) ? true : false;
            }
            else
            {
                throw new Exception(string.Format("Bu {0} numaralı id'ye sahip haber bulunamadı", id));
            }
        }

        public bool UpdateTags(int id, List<Tag> list)
        {
            Haber haber = FindById(id);
            if (list == null) list = new List<Tag>();
            if (haber.Tags.Count() > 0)
            {
                //hepsini kaldır
                haber.Tags.RemoveAll(x => x.TagId > 0);
            }
            if (list.Count() > 0)
            {
                list.ForEach(x => haber.Tags.Add(x));
            }
            context.SaveChanges();
            return true;
        }
        public bool Update(Haber haber)
        {
            context.Entry(haber).State = EntityState.Modified;
            return (context.SaveChanges() > 0) ? true : false;
        }

        public Haber FindById(int id)
        {
            return context.Haberler.Include(x => x.Tags).Include(x => x.CreatedBy).Include(x => x.UpdatedBy).Where(x => x.Id == id).FirstOrDefault();
        }
    }
}

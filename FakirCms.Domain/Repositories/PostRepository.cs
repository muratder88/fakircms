﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Repositories
{
    public class PostRepository : FakirCms.Domain.Repositories.IPostRepository
    {
        private EFDbFakirCms context;
        public PostRepository(EFDbFakirCms context)
        {
            this.context = context;
        }
        public int Add(Post item)
        {
          context.Entry(item).State = EntityState.Added;
          context.Posts.Add(item);
          return context.SaveChanges();
        }

        public int Delete(int id)
        {
            Post content = context.Posts.Where(x => x.Id == id).FirstOrDefault();
            if(content!=null){
                context.Posts.Remove(content);
                return context.SaveChanges();               
            }
             else
            {
                throw new Exception("Böyle bir id yok");
            }
        }
        public IEnumerable<Post> Items
        {
            get
            {
                return context.Posts;
            }
        }

        public int Update(Post item)
        {
            context.Entry(item).State=EntityState.Modified;
            int result=context.SaveChanges();
            return result;
        }


        public int UpdateTags(int postID,List<Tag> list)
        {
            Post post = context.Posts.Include(x => x.Tags).Where(x => x.Id == postID).FirstOrDefault();
            if(list==null)list=new List<Tag>();
            if (post.Tags.Count() > 0)
            {
                //hepsini Kaldır
                post.Tags.RemoveAll(x=>x.TagId>0);
            }
            if (list.Count() > 0)
            {
                list.ForEach(x => post.Tags.Add(x));
            }
            return context.SaveChanges();
        }
    }
}

﻿using FakirCms.Domain.Entities;
using System;
namespace FakirCms.Domain.Repositories
{
    public interface IAuthorRepository
    {
        bool Add(FakirCms.Domain.Entities.Author author);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Author> Authors { get; }
        bool Delete(int id);
        FakirCms.Domain.Entities.Author FindByAlias(string alias);
        FakirCms.Domain.Entities.Author FindById(int id);
        bool Update(FakirCms.Domain.Entities.Author author);
        Article GetAurhorPublishLastArticle(Author author);
    }
}

﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using FakirCms.Domain.Models;
using FakirCms.Domain.Manager;

namespace FakirCms.Domain.Services
{
    public class ArticleService : FakirCms.Domain.Services.IArticleService
    {
        private IArticleRepository _articleRepository;
        private IUserService _userService;
        private IUrlRoutingManager _urlRoutingManager;
        private IAuthorService _authorService;
        public ArticleService(IArticleRepository articleRepository, IUserService userService,
            IUrlRoutingManager urlRoutingManager,IAuthorService authorService)
        {
            _articleRepository = articleRepository;
            _userService = userService;
            _authorService = authorService;
            _urlRoutingManager = urlRoutingManager;
        }
        public IEnumerable<Article> Items
        {
            get
            {
                return _articleRepository.Articles;
            }
        }
        public bool Create(Article article)
        {
            article.Created = DateTime.Now;
            article.CreatedByID = _userService.GetCurrentUser().Id;
            article.Updated = DateTime.Now;
            article.UpdatedByID = _userService.GetCurrentUser().Id;
            return _articleRepository.Add(article);
        }
        public bool Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentNullException("Id not be eguals or above zero");
            }
            return _articleRepository.Delete(id);
        }
        public bool Update(Article article)
        {
            if (article == null)
            {
                throw new ArgumentNullException("Author can not be null");
            }

            article.Updated = DateTime.Now;
            article.UpdatedByID = _userService.GetCurrentUser().Id;
            return _articleRepository.Update(article);
        }

        public Article FindById(int id)
        {
            return _articleRepository.FindById(id);
        }

        public Article FindByAlias(string alias)
        {
            return _articleRepository.FindByAlias(alias);
        }

        public ArticleDetail GetArticleDetails(Article article)
        {
            ArticleDetail detailedArticle =new  ArticleDetail();
            detailedArticle.Alias = article.Alias;
            detailedArticle.Id = article.Id;
            detailedArticle.Title = article.Title;
            detailedArticle.Hits = article.Hits;
            detailedArticle.Date = article.Date;
            detailedArticle.Link = _urlRoutingManager.GetArticleUrl(article.Author.Alias, article);
            detailedArticle.MetaDescription = article.MetaDescription;
            detailedArticle.MetaKeywords = article.MetaKeywords;
            detailedArticle.MetaTitle = article.MetaTitle;
            detailedArticle.Content = article.Content;
            detailedArticle.Author = _authorService.GetAuthorDetail(article.Author);
            return detailedArticle;
        }

        public List<ArticleDetail> GetArticleDetailList(List<Article> articleList)
        {
            List<ArticleDetail> detailedArticleList = new List<ArticleDetail>();
            articleList.ForEach(x =>
            {
                detailedArticleList.Add(this.GetArticleDetails(x));
            });
            return detailedArticleList;
        }

      
        public IQueryable<Article> GetPublishedArticles(Author author) 
        {
            return _articleRepository.Articles.Where(x => x.AuthorID == author.Id && x.State == true).AsQueryable();
        }
        public Article GetPublishedAuthorArticle(string authorAlias, int ArticleId)
        {
            return _articleRepository.Articles.
                Where(x => x.Id == ArticleId && x.Author.Alias == authorAlias).FirstOrDefault();
        }
    }
}

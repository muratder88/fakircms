﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Enums;
using FakirCms.Domain.FileSystems.Thumbnail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakirCms.Domain.Helpers;
using System.IO;
using System.Drawing;
using System.Web;

namespace FakirCms.Domain.Services
{
    public class ThumbnailService : FakirCms.Domain.Services.IThumbnailService
    {
        private IThumbnailFolder _thumbnailVirtualProvier;


        public ThumbnailService(IThumbnailFolder thumbnailVirtualProvier, IImageEditorService imageEditorService)
        {
            _thumbnailVirtualProvier = thumbnailVirtualProvier;
        }

        public bool CreateHaberThumbnails(Haber haber)
        {
            string result1 = CreateHaberThumbnail(haber, HaberThumbnailType.Small);
            string result2 = CreateHaberThumbnail(haber, HaberThumbnailType.Medium);
            string result3 = CreateHaberThumbnail(haber, HaberThumbnailType.High);
            return (!string.IsNullOrEmpty(result1) && !string.IsNullOrEmpty(result2) && !string.IsNullOrEmpty(result3));
        }

        public bool UpdateHaberThumnails(Haber haber)
        {
            try
            {
                //Delete Haber Thumbnails
                string smallPath = GetHaberThumbnailByType(haber, HaberThumbnailType.Small);
                _thumbnailVirtualProvier.DeleteFile(smallPath);
                string mediumPath = GetHaberThumbnailByType(haber, HaberThumbnailType.Medium);
                _thumbnailVirtualProvier.DeleteFile(mediumPath);
                string highPath = GetHaberThumbnailByType(haber, HaberThumbnailType.High);
                _thumbnailVirtualProvier.DeleteFile(highPath);

                return CreateHaberThumbnails(haber);

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private string CreateHaberThumbnail(Haber haber, HaberThumbnailType haberThumbnailType)
        {
            //check imageUrl and image exist
            //check imageUrl  and image exist
            if (string.IsNullOrEmpty(haber.ImageUrl) && !File.Exists(HttpContext.Current.Server.MapPath(haber.ImageUrl)))
                return null;

            //check haber thumbnail exist
            string thumbnailPath = @"haber\" + haber.Id + @"\" + haberThumbnailType.GetStringValue() + @"\" + haber.Title.ConverToFileName() + ".jpg";
            if (_thumbnailVirtualProvier.CanGenerateThumbnail(haber.ImageUrl))
            {
                Size thumbnailSize = new Size();
                if (haberThumbnailType == HaberThumbnailType.Small)
                    thumbnailSize = new Size(80, 80);
                if (haberThumbnailType == HaberThumbnailType.Medium)
                    thumbnailSize = new Size(120, 80);
                if (haberThumbnailType == HaberThumbnailType.High)
                    thumbnailSize = new Size(150, 80);

                //check thumbnail parent directory exist 
                if (!_thumbnailVirtualProvier.DirectoryExists("haber/" + haber.Id + "/" + haberThumbnailType.GetStringValue()))
                    _thumbnailVirtualProvier.CreateDirectory("haber/" + haber.Id + "/" + haberThumbnailType.GetStringValue());
                string outpath = _thumbnailVirtualProvier.CreateThumbnail(HttpContext.Current.Server.MapPath(haber.ImageUrl), @"haber\" + haber.Id + @"\" + haberThumbnailType.GetStringValue() + @"\", haber.Title.ConverToFileName(), thumbnailSize, true);
                string thumbUrl = _thumbnailVirtualProvier.GetImageUrlFromFullPath(outpath);
                return thumbUrl;
            }
            else
            {
                return null;
            }
        }

        public string GetHaberThumbnailByType(Haber haber, HaberThumbnailType haberThumbnailType)
        {
            //check haber main url and image exist
            if (string.IsNullOrEmpty(haber.ImageUrl) || !File.Exists(HttpContext.Current.Server.MapPath(haber.ImageUrl)))
                return null;
            if (this.HaberThumbnailExist(haber, haberThumbnailType))
            {
                string thumbPath = @"haber\" + haber.Id + @"\" + haberThumbnailType.GetStringValue() + @"\" + haber.Title.ConverToFileName() + ".jpg";
                string outpath = _thumbnailVirtualProvier.MapPath(thumbPath);
                string thumbURl = _thumbnailVirtualProvier.GetImageUrlFromFullPath(outpath);
                return thumbURl;
            }
            else
            {
                return CreateHaberThumbnail(haber, haberThumbnailType);
            }
        }

        private bool HaberThumbnailExist(Haber haber, HaberThumbnailType haberThumbnailType)
        {
            string thumbPath = @"haber\" + haber.Id + @"\" + haberThumbnailType.GetStringValue() + @"\" + haber.Title.ConverToFileName() + ".jpg";
            return _thumbnailVirtualProvier.FileExists(thumbPath);
        }

        public bool IsWriteable()
        {
            return _thumbnailVirtualProvier.CheckThubmnailPermission();
        }

    }

    public enum HaberThumbnailType
    {
        [StringValue("small")]
        Small,
        [StringValue("medium")]
        Medium,
        [StringValue("high")]
        High,

    }
}

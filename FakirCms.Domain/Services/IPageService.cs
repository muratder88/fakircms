﻿using System;
namespace FakirCms.Domain.Services
{
    public interface IPageService
    {
        bool Create(FakirCms.Domain.Entities.Page page);
        bool Delete(int id);
        System.Linq.IQueryable<FakirCms.Domain.Entities.Page> FinAllPublishedPages();
        FakirCms.Domain.Entities.Page FindByAlias(string alias);
        FakirCms.Domain.Entities.Page FindById(int id);
        bool IncreaseHits(int pageId);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Page> items { get; }
        bool Update(FakirCms.Domain.Entities.Page page);
    }
}

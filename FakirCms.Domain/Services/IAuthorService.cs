﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
namespace FakirCms.Domain.Services
{
    public interface IAuthorService
    {
        bool Create(FakirCms.Domain.Entities.Author author);
        bool Delete(int id);
        FakirCms.Domain.Entities.Author FindByAlias(string alias);
        FakirCms.Domain.Entities.Author FindById(int id);
        System.Collections.Generic.List<System.Web.Mvc.SelectListItem> GetSelectList();
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Author> Items { get; }
        bool Update(FakirCms.Domain.Entities.Author author);
        FakirCms.Domain.Models.AuthorDetail GetAuthorDetail(FakirCms.Domain.Entities.Author author);
        System.Collections.Generic.List<FakirCms.Domain.Models.AuthorDetail> GetAuthorDetailList(List<Author> authorList);
    }
}

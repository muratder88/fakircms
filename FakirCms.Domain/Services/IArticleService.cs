﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
namespace FakirCms.Domain.Services
{
    public interface IArticleService
    {
        bool Create(FakirCms.Domain.Entities.Article article);
        bool Delete(int id);
        FakirCms.Domain.Entities.Article FindByAlias(string alias);
        FakirCms.Domain.Entities.Article FindById(int id);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Article> Items { get; }
        bool Update(FakirCms.Domain.Entities.Article article);
        IQueryable<FakirCms.Domain.Entities.Article> GetPublishedArticles(FakirCms.Domain.Entities.Author author);
        Article GetPublishedAuthorArticle(string authorAlias, int ArticleId);
        FakirCms.Domain.Models.ArticleDetail GetArticleDetails(Article article);
        List<ArticleDetail> GetArticleDetailList(List<Article> articleList);
       
    }
}

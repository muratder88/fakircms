﻿using System;
namespace FakirCms.Domain.Services
{
    public interface IThumbnailService
    {
        bool CreateHaberThumbnails(FakirCms.Domain.Entities.Haber haber);
        string GetHaberThumbnailByType(FakirCms.Domain.Entities.Haber haber, HaberThumbnailType haberThumbnailType);
        bool IsWriteable();
        bool UpdateHaberThumnails(FakirCms.Domain.Entities.Haber haber);
    }
}

﻿using System;
namespace FakirCms.Domain.Services
{
    public interface IPostService
    {
        bool Add(FakirCms.Domain.Entities.Post item);
        bool Delete(int itemID);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Post> GetContentsFromCategoryID(int catId);
        FakirCms.Domain.Models.PostDetail GetDetailsOfPost(FakirCms.Domain.Entities.Post post);
        string GetFirstImageFromText(string text);
        FakirCms.Domain.Entities.Post GetPageBreakedPost(FakirCms.Domain.Entities.Post post);
        System.Collections.Generic.List<System.Web.Mvc.SelectListItem> GetUserList();
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Post> items { get; }
        bool Update(FakirCms.Domain.Entities.Post item);
    }
}

﻿using System;
namespace FakirCms.Domain.Services
{
    public interface IHaberCategoryService
    {
        bool Create(FakirCms.Domain.Entities.HaberCategory category);
        bool Delete(int id);
        FakirCms.Domain.Entities.HaberCategory FindById(int id);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.HaberCategory> items { get; }
        bool Update(FakirCms.Domain.Entities.HaberCategory category);
        System.Collections.Generic.List<System.Web.Mvc.SelectListItem> GetSelectList();
        FakirCms.Domain.Entities.HaberCategory FindByAlias(string alias);
    }
}

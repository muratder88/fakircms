﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FakirCms.Domain.Services
{
    public class HaberCategoryService : FakirCms.Domain.Services.IHaberCategoryService
    {
        private IHaberCategoryRepository _haberCategoryRepository;
        private IUserService _userService;
        public HaberCategoryService(IHaberCategoryRepository haberCategoryRepository,IUserService userService)
        {
            _haberCategoryRepository = haberCategoryRepository;
            _userService = userService;
        }
        public IEnumerable<HaberCategory> items
        {
            get
            {
                return _haberCategoryRepository.HaberCategories;
            }
        }
        public bool Create(HaberCategory category)
        {
            category.Created = DateTime.Now;
            category.CreatedByID = _userService.GetCurrentUser().Id;
            category.Updated = DateTime.Now;
            category.UpdatedByID = _userService.GetCurrentUser().Id;
            return _haberCategoryRepository.Add(category);
        }

        public bool Delete(int id)
        {
            return _haberCategoryRepository.Delete(id);
        }

        public bool Update(HaberCategory category)
        {
            category.Updated = DateTime.Now;
            category.UpdatedByID = _userService.GetCurrentUser().Id;
            return _haberCategoryRepository.Update(category);
        }
        public HaberCategory FindById(int id)
        {
            return _haberCategoryRepository.FindById(id);
        }

        public HaberCategory FindByAlias(string alias)
        {
            return _haberCategoryRepository.FindByAlias(alias);
        }

        public List<SelectListItem> GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (items.Count() > 0)
            {
                foreach (var cat in items)
                {
                    list.Add(new SelectListItem()
                    {
                        Value=cat.Id.ToString(),
                        Text=cat.Title
                    });
                }
            }

            return list;
        }
    }
}

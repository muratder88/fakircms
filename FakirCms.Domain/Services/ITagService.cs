﻿using System;
namespace FakirCms.Domain.Services
{
    public interface ITagService
    {
        void Add(FakirCms.Domain.Entities.Tag tag);
        void Delete(FakirCms.Domain.Entities.Tag tag);
        FakirCms.Domain.Entities.Tag FindByAlias(string alias);
        FakirCms.Domain.Models.TagDetail GetTagDetail(FakirCms.Domain.Entities.Tag tag);
        System.Collections.Generic.List<System.Web.Mvc.SelectListItem> GetTagSelectListItems();
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Tag> Tags { get; }
        void Update(FakirCms.Domain.Entities.Tag tag);
    }
}

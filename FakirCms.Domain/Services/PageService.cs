﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Exceptions;
using FakirCms.Domain.Manager;
using FakirCms.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Services
{
    public class PageService : FakirCms.Domain.Services.IPageService
    {
        private IPageRepository _pageRepository;
        private IUrlRoutingManager _urlRoutinManager;
        private IUserService _userService;

        public PageService(IPageRepository pageRepository, IUrlRoutingManager urlRoutingManager,
            IUserService userService)
        {
            _pageRepository = pageRepository;
            _urlRoutinManager = urlRoutingManager;
            _userService = userService;
        }
        public IEnumerable<Page> items
        {
            get
            {
                return _pageRepository.Pages;
            }
        }

        public bool Create(Page page)
        {
            if (FindByAlias(page.Alias) != null)
            {
                throw new InsertDuplicateException(string.Format("Takma isim benzersiz olmak zorunda.{0} takma ismi kayıtlarda var.Başka bir takma isim vermeyi deneyin", page.Alias));
            }
            page.Created = DateTime.Now;
            page.CreatedByID = _userService.GetCurrentUser().Id;
            page.Hits = 0;
            page.Updated = DateTime.Now;
            page.UpdatedByID = _userService.GetCurrentUser().Id;
            return _pageRepository.Add(page);
        }

        public bool Delete(int id)
        {
            return _pageRepository.Delete(id);
        }

        public bool Update(Page page)
        {
            page.Updated = DateTime.Now;
            bool isAliasExist = _pageRepository.Pages.Where(x => x.Alias == page.Alias && x.Id != page.Id).Any();
            if (isAliasExist)
                throw new InsertDuplicateException(string.Format("Takma isim benzersiz olmak zorunda.{0} takma ismi kayıtlarda var.Başka bir takma isim vermeyi deneyin", page.Alias));
            page.UpdatedByID = _userService.GetCurrentUser().Id;
            return _pageRepository.Update(page);
        }

        public Page FindById(int id)
        {
            return _pageRepository.FindById(id);
        }
        public Page FindByAlias(string alias)
        {
            return _pageRepository.Pages.Where(x => x.Alias == alias).FirstOrDefault();
        }

        public bool IncreaseHits(int pageId)
        {
            Page page = FindById(pageId);
            if (page == null)
            {
                return false;
            }
            page.Hits = page.Hits + 1;
            return _pageRepository.Update(page);
        }

        public IQueryable<Page> FinAllPublishedPages()
        {
            return items.Where(x => x.State == true).AsQueryable();
        }
    }
}

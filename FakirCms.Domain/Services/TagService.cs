﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Exceptions;
using FakirCms.Domain.Manager;
using FakirCms.Domain.Models;
using FakirCms.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FakirCms.Domain.Services
{
    public class TagService : FakirCms.Domain.Services.ITagService
    {
        private ITagRepository _tagRepository;
        private IUrlRoutingManager _urlRoutingManager;
        public TagService(ITagRepository tagRepository, IUrlRoutingManager urlRoutingManager)
        {
            _tagRepository = tagRepository;
            _urlRoutingManager = urlRoutingManager;
        }
        public IEnumerable<Tag> Tags
        {
            get
            {
                return _tagRepository.Items;
            }
        }

        public void Add(Tag tag)
        {
            if (FindByAlias(tag.Alias) != null)
            {
                throw new InsertDuplicateException(string.Format("{0} etiketi {1}'ye donüştürüldü.{1} etiketi zaten kayıtlarda bulunuyor", tag.Name, tag.Alias));
            }
            _tagRepository.Add(tag);
        }

        public void Delete(Tag tag)
        {
            _tagRepository.Delete(tag.TagId);
        }

        public void Update(Tag tag)
        {
            bool isAliasExist = _tagRepository.Items.Where(x => x.Alias == tag.Alias && x.TagId != tag.TagId).Any();
            if (isAliasExist)
                throw new InsertDuplicateException(string.Format("{0} etiketi {1}'ye donüştürüldü.{1} etiketi zaten kayıtlarda bulunuyor", tag.Name, tag.Alias));
            _tagRepository.Update(tag);
        }

        public TagDetail GetTagDetail(Tag tag)
        {
            TagDetail detailedTag = new TagDetail();
            detailedTag.Id = tag.TagId;
            detailedTag.Name = tag.Name;
            detailedTag.Alias = tag.Alias;
            detailedTag.HaberLink = _urlRoutingManager.GetHaberTagUrl(tag.Alias);
            return detailedTag;
        }



        public Tag FindByAlias(string alias)
        {
            return _tagRepository.Items.Where(x => x.Alias == alias).FirstOrDefault();
        }


        public List<SelectListItem> GetTagSelectListItems()
        {
            List<Tag> tagList = _tagRepository.Items.ToList();
            List<SelectListItem> list = new List<SelectListItem>();
            if (tagList != null && tagList.Count > 0)
            {
                list.Add(new SelectListItem() { Text = "Etiket Seç", Value = "" });
                tagList.ForEach(tag =>
                {
                    SelectListItem item = new SelectListItem()
                    {
                        Text = tag.Name,
                        Value = tag.TagId.ToString()
                    };
                    list.Add(item);
                });
                return list;
            }
            else
            {
                list.Add(new SelectListItem() { Text = "Etiket Seç", Value = "" });
                return list;
            }
        }
    }
}

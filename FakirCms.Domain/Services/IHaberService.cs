﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Models;
using System;
using System.Linq;
namespace FakirCms.Domain.Services
{
    public interface IHaberService
    {
        bool Create(FakirCms.Domain.Entities.Haber haber);
        bool Delete(int id);
        System.Linq.IQueryable<FakirCms.Domain.Entities.Haber> FindAllCityPublishedHabers(string CityName);
        System.Linq.IQueryable<FakirCms.Domain.Entities.Haber> FindAllHabersByTagAlias(string alias);
        System.Linq.IQueryable<FakirCms.Domain.Entities.Haber> FindAllPublishedHabers();
        System.Linq.IQueryable<FakirCms.Domain.Entities.Haber> FindAllTownPublishedHabers(string TownName);
        FakirCms.Domain.Entities.Haber FindById(int id);
        HaberDetail GetHaberDetails(FakirCms.Domain.Entities.Haber haber);
        FakirCms.Domain.Entities.Haber GetNextHaber(FakirCms.Domain.Entities.Haber haber);
        FakirCms.Domain.Entities.Haber GetPreviousHaber(FakirCms.Domain.Entities.Haber haber);
        System.Collections.Generic.List<FakirCms.Domain.Entities.Haber> GetRelatedHabers(FakirCms.Domain.Entities.Haber haber);
        bool IncreaseHits(int haberID);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.Haber> items { get; }
        bool Update(FakirCms.Domain.Entities.Haber haber);
        bool UpdateTags(int id, System.Collections.Generic.List<FakirCms.Domain.Entities.Tag> tags);
        IQueryable<FakirCms.Domain.Entities.Haber> GetPublishedCategoryHabersByCategoryAlias(string categoryAlias);
        IQueryable<FakirCms.Domain.Entities.Haber> GetFrontPageMansetHabers();
        System.Collections.Generic.List<HaberDetail> GetHaberDetailList(System.Collections.Generic.List<FakirCms.Domain.Entities.Haber> haberList);
        IQueryable<Haber> GetFrontPageMainHabers();
        IQueryable<Haber> GetFrontPageAltHabers();
        IQueryable<Haber> GetFrontPageUstHabers();
        IQueryable<Haber> GetPopularHabers();
        IQueryable<Haber> GetRecentHabers();
    }
}

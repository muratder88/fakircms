﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Manager;
using FakirCms.Domain.Models;
using FakirCms.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FakirCms.Domain.Services
{
    public class AuthorService : FakirCms.Domain.Services.IAuthorService
    {
        private IAuthorRepository _authorRepository;
        private IUserService _userService;
        private IUrlRoutingManager _urlRoutingManager;
      
        public AuthorService(IAuthorRepository authorRepository,IUserService userService,
            IUrlRoutingManager urlRoutingManager)
        {
            _authorRepository = authorRepository;
            _userService = userService;
            _urlRoutingManager = urlRoutingManager;
        }

        public IEnumerable<Author> Items
        {
            get
            {
                return _authorRepository.Authors;
            }
        }

        public bool Create(Author author)
        {
            author.Created = DateTime.Now;
            author.CreatedByID = _userService.GetCurrentUser().Id;
            author.Updated = DateTime.Now;
            author.UpdatedByID = _userService.GetCurrentUser().Id;
            return _authorRepository.Add(author);
        }

        public bool Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentNullException("Id not be eguals or above zero");
            }
            return _authorRepository.Delete(id);
        }

        public bool Update(Author author)
        {
            if (author == null)
            {
                throw new ArgumentNullException("Author can not be null");
            }

            author.Updated = DateTime.Now;
            author.UpdatedByID = _userService.GetCurrentUser().Id;
            return _authorRepository.Update(author);
        }

        public Author FindById(int id)
        {
            return _authorRepository.FindById(id);
        }

        public Author FindByAlias(string alias)
        {
            return _authorRepository.FindByAlias(alias);
        }

        public AuthorDetail GetAuthorDetail(Author author)
        {
            AuthorDetail detailedAuthor = new AuthorDetail();
            detailedAuthor.Alias = author.Alias;
            detailedAuthor.Id = author.Id;
            detailedAuthor.Email = author.Email;
            detailedAuthor.MetaDescription = author.MetaDescription;
            detailedAuthor.MetaTitle = author.MetaTitle;
            detailedAuthor.MetaKeywords = author.MetaKeywords;
            detailedAuthor.Image = author.Image;
            detailedAuthor.Name = author.Name;
            detailedAuthor.Link = _urlRoutingManager.GetAuthorUrl(author.Alias);
            Article article =_authorRepository.GetAurhorPublishLastArticle(author);
            detailedAuthor.LastArticleLink = _urlRoutingManager.GetArticleUrl(article.Author.Alias, article);
            detailedAuthor.LastArticleTitle = article.Title;
            detailedAuthor.LastArticleDate = article.Date;
            
            return detailedAuthor;

        }

        public List<AuthorDetail> GetAuthorDetailList(List<Author> authorList)
        {
            List<AuthorDetail> list = new List<AuthorDetail>();
            authorList.ForEach(x =>
            {
                list.Add(this.GetAuthorDetail(x));
            });
            return list;
        }
       
        
        public List<SelectListItem> GetSelectList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (Items.Count() > 0)
            {
                foreach (var author in Items)
                {
                    list.Add(new SelectListItem()
                    {
                        Value = author.Id.ToString(),
                        Text = author.Name.ToString()
                    });
                }
            }

            return list;
        }
    }
}

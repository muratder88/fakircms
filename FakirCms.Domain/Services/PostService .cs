﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Models;
using FakirCms.Domain.Repositories;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;

namespace FakirCms.Domain.Services
{
    public class PostService : FakirCms.Domain.Services.IPostService
    {
        private PostRepository _postRepository;
        public PostService(IPostRepository postRepository)
        {
            this._postRepository = (PostRepository)postRepository;
        }
        public IEnumerable<Post> items
        {
            get
            {

                return _postRepository.Items;
            }
        }
        public Boolean Add(Post item)
        {
            int result = _postRepository.Add(item);
            return (result > 0) ? true : false;
        }
        public Boolean Delete(int itemID)
        {
            int result = _postRepository.Delete(itemID);
            return (result > 0) ? true : false;
        }

        public Boolean Update(Post item)
        {
            int result = _postRepository.Update(item);
            return (result > 0) ? true : false;
        }
        /// <summary>
        /// Post Fulltext eğer page break varsa ona göre işlem yaparak introtext'i belirler.Eğer page break yoksa fulltext
        /// null olur introtext=fulltext olur
        /// </summary>
        /// <param name="post">Post has FullText</param>
        /// <returns></returns>
        public Post GetPageBreakedPost(Post post)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(post.FullText);
            //Page break varsa ona göre işlem yap
            if (doc.DocumentNode.SelectNodes("//div[contains(@style,'page-break-after: always')]") != null)
            {
                HtmlDocument introdoc = new HtmlDocument();
                HtmlDocument fulldoc = new HtmlDocument();
                Boolean intro = true;
                foreach (var node in doc.DocumentNode.ChildNodes)
                {
                    if (node.NodeType == HtmlNodeType.Element && node.Name == "div" && node.Attributes["style"] != null
                        && node.Attributes["style"].Value == "page-break-after: always")
                    {
                        intro = false;
                    }
                    else
                    {
                        if (intro)
                            introdoc.DocumentNode.AppendChild(node);
                        else
                            fulldoc.DocumentNode.AppendChild(node);
                    }

                }

                post.IntroText = introdoc.DocumentNode.InnerHtml;
                post.FullText = fulldoc.DocumentNode.InnerHtml;
            }
            else
            {
                post.IntroText = post.FullText;
                post.FullText = null;
            }
            return post;
        }

        //public List<SelectListItem> GetCategoryList()
        //{
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    foreach (var pair in _postRepository.GetCategories())
        //    {
        //        SelectListItem item = new SelectListItem()
        //        {
        //            Text = pair.Value,
        //            Value = pair.Key.ToString()
        //        };
        //        list.Add(item);
        //    }
        //    return list;
        //}

        public List<SelectListItem> GetUserList()
        {
            //List<SelectListItem> list = new List<SelectListItem>();
            //foreach (var pair in _postRepository.GetUsers())
            //{
            //    SelectListItem item = new SelectListItem()
            //    {
            //        Text = pair.Value,
            //        Value = pair.Key.ToString()
            //    };
            //    list.Add(item);
            //}
            //return list;
            return null;
        }

        public IEnumerable<Post> GetContentsFromCategoryID(int catId)
        {
            return _postRepository.Items.Where(x => x.CategoryID == catId);
        }

        /// <summary>
        /// Get post details example image from text and link from routing vs
        /// </summary>
        /// <param name="post">Post want to detail</param>
        /// <returns>PostDetail:setted according to given post</returns>
        public PostDetail GetDetailsOfPost(Post post)
        {
            PostDetail model = new PostDetail();

            model.id = post.Id;

            //Link From system routing
            model.Link = GetLinkFromPost(post);

            model.Title = post.Title;
            model.Created = post.Created;
            model.CreatedBy = post.CreatedBy;
            model.UserName = post.CreatedBy.UserName;
            model.IntroHtmlText = post.IntroText;
            if (string.IsNullOrEmpty(post.FullText))
            {
                model.FullHtmlText = null;
                model.PageBreakExist = false;
            }
            else
            {
                model.FullHtmlText = post.FullText;
                model.PageBreakExist = true;
            }

            model.Image = GetFirstImageFromText(post.IntroText);
            model.ImageExist = (model.Image == null) ? false : true;
            model.IntroText = GetTextFromHtml(post.IntroText);

            return model;
        }

        /// <summary>
        /// Get Inner text after comments removed
        /// </summary>
        /// <param name="htmlText">Html Content</param>
        /// <returns>string html innertext removed comments</returns>
        private string GetTextFromHtml(string htmlText)
        {
            HtmlDocument doc = new HtmlDocument();

            //encede olmuş html düzelt
            htmlText = HttpUtility.HtmlDecode(htmlText);
            doc.LoadHtml(htmlText);

            //remove Comments
            if (doc.DocumentNode.SelectNodes("//comment()") != null)
            {
                foreach (HtmlNode comment in doc.DocumentNode.SelectNodes("//comment()"))
                {
                    comment.ParentNode.RemoveChild(comment);
                }
            }

            //Get Intro Content From FullText
            string fullText = doc.DocumentNode.InnerText;



            if (fullText != null && fullText.Count() > 100)
                return fullText.Trim();
            return null;
        }

        /// <summary>
        /// Gets Route Url of post
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        private string GetLinkFromPost(Post post)
        {
            //Url Routing UrlHelper make
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return urlHelper.RouteUrl("ArticleDetail", new
            {
                //sectionAlias = post.Category.Section.Alias,
                //categoryAlias = post.Category.Alias,
                //id = post.Id,
                //alias = post.Alias
            });
        }
        /// <summary>
        /// Gets first img value attribute, image not exist it return null
        /// </summary>
        /// <param name="text">This is html text</param>
        /// <returns>string img src value</returns>
        public string GetFirstImageFromText(string text)
        {
            //Get Image Url From Content Text
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(text);
            HtmlNode node = null;
            if (doc.DocumentNode.SelectNodes("//img") != null)
            {
                node = doc.DocumentNode.SelectNodes("//img")[0];
            }
            if (node != null)
                return node.Attributes["src"].Value;
            else
                return null;
        }
    }
}

﻿using System;
namespace FakirCms.Domain.Services
{
    public interface IUserService
    {
        Microsoft.AspNet.Identity.IdentityResult Add(FakirCms.Domain.Entities.AppUser user, string password);
        Microsoft.AspNet.Identity.IdentityResult AddRole(string RoleName);
        Microsoft.AspNet.Identity.IdentityResult AddUserToRole(string userName, string roleName);
        Microsoft.AspNet.Identity.IdentityResult CreateUser(FakirCms.Domain.Entities.AppUser user, string password);
        Microsoft.AspNet.Identity.IdentityResult DeleteUser(string userId);
        FakirCms.Domain.Entities.AppUser FindUserById(string id);
        FakirCms.Domain.Entities.AppUser FindUserByUserName(string userName);
        FakirCms.Domain.Entities.AppUser GetCurrentUser();
        System.Collections.Generic.List<string> GetUserRoles(string userid);
        System.Collections.Generic.List<System.Web.Mvc.SelectListItem> GetUsersSelectListItems();
        bool Login(string userName, string password);
        bool Logout();
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.AppRole> Roles { get; }
        Microsoft.AspNet.Identity.IdentityResult SetUserPassword(string userId, string newpassword);
        Microsoft.AspNet.Identity.IdentityResult UpdateUser(FakirCms.Domain.Entities.AppUser user);
        Microsoft.AspNet.Identity.IdentityResult UpdateUserRoles(string userId, System.Collections.Generic.List<string> roles);
        Microsoft.AspNet.Identity.IdentityResult UserResetPassword(string userId, string currentPassword, string newPassword);
        System.Collections.Generic.IEnumerable<FakirCms.Domain.Entities.AppUser> Users { get; }
    }
}

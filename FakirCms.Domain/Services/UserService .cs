﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Manager;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace FakirCms.Domain.Services
{
    public class UserService : FakirCms.Domain.Services.IUserService
    {
        private AppUserManager _userManager;
        private AppRoleManager _roleManager;
        private IAuthenticationManager _authManager;
        private EFDbFakirCms _context;
        public UserService(AppUserManager userManager, IAuthenticationManager authManager,
            AppRoleManager roleManager, EFDbFakirCms context)
        {
            _userManager = userManager;
            _authManager = authManager;
            _roleManager = roleManager;
            _context = context;
        }

        public IEnumerable<AppUser> Users
        {
            get { return _userManager.Users.ToList(); }
        }
        public IEnumerable<AppRole> Roles
        {
            get { return _roleManager.Roles; }
        }

        public IdentityResult Add(AppUser user, string password)
        {
            return _userManager.Create(user, password);
        }

        public IdentityResult UpdateUser(AppUser user)
        {
            _context.Entry(user).State = EntityState.Modified;
            return _userManager.Update(user);
        }
        public List<string> GetUserRoles(string userid)
        {
            List<string> userRoles = _userManager.GetRoles(userid).ToList();
            if (userRoles.Count() > 0)
            {
                return userRoles;
            }
            else
            {
                return null;
            }

        }

        public IdentityResult CreateUser(AppUser user, string password)
        {
            return _userManager.Create(user, password);
        }

        public IdentityResult AddRole(string RoleName)
        {
            //AppRole role = new AppRole(RoleName);
            //return _roleManager.Create(role);
            return null;
        }

        public IdentityResult AddUserToRole(string userName, string roleName)
        {
            AppUser user = _userManager.Users.Where(x => x.UserName == userName).FirstOrDefault();
            if (user == null)
            {

                throw new Exception(userName + " adına sahip bir kulllanıcı yok");
            }
            else
            {
                return _userManager.AddToRole(user.Id, roleName);
            }
        }

        public IdentityResult DeleteUser(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new Exception("UserId cannot be null or empty");
            AppUser user = _userManager.FindById(userId);
            if (user == null)
            {
                throw new Exception(string.Format("Böyle bir id({0})'ye sahip kullanıcı bulunamadı", userId));
            }
            else
            {
                return _userManager.Delete(user);
            }

        }

        public IdentityResult UpdateUserRoles(string userId, List<string> roles)
        {
            if (string.IsNullOrEmpty(userId))
                throw new Exception("UserId cannot be null or empty");
            AppUser user = _userManager.FindById(userId);
            if (roles == null || roles.Count <= 0)
            {
                //if roles is null, user doesn't select any role because of this, delete all roles and don't add any new role to user
                if (user.Roles.Count() > 0)
                {
                    IdentityResult deletionResult = new IdentityResult();
                    foreach (var role in user.Roles.ToList())
                    {
                        string roleName = _roleManager.FindById(role.Id).Name;
                        deletionResult = _userManager.RemoveFromRole(userId, roleName);
                    }
                    return deletionResult;
                }
                else
                {
                    //donot any thing
                    return new IdentityResult(null);
                }
            }
            else
            {
                //if roles is not null, delete existing user Roles and add new roles to user
                if (user.Roles.Count() > 0)
                {
                    IdentityResult deletionResult = new IdentityResult();
                    foreach (var role in user.Roles.ToList())
                    {
                        string roleName = _roleManager.FindById(role.Id).Name;
                        deletionResult = _userManager.RemoveFromRole(userId, roleName);
                    }
                }
                IdentityResult addedDeletionResult = new IdentityResult();
                foreach (var roleName in roles)
                {
                    if (!_roleManager.RoleExists(roleName))
                        throw new Exception(roleName + " sitede yüklü kullanıcı rollerinden değil");
                    addedDeletionResult = _userManager.AddToRole(userId, roleName);
                }
                return addedDeletionResult;
            }
            
        }


        public Boolean Login(string userName, string password)
        {
            AppUser user = _userManager.Find(userName, password);
            if (user == null)
            {
                return false;
            }
            else
            {
                ClaimsIdentity ident = _userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                IAuthenticationManager authManager = HttpContext.Current.GetOwinContext().Authentication;
                authManager.SignOut();
                authManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = false
                }, ident);
                user.LastVisitDate = DateTime.Now;
                user.Active = true;
                _userManager.Update(user);
                return true;
            }
        }

        public AppUser FindUserByUserName(string userName)
        {

            return _userManager.Users.Where(x => x.UserName == userName).FirstOrDefault();
        }
        public AppUser FindUserById(string id)
        {
            return _userManager.FindById(id);
        }

        public IdentityResult SetUserPassword(string userId, string newpassword)
        {
            var result = _userManager.RemovePassword(userId);
            var result2 = _userManager.AddPassword(userId, newpassword);
            if (!result.Succeeded)
                return result;
            return result2;
        }
        public IdentityResult UserResetPassword(string userId, string currentPassword, string newPassword)
        {
            return _userManager.ChangePassword(userId, currentPassword, newPassword);
        }

        public AppUser GetCurrentUser()
        {
            if (HttpContext.Current.User != null)
            {
                string userName = HttpContext.Current.User.Identity.Name;
                return _userManager.Users.Where(x => x.UserName == userName).FirstOrDefault();

            }
            return null;
        }

        public List<SelectListItem> GetUsersSelectListItems()
        {
            List<AppUser> userList = _userManager.Users.ToList();
            List<SelectListItem> list = new List<SelectListItem>();
            if (userList != null && userList.Count > 0)
            {
                list.Add(new SelectListItem() { Text = "Kullanıcı Seç", Value = "" });
                userList.ForEach(user =>
                {
                    SelectListItem item = new SelectListItem()
                    {
                        Text = user.UserName,
                        Value = user.UserName
                    };
                    list.Add(item);
                });
                return list;
            }
            else
            {
                list.Add(new SelectListItem() { Text = "Kullanıcı Seç", Value = "" });
                return list;
            }
        }
        public bool Logout()
        {
            IAuthenticationManager autManager = HttpContext.Current.GetOwinContext().Authentication;
            string userID = autManager.User.Identity.GetUserId();
            AppUser user = _userManager.Users.Where(x => x.Id == userID).FirstOrDefault();
            if (user != null)
            {
                user.Active = false;
                _userManager.Update(user);
                autManager.SignOut();
                return true;
            }
            else
            {
                autManager.SignOut();
                return true;
            }
        }
    }
}

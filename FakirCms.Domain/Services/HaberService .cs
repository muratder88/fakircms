﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Helpers;
using FakirCms.Domain.Manager;
using FakirCms.Domain.Models;
using FakirCms.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Services
{
    public class HaberService : FakirCms.Domain.Services.IHaberService 
    {
        private IHaberRepository _haberRepository;
        private IUrlRoutingManager _urlRoutinManager;
        private IThumbnailService _thumnailService;
        private IUserService _userService;
        private ITagService _tagService;


        public HaberService(IHaberRepository haberRepository, IUrlRoutingManager urlRoutinManager,
            IThumbnailService thumbnailService, IUserService userService, ITagService tagService)
        {
            this._haberRepository = haberRepository;
            _urlRoutinManager = urlRoutinManager;
            _thumnailService = thumbnailService;
            _userService = userService;
            _tagService = tagService;
        }

        public IEnumerable<Haber> items
        {
            get
            {
                return _haberRepository.Habers;
            }
        }

        public bool Create(Haber haber)
        {
            haber.Created = DateTime.Now;
            haber.CreatedByID = _userService.GetCurrentUser().Id;
            haber.Hits = 0;
            haber.Updated = DateTime.Now;
            haber.UpdatedByID = _userService.GetCurrentUser().Id;
            return _haberRepository.Add(haber);
        }

        public bool Delete(int id)
        {
            return _haberRepository.Delete(id);
        }

        public bool Update(Haber haber)
        {
            haber.Updated = DateTime.Now;
            haber.UpdatedByID = _userService.GetCurrentUser().Id;
            return _haberRepository.Update(haber);
        }
        public Haber FindById(int id)
        {
            return _haberRepository.FindById(id);
        }

        public bool IncreaseHits(int haberID)
        {
            Haber haber = FindById(haberID);
            if (haber == null)
            {
                return false;
            }
            haber.Hits = haber.Hits + 1;
            return _haberRepository.Update(haber);
        }

        public bool UpdateTags(int id, List<Tag> tags)
        {
            return _haberRepository.UpdateTags(id, tags);
        }

        public Haber GetNextHaber(Haber haber)
        {
            return _haberRepository.Habers.Where(x => x.Date < haber.Date).OrderByDescending(x => x.Date).FirstOrDefault();
        }

        public Haber GetPreviousHaber(Haber haber)
        {
            return _haberRepository.Habers.Where(x => x.Date > haber.Date).OrderBy(x => x.Date).FirstOrDefault();
        }


        public List<HaberDetail> GetHaberDetailList(List<Haber> haberList)
        {
            List<HaberDetail> detailedHaberList = new List<HaberDetail>();
            haberList.ForEach(x =>
            {
                detailedHaberList.Add(this.GetHaberDetails(x));
            });
            return detailedHaberList;
        }
        public HaberDetail GetHaberDetails(Haber haber)
        {
            HaberDetail detailedHaber = new HaberDetail();
            detailedHaber.id = haber.Id;
            detailedHaber.Title = haber.Title;
            detailedHaber.Image = haber.ImageUrl;
            detailedHaber.Date = haber.Date;
            detailedHaber.ImageExist = (!string.IsNullOrEmpty(haber.ImageUrl));
            detailedHaber.IntroText = HtmlParser.GetTextFromHtml(haber.IntroText);
            detailedHaber.IntroHtmlText = haber.IntroText;
            detailedHaber.FullHtmlText = haber.FullText;
            detailedHaber.FullText = HtmlParser.GetTextFromHtml(haber.IntroText);
            detailedHaber.Link = _urlRoutinManager.GetHaberDetailUrl(haber);
            detailedHaber.City.CityName = haber.City;
            detailedHaber.City.Link = (string.IsNullOrEmpty(haber.City))?null:_urlRoutinManager.GetHaberCityUrl(haber.City.ToAlias());
            detailedHaber.Town.Name = haber.Town;
            detailedHaber.MetaDescription = haber.MetaDescription;
            detailedHaber.MetaTitle = haber.MetaTitle;
            detailedHaber.MetaKeywords = haber.MetaKeywords;
            detailedHaber.Updated = haber.Updated;
            detailedHaber.Town.Link = (string.IsNullOrEmpty(haber.Town)) ? null : _urlRoutinManager.GetHaberTownUrl(haber.City.ToAlias(), haber.Town.ToAlias());
            detailedHaber.SpotBigTittle = haber.SpotBigTittle;
            detailedHaber.SpotSmallTitle=haber.SpotSmallTitle;
            string mediumImgUrl = _thumnailService.GetHaberThumbnailByType(haber, HaberThumbnailType.Medium);
            string smallImgUrl = _thumnailService.GetHaberThumbnailByType(haber, HaberThumbnailType.Small);
            string hightImgUrl = _thumnailService.GetHaberThumbnailByType(haber, HaberThumbnailType.High);
            detailedHaber.MediumThumbnail = (mediumImgUrl == null) ? haber.ImageUrl : mediumImgUrl;
            detailedHaber.SmallThumbnail = (smallImgUrl == null) ? haber.ImageUrl : smallImgUrl;
            detailedHaber.HighThumbnail = (hightImgUrl == null) ? haber.ImageUrl : hightImgUrl;
            detailedHaber.Tags = getDetailedTagList(haber);
            detailedHaber.Cateogory.Link = (haber.HaberCategory!=null)?_urlRoutinManager.GetHaberListUrl(haber.HaberCategory.Alias):null;
            detailedHaber.Cateogory.Name = (haber.HaberCategory!=null)?haber.HaberCategory.Title:null;
            return detailedHaber;
        }

        private List<TagDetail> getDetailedTagList(Haber haber)
        {
            if (haber.Tags.Count() <= 0)
                return null;
            List<TagDetail> detailedTagList = new List<TagDetail>();
            haber.Tags.ForEach(tag =>
            {
                detailedTagList.Add(_tagService.GetTagDetail(tag));
            });
            return detailedTagList;
        }

        public List<Haber> GetRelatedHabers(Haber haber)
        {
            List<Haber> haberList = _haberRepository.Habers.Where(x => x.Tags.Exists(tag => haber.Tags.Contains(tag) && x.Id != haber.Id)).OrderBy(x => x.Date).Take(6).ToList();
            if (haberList != null && haberList.Count() < 6)
            {
                List<Haber> cityRelatedHabers = _haberRepository.Habers.Where(x => x.City == haber.City && !haberList.Contains(x)).OrderBy(x => x.Date).Take(6 - haberList.Count()).ToList();
                cityRelatedHabers.ForEach(h =>
                {
                    haberList.Add(h);
                });

            }

            if (haberList == null)
            {
                haberList = _haberRepository.Habers.Where(x => x.City == haber.City && !haberList.Contains(x) && haber.Id == x.Id).OrderBy(x => x.Date).Take(6).ToList();
            }

            return haberList;
        }
        public IQueryable<Haber> GetPublishedCategoryHabersByCategoryAlias(string categoryAlias)
        {
            return _haberRepository.Habers.Where(x => x.HaberCategory.Alias == categoryAlias).Where(x=>x.State==true).OrderByDescending(x => x.Date).AsQueryable();
        }
        public IQueryable<Haber> FindAllPublishedHabers()
        {
            return _haberRepository.Habers.Where(x => x.State == true).OrderByDescending(x => x.Date).AsQueryable();
        }

        public IQueryable<Haber> FindAllHabersByTagAlias(string alias)
        {
            return _haberRepository.Habers.Where(x => x.Tags.Select(tag => tag.Alias).Contains(alias)).OrderByDescending(x => x.Date).AsQueryable();
        }
        public IQueryable<Haber> FindAllCityPublishedHabers(string CityName)
        {
            return _haberRepository.Habers.Where(x => x.State == true).Where(x => x.City.ToAlias() == CityName.ToAlias())
                .OrderByDescending(x => x.Date).AsQueryable();
        }
        public IQueryable<Haber> FindAllTownPublishedHabers(string TownName)
        {
            return _haberRepository.Habers.Where(x => x.State == true).Where(x => !string.IsNullOrEmpty(x.Town) && x.Town.ToAlias() == TownName.ToAlias())
                .OrderByDescending(x => x.Date).AsQueryable();
        }

        public IQueryable<Haber> GetFrontPageMansetHabers()
        {
            return _haberRepository.Habers.Where(x => x.ShowManset == true && x.State==true).OrderByDescending(x => x.Date).AsQueryable();
        }
        public IQueryable<Haber> GetFrontPageMainHabers()
        {
            return _haberRepository.Habers.Where(x => x.ShowFrontPageMain == true && x.State == true).OrderByDescending(x => x.Date).AsQueryable();
        }

        public IQueryable<Haber> GetFrontPageAltHabers()
        {
            return _haberRepository.Habers.Where(x => x.ShowFrontPageAlt == true && x.State == true).OrderByDescending(x => x.Date).AsQueryable();
        }

        public IQueryable<Haber> GetFrontPageUstHabers()
        {
            return _haberRepository.Habers.Where(x => x.ShowFrontPageUst == true).OrderByDescending(x => x.Date).AsQueryable();
        }

        public IQueryable<Haber> GetPopularHabers()
        {
            return _haberRepository.Habers.Where(x => x.State == true).OrderByDescending(x => x.Hits).AsQueryable();
        }

        public IQueryable<Haber> GetRecentHabers()
        {
            return _haberRepository.Habers.Where(x => x.State == true).OrderByDescending(x => x.Date).AsQueryable();
        }
    }
}

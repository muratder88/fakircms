﻿using System;
namespace FakirCms.Domain.Services
{
    public interface IImageEditorService
    {
        bool CanGenerateThumbnail(string filePath);
        string CreateThumbnail(string sourceImagePath, string destThumbsDir, string thumbFileName, System.Drawing.Size thumbSize, bool restrictWidth);
        bool CropImage(string sourceImagePath, System.Drawing.Point topLeft, System.Drawing.Size newSize);
        bool ResizeImage(string sourceImagePath, System.Drawing.Size newSize);
        bool RotateImage(string sourceImagePath, int rotationDegree);
        System.Collections.Generic.IEnumerable<string> SupportedExtensions { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Exceptions
{
    public class InsertDuplicateException : Exception
    {
        public InsertDuplicateException(string message)
            : base(message)
        {

        }
    }
}

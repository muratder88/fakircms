﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Enums
{
    public enum CategoryView
    {
        [Display(Name = "Bir Satırda 3 Makale")]
        Category3Column = 1,
        [Display(Name = "Bir Satırda 2 Makale")]
        Category2Column = 2,
        [Display(Name = "Bir Satırda iki Haber ve Resim Küçük")]
        Category2SmallImage = 3,
        [Display(Name = "Klasik giriş metnini göster")]
        CategoryBlog = 4
    }
}

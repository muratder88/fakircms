﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Enums
{
    public enum AccessTypes
    {
        [Display(Name = "Genel")]
        General = 1,
        [Display(Name = "Özel")]
        Special = 2,
        [Display(Name = "Kayıtlı")]
        Registered = 3
    }
}

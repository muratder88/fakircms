﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FakirCms.Domain.Helpers
{
    public static class HtmlParser
    {
        /// <summary>
        /// Gets first img value attribute, image not exist it return null
        /// </summary>
        /// <param name="text">This is html text</param>
        /// <returns>string img src value</returns>
        public static string GetFirstImageFromText(string text)
        {
            //Get Image Url From Content Text
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(text);
            HtmlNode node = null;
            if (doc.DocumentNode.SelectNodes("//img") != null)
            {
                node = doc.DocumentNode.SelectNodes("//img")[0];
            }
            if (node != null)
                return node.Attributes["src"].Value;
            else
                return null;
        }

        /// <summary>
        /// Get Inner text after comments removed
        /// </summary>
        /// <param name="htmlText">Html Content</param>
        /// <returns>string html innertext removed comments</returns>
        public static string GetTextFromHtml(string htmlText)
        {
            HtmlDocument doc = new HtmlDocument();

            //encede olmuş html düzelt
            htmlText = HttpUtility.HtmlDecode(htmlText);
            doc.LoadHtml(htmlText);

            //remove Comments
            if (doc.DocumentNode.SelectNodes("//comment()") != null)
            {
                foreach (HtmlNode comment in doc.DocumentNode.SelectNodes("//comment()"))
                {
                    comment.ParentNode.RemoveChild(comment);
                }
            }

            //Get Intro Content From FullText
            string fullText = doc.DocumentNode.InnerText;
            return fullText.Trim();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FakirCms.Domain.Helpers
{
    public static class StringHelpers
    {
        public static string ToAlias(this string str)
        {


            if (str == null)
            {
                return str;
            }
            //space convert -

            str = str.Trim().Replace(" ", "-");
            str = str.ToLower();
            //Turkçe karekterleri kaldır
            //Convert Characters
            str = str.Replace("ö", "o");
            str = str.Replace("ç", "c");
            str = str.Replace("ş", "s");
            str = str.Replace("ı", "i");
            str = str.Replace("ğ", "g");
            str = str.Replace("ü", "u");
            //remove non '" special chars
            string pattern = @"[^a-zA-Z0-9-]";
            string replacement = "";
            Regex rgx = new Regex(pattern);
            str = rgx.Replace(str, replacement);
            return str;
        }

        public static string ConverToFileName(this string str)
        {
            if (str == null)
            {
                return str;
            }
            //space convert -

            str = str.Trim().Replace(" ", "_");
            str = str.ToLower();
            //Turkçe karekterleri kaldır
            //Convert Characters
            str = str.Replace("ö", "o");
            str = str.Replace("ç", "c");
            str = str.Replace("ş", "s");
            str = str.Replace("ı", "i");
            str = str.Replace("ğ", "g");
            str = str.Replace("ü", "u");
            //remove non '" special chars
            string pattern = @"[^a-zA-Z0-9_]";
            string replacement = "";
            Regex rgx = new Regex(pattern);
            str = rgx.Replace(str, replacement);
            return str;
        }
    }
}

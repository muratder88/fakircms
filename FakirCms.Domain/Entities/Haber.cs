﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Entities
{
    public class Haber
    {
        //Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public string Alias { get; set; }
        public string IntroText { get; set; }
        public string FullText { get; set; }
        public DateTime Date { get; set; }
        public bool State { get; set; }
        public int Hits { get; set; }
        public string City { get; set; }
        public string Town { get; set; }
        public string ImageUrl { get; set; }
        public string HaberType { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }

        public bool ShowManset { get; set; }
        public bool ShowCategoryManset { get; set; }
        public bool ShowFrontPageMain { get; set; }
        public bool ShowFrontPageAlt { get; set; }
        public bool ShowFrontPageUst { get; set; }
        public string SpotBigTittle { get; set; }
        public string SpotSmallTitle { get; set; }


        //Seo Properties
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }

        //Foreign Keys
        public string CreatedByID { get; set; }
        public string UpdatedByID { get; set; }
        public int HaberCategoryID { get; set; }



        //Navigation properties
        public AppUser CreatedBy { get; set; }
        public AppUser UpdatedBy { get; set; }
        public HaberCategory HaberCategory { get; set; }
        public List<Tag> Tags { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Entities
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }

        //Seo Properties
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }

        //Foreign Keys
        public string CreatedByID { get; set; }
        public string UpdatedByID { get; set; }

        //Navigation properties
        public AppUser CreatedBy { get; set; }
        public AppUser UpdatedBy { get; set; }
        public List<Article> Articles { get; set; }
    }
}

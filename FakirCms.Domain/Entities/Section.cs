﻿using FakirCms.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Entities
{
    public class Section
    {
        public int SectionID { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public bool State { get; set; }
        public AccessTypes SectionAccess { get; set; }
    }
}

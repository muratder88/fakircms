﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Entities
{
    public class Tag
    {
        public int TagId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }

        public List<Post> Posts { get; set; }
        public List<Haber> Habers { get; set; }
        
    }
}

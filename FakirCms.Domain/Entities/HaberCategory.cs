﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Entities
{
    public class HaberCategory
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }
        
        //Foreign Keys
        public string CreatedByID { get; set; }
        public string UpdatedByID { get; set; }
        
        //Navigation properties
        public AppUser CreatedBy { get; set; }
        public AppUser UpdatedBy { get; set; }

        public List<Haber> Habers { get; set; }
    }
}

﻿using FakirCms.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Entities
{
    public class Post
    {
        //Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public string Alias { get; set; }
        public string IntroText { get; set; }
        public string FullText { get; set; }
        public int State { get; set; }
        public int Order { get; set; }
        public string MetaData { get; set; }
        public int Hits { get; set; }
        public AccessTypes Access { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }

        //Foreign Keys
        public string CreatedByID { get; set; }
        public string UpdatedByID { get; set; }
        public int CategoryID { get; set; }


        //Navigation properties
        public AppUser CreatedBy { get; set; }
        public AppUser UpdatedBy { get; set; }
        public List<Tag> Tags { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Entities
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Alias { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public int Hits { get; set; }
        public bool State { get; set; }

        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }


        //Seo Properties
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }

        //Foreign Keys
        public string CreatedByID { get; set; }
        public string UpdatedByID { get; set; }
        public int AuthorID { get; set; }

        //Navigation properties
        public AppUser CreatedBy { get; set; }
        public AppUser UpdatedBy { get; set; }
        public Author Author { get; set; }
    }
}

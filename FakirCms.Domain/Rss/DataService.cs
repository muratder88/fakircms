﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Rss.Model;
using FakirCms.Domain.Services;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using FakirCms.Domain.Helpers;
using FakirCms.Domain.Exceptions;

namespace FakirCms.Domain.Rss
{
    public class DataService : FakirCms.Domain.Rss.IDataService
    {
        private RssReader _rssReader;
        private HaberCrawler _haberCrawler;
        private IHaberService _haberService;
        private ITagService _tagService;
        private FileManager _fileManager;

        public DataService(IHaberService haberService,ITagService tagService)
        {
            _rssReader = new RssReader();
            _haberCrawler = new HaberCrawler();
            _haberService = haberService;
            _tagService = tagService;
            _fileManager = new FileManager();
        }

        public void AddHaberFromRssToDatabase(string rssUrl, int categoryId)
        {
            List<Feed> feeds = _rssReader.ReadRss(rssUrl);
            
            
            //get Feeds Details
            List<LinkItem> list = getLinkItems(feeds);


            ////add habers
            this.AddItemsToDatabase(list, categoryId);

        }


        private List<LinkItem> getLinkItems(List<Feed> feeds)
        {
            if (feeds == null)
                throw new ArgumentNullException("paramater not be null");
            List<LinkItem> list = new List<LinkItem>();
            foreach (var feed in feeds)
            {
                try
                {
                    LinkItem item = _haberCrawler.GetHaberFromLink(feed.Link);
                    list.Add(item);
                }
                catch (Exception ex)
                {

                }
            }
            return list;
        }

        private void AddItemsToDatabase(IEnumerable<LinkItem>items, int catid)
        {
            foreach (var item in items)
            {
                try
                {
                    this.AddItemToDatabase(item, catid);
                }
                catch (Exception ex)
                {

                }
            }
        }
        private void AddItemToDatabase(LinkItem item, int catId)
        {
            if (!ItemExistInDatabase(item, catId) && !string.IsNullOrEmpty(item.Image))
            {
                Haber haber = new Haber
                {
                    Title = item.Title,
                    IntroText = item.Description,
                    FullText = item.Content,
                    MetaDescription = (item.Description.Length > 160) ? item.Description.Substring(0, 160) : item.Description,
                    MetaKeywords = String.Join(",", item.Tags),
                    MetaTitle = (item.Title.Length > 70) ? item.Title.Substring(0, 70) : item.Title,
                    HaberCategoryID = catId,
                    Alias = item.Title.ToAlias(),
                    State = true,
                    Date = item.Date,
                    ImageUrl =_fileManager.DowloandImage(item.Image)
                };
                
                if (item.Tags != null && item.Tags.Count>0)
                {
                    List<Tag> taglist = new List<Tag>();
                    foreach (var tag in item.Tags)
                    {
                        try
                        {
                            _tagService.Add(new Tag() { Name = tag, Alias = tag.ToAlias() });
                        }
                        catch (InsertDuplicateException ex)
                        {

                        }

                        taglist.Add(_tagService.FindByAlias(tag.ToAlias()));
                    }
                    haber.Tags = taglist;
                }

                _haberService.Create(haber);

            }
        }

        public Boolean ItemExistInDatabase(LinkItem item, int catId)
        {
            int count = _haberService.items.Where(x => x.Title == item.Title && x.HaberCategoryID == catId).Count();
            if (count > 0)
                return true;
            else
                return false;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FakirCms.Domain.Rss
{
    public class FileManager
    {
        private static string root = "/uploads/haber/hurriyet";
        private string rootPath = HttpContext.Current.Server.MapPath(root);

        public FileManager()
        {

        }

        private string getUniqueName()
        {
            int i = 1;
            while (true)
            {
                i++;
                if (!File.Exists(rootPath + "/"+ i + ".jpg"))
                {
                    return i + ".jpg";
                }

            }
        }

        private void CreateDirectoryIfNotExist(string folderName)
        {

            if (!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }
        }

        /// <summary>
        /// It downloands image 
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="item"></param>
        /// <returns>Dowlanded image name</returns>
        public string DowloandImage(string imageLink)
        {
            //Folder kontrol et
            this.CreateDirectoryIfNotExist(rootPath);
            
            using (WebClient webClient = new WebClient())
            {
                string fileName = this.getUniqueName();
                webClient.DownloadFile(imageLink, rootPath + "/" + fileName);
                return root +"/"+fileName;
            }

        }
    }
}

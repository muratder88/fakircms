﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Rss.Model;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Rss
{
    public class HaberCrawler
    {
        public HaberCrawler(){

        }

        private string getMainImage(HtmlDocument doc)
        {
            if (doc == null)
                throw new ArgumentNullException("Document can not be null");
            HtmlNodeCollection spotNode = null;
            spotNode = doc.DocumentNode.SelectNodes("//img[@itemprop='image']");
            if (spotNode != null && spotNode.Count > 0)
            {
                string text = spotNode.ElementAt(0).Attributes["src"].Value;
                return text;
            }
            
            return null;
        }

        private string getDescription(HtmlDocument doc)
        {
            if (doc == null)
                throw new ArgumentNullException("Document can not be null");
            HtmlNodeCollection spotNode = null;
            spotNode = doc.DocumentNode.SelectNodes("//h2[@itemprop='description']");
            if (spotNode != null && spotNode.Count > 0)
            {
                string text = spotNode.ElementAt(0).InnerHtml;
                return text;
            }
            return null;
        }

        private string getTitle(HtmlDocument doc)
        {
            if (doc == null)
                throw new ArgumentNullException("Document can not be null");
            HtmlNodeCollection title = null;
            title = doc.DocumentNode.SelectNodes("//h1[@itemprop='name']");
            if (title != null && title.Count > 0)
            {
                string text = title.ElementAt(0).InnerHtml;
                return text;
            }
            return null;
        }
        
        private string getContent(HtmlDocument doc)
        {
            if (doc == null)
                throw new ArgumentNullException("Document can not be null");    
            HtmlNodeCollection detail = null;
            detail = doc.DocumentNode.SelectNodes("//div[@class='ctx_content']");
            if (detail != null && detail.Count > 0)
            {
                string text = detail.ElementAt(0).InnerHtml;
                return text;
            }
            return null;
        }
        private List<string> getTags(HtmlDocument doc)
        {
            if (doc == null)
                throw new ArgumentNullException("Document can not be null");
            HtmlNodeCollection tagNodes = null;
            tagNodes = doc.DocumentNode.SelectNodes("//a[@class='singleTag']");
            List<string> tags = null;
            if (tagNodes != null && tagNodes.Count > 0)
            {
                tags = new List<string>();
                foreach (var item in tagNodes)
                {
                    tags.Add(item.InnerText);
                }
            }
            return tags;
        }

        private DateTime getDate(HtmlDocument doc)
        {
            if (doc == null)
                throw new ArgumentNullException("Document can not be null");
            HtmlNodeCollection dateNode = null;
            dateNode = doc.DocumentNode.SelectNodes("//meta[@itemprop='datePublished']");
            if (dateNode != null && dateNode.Count > 0)
            {
                string text = dateNode.ElementAt(0).Attributes["content"].Value;
                return DateTime.Parse(text);
            }
            
            return DateTime.Now;
        }
        public LinkItem GetHaberFromLink(string link)
        {
            Uri url = new Uri(link);
            WebClient client = new WebClient();
            client.Encoding = Encoding.GetEncoding("ISO-8859-9");
            string html = client.DownloadString(url); // html kodları indiriyoruz. 

            HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(html); // html kodlarını bir HtmlDocment nesnesine yüklüyoruz. 

            LinkItem haber = new LinkItem();
            haber.Title = getTitle(document);
            haber.Image = getMainImage(document);
            haber.Description = getDescription(document);
            haber.Content = getContent(document);
            haber.Tags = getTags(document);
            haber.Date = getDate(document);
            return haber;
        }
    }
}

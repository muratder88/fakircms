﻿using System;
namespace FakirCms.Domain.Rss
{
    public interface IDataService
    {
        void AddHaberFromRssToDatabase(string rssUrl, int categoryId);
    }
}

﻿using FakirCms.Domain.Rss.Model;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FakirCms.Domain.Rss
{
    public class RssReader
    {

        public RssReader()
        {

        }

        public List<Feed> ReadRss(string rssUrl)
        {
            Uri url = new Uri(rssUrl);
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            string html = client.DownloadString(url); // html kodları indiriyoruz. 
            XDocument xml = XDocument.Parse(html);

            var feedList = (from story in xml.Descendants("item")
                             select new Feed
                             {
                                 Title = ((string)story.Element("title")),
                                 Link = ((string)story.Element("link")),
                                 Description = ((string)story.Element("description")),
                                 PubDate = ((string)story.Element("pubDate"))
                             }).ToList();
            return feedList;
        }
    }
}

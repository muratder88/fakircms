﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Rss.Model
{

    public class LinkItem
    {
        public string Title { get; set; }
        public string Image { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public List<string> Tags { get; set; }
    }
}

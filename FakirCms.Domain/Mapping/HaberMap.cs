﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.ClaimMap
{
    public class HaberMap : EntityTypeConfiguration<Haber>
    {
        public HaberMap()
        {
            HasKey(h => h.Id).Property(h => h.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Title).IsRequired();
            Property(x => x.Alias).IsRequired();
            Property(x => x.ImageUrl).IsRequired();

            HasRequired(h => h.HaberCategory).WithMany(x => x.Habers)
                .HasForeignKey(h => h.HaberCategoryID).WillCascadeOnDelete(false);

            Property(h => h.SpotBigTittle).HasMaxLength(70);
            Property(h => h.SpotSmallTitle).HasMaxLength(90);
            Property(x => x.MetaTitle).HasMaxLength(70);
            Property(x => x.MetaDescription).HasMaxLength(160);
            HasMany<Tag>(h => h.Tags)
                .WithMany(t => t.Habers).Map(ht =>
                {
                    ht.MapLeftKey("HaberId");
                    ht.MapRightKey("TagId");
                    ht.ToTable("haber_tags");
                });

        }
    }
}

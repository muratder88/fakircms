﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using FakirCms.Domain.Entities;

namespace FakirCms.Domain.ClaimMap
{
    public class PostMap:EntityTypeConfiguration<Post>
    {
        public PostMap()
        {
            //Post
           this.HasKey(p => p.Id);
           this.HasMany<Tag>(p => p.Tags)
                .WithMany(c => c.Posts).Map(pt =>
                {
                    pt.MapLeftKey("PostId");
                    pt.MapRightKey("TagId");
                    pt.ToTable("post_tags");
                });
        }
    }
}

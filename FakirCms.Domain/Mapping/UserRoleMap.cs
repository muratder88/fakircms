﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using FakirCms.Domain.Entities;
namespace FakirCms.Domain.Mapping
{
    public class UserRoleMap:EntityTypeConfiguration<AppRole>
    {
        public UserRoleMap()
        {
            ToTable("Role");
            HasKey(x => x.Id);
            Property(x => x.Id).HasMaxLength(128);
            Property(x => x.Name).HasMaxLength(256);
        }
    }
}

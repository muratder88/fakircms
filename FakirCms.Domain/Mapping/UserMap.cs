﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakirCms.Domain.Entities;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
namespace FakirCms.Domain.Mapping
{
    public class UserMap : EntityTypeConfiguration<AppUser>
    {          
        //modelBuilder.Entity().HasMany(c => c.Logins).WithOptional().HasForeignKey(c => c.UserId);
        //    modelBuilder.Entity().HasMany(c => c.Claims).WithOptional().HasForeignKey(c => c.UserId);
        //    modelBuilder.Entity().HasMany(c => c.Roles).WithRequired().HasForeignKey(c => c.UserId);
        public UserMap()
        {

            HasKey(u => u.Id);
            Property(u => u.Id).HasMaxLength(128).IsRequired();
            ToTable("Users");
            Property(u => u.UserName).HasMaxLength(25);
            Property(u => u.Email).HasMaxLength(256);
            
            HasMany(u => u.Logins).WithOptional().HasForeignKey(l => l.UserId);
            HasMany(u => u.Claims).WithOptional().HasForeignKey(c => c.UserId);
            HasMany<AppRole>(u=>u.Roles)
                 .WithMany(c => c.Users)
                 .Map(cs =>
                 {
                     cs.MapLeftKey("UserId");
                     cs.MapRightKey("RoleId");
                     cs.ToTable("UserRole");
                 });
        }
    }
}

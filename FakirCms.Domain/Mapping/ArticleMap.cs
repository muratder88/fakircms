﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using FakirCms.Domain.Entities;

namespace FakirCms.Domain.Mapping
{
    public class ArticleMap:EntityTypeConfiguration<Article>
    {
        public ArticleMap()
        {
            HasKey(x => x.Id);
            Property(x => x.Title).IsRequired();
            Property(x => x.Alias).IsRequired();
            Property(x => x.Content).IsRequired();

            Property(x => x.MetaTitle).HasMaxLength(70);
            Property(x => x.MetaDescription).HasMaxLength(160);
        }
    }
}

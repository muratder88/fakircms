﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakirCms.Domain.Entities;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
namespace FakirCms.Domain.Mapping
{
    public class UserLoginMap:EntityTypeConfiguration<UserLogin>
    {

        public UserLoginMap()
        {
            ToTable("UserLogin");
            HasKey(p => new { p.LoginProvider, p.ProviderKey, p.UserId });

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using FakirCms.Domain.Entities;

namespace FakirCms.Domain.Mapping
{
    public class AuthorMap:EntityTypeConfiguration<Author>
    {
        public AuthorMap()
        {
            HasKey(x => x.Id);
            Property(x => x.Name).IsRequired();
            Property(x => x.Alias).IsRequired();
            Property(x => x.Image).IsRequired();

            Property(x => x.MetaTitle).HasMaxLength(70);
            Property(x => x.MetaDescription).HasMaxLength(160);
        }
    }
}

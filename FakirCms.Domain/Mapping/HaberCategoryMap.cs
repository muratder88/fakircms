﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using FakirCms.Domain.Entities;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FakirCms.Domain.Mapping
{
    public class HaberCategoryMap:EntityTypeConfiguration<HaberCategory>
    {
        public HaberCategoryMap()
        {
            HasKey(x => x.Id);
            Property(x => x.Title).IsRequired();
            Property(x => x.Alias).IsRequired();
            Property(x => x.Title).HasMaxLength(70);
            Property(x => x.Description).HasMaxLength(160);
            Property(x => x.Alias).IsRequired().HasMaxLength(70)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(
                    new IndexAttribute("IX_Alias", 1) { IsUnique = true }));
        }
    }
}

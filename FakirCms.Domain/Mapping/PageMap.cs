﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using FakirCms.Domain.Entities;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FakirCms.Domain.ClaimMap
{
    public class PageMap : EntityTypeConfiguration<Page>
    {
        public PageMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.MetaTitle).HasMaxLength(70);
            this.Property(p => p.MetaDescription).HasMaxLength(160);
            this.Property(p => p.Title).HasMaxLength(70);
            this.Property(p => p.Alias).IsRequired().HasMaxLength(70)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(
                    new IndexAttribute("IX_Alias", 1) { IsUnique = true }));
        }
    }
}

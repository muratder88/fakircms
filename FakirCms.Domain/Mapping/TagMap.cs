﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using FakirCms.Domain.Entities;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FakirCms.Domain.ClaimMap
{
    public class TagMap:EntityTypeConfiguration<Tag>
    {
        public TagMap()
        {
            //Tag
            this.HasKey(t => t.TagId);
            this.Property(t => t.Alias).HasMaxLength(70)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(
                    new IndexAttribute("IX_Alias", 1) { IsUnique = true }));
        }
    }
}

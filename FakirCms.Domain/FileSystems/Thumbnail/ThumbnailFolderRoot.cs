﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace FakirCms.Domain.FileSystems.Thumbnail
{
    public class ThumbnailFolder : IThumbnailFolder
    {

        private IList<string> _supportedExtensions;
        private static object _mutex = new object();

        public ThumbnailFolder()
        {
        }

        public IEnumerable<string> SupportedExtensions
        {
            get
            {
                if (_supportedExtensions == null)
                {
                    lock (_mutex)
                    {
                        // cache supported extensions
                        if (_supportedExtensions == null)
                            _supportedExtensions = ImageCodecInfo.GetImageEncoders().SelectMany(
                                x => x.FilenameExtension.Replace("*.", string.Empty).ToLowerInvariant().Split(';')).ToList();
                    }
                }
                return _supportedExtensions;
            }
        }

        public string RootFolder
        {
            get
            {
                return "~/thumbnail";
            }
        }

        public string RootPath
        {
            get { return HostingEnvironment.MapPath(RootFolder); }
        }

        private ImageCodecInfo getEncoder(string fileExt, out EncoderParameters encParams)
        {
            // setup standard params (high quality = 90%)
            encParams = new EncoderParameters(1);
            encParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90L);

            return ImageCodecInfo.GetImageEncoders().First(info =>
                        info.FilenameExtension.IndexOf(fileExt, StringComparison.OrdinalIgnoreCase) > -1);
        }

        public bool CanGenerateThumbnail(string filePath)
        {

            string fileExt = Path.GetExtension(filePath).ToLowerInvariant().Trim('.');
            return SupportedExtensions.Contains(fileExt);
        }
        public string CreateThumbnail(string sourceImagePath, string destinationPathDir, string thumbFileName,
            System.Drawing.Size thumbSize, bool restrictWidth)
        {
            string destThumbsDir = CombineToPhysicalPath(destinationPathDir);
            try
            {
                string fileExt = Path.GetExtension(sourceImagePath).ToLowerInvariant().Trim('.');

                if (destThumbsDir.Last() != Path.DirectorySeparatorChar)
                    destThumbsDir += Path.DirectorySeparatorChar;

                string outputFileName = thumbFileName + "." + fileExt;
                string outputPath = destThumbsDir + outputFileName;

                if (File.Exists(outputPath))
                    return null;

                using (Bitmap inputImage = new Bitmap(sourceImagePath))
                {
                    EncoderParameters encParams = null;
                    var encoder = getEncoder(fileExt, out encParams);

                    try
                    {
                        if (restrictWidth)
                        {
                            float scale = thumbSize.Width / (float)inputImage.Width;
                            Size outSize = new Size(thumbSize.Width, (int)Math.Floor(inputImage.Height * scale));
                            using (Bitmap outBmp = new Bitmap(outSize.Width, outSize.Height))
                            {
                                using (Graphics g = Graphics.FromImage(outBmp))
                                {
                                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
                                    g.DrawImage(inputImage, 0, 0, outSize.Width, outSize.Height);
                                }
                                outBmp.Save(outputPath, encoder, encParams);
                            }
                        }
                        else
                        {
                            // first rescale image to aspect ratio
                            float wFactor = inputImage.Width / (float)thumbSize.Width;
                            float hFactor = inputImage.Height / (float)thumbSize.Height;
                            float minFactor = Math.Min(wFactor, hFactor);
                            Size tempSize = new Size((int)Math.Round(thumbSize.Width * minFactor),
                                        (int)Math.Round(thumbSize.Height * minFactor));

                            Rectangle clipRectangle = new Rectangle(
                                        (inputImage.Width - tempSize.Width) / 2,
                                       (inputImage.Height - tempSize.Height) / 2,
                                       tempSize.Width, tempSize.Height);

                            using (Bitmap tempBmp = new Bitmap(tempSize.Width, tempSize.Height))
                            {
                                // clip image
                                tempBmp.SetResolution(inputImage.HorizontalResolution, inputImage.VerticalResolution);
                                using (Graphics g = Graphics.FromImage(tempBmp))
                                {
                                    g.DrawImage(inputImage, 0, 0, clipRectangle, GraphicsUnit.Pixel);
                                }
                                // resize bitmap
                                using (Bitmap resBitmap = new Bitmap(thumbSize.Width, thumbSize.Height))
                                {
                                    using (Graphics g = Graphics.FromImage(resBitmap))
                                    {
                                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
                                        g.DrawImage(tempBmp, 0, 0, thumbSize.Width, thumbSize.Height);
                                    }
                                    resBitmap.Save(outputPath, encoder, encParams);
                                }
                            }
                        }
                    }
                    catch
                    {
                        return null;
                    }

                }
                return outputPath;
            }
            catch
            {
                return null;
            }
        }
        private string CombineToPhysicalPath(params string[] paths)
        {
            string firstCombine = (Path.Combine(paths));
            if (firstCombine.StartsWith(RootFolder))
            {
                firstCombine = firstCombine.Replace(RootFolder, "");
            }

            if (firstCombine.StartsWith("/"))
            {
                firstCombine = firstCombine.Remove(0, 1);
            }
            string result = Path.Combine(RootPath, firstCombine).Replace('/', Path.DirectorySeparatorChar);
            return result;
        }

        public IEnumerable<string> ListFiles(string path)
        {
            var directoryPath = CombineToPhysicalPath(path);
            if (!Directory.Exists(directoryPath))
                return Enumerable.Empty<string>();

            var files = Directory.GetFiles(directoryPath);
            return files.Select(file =>
            {
                var fileName = Path.GetFileName(file);
                return Combine(path, fileName);
            });

        }

        public IEnumerable<string> ListDirectories(string path)
        {
            var directoryPath = CombineToPhysicalPath(path);
            if (!Directory.Exists(directoryPath))
                return Enumerable.Empty<string>();

            var directories = Directory.GetFiles(directoryPath);
            return directories.Select(file =>
            {
                var fileName = Path.GetFileName(file);
                return Combine(path, fileName);
            });
        }

        public string Combine(params string[] paths)
        {
            return Path.Combine(paths).Replace(Path.DirectorySeparatorChar, '/');
        }

        public bool FileExists(string path)
        {
            return File.Exists(CombineToPhysicalPath(path));
        }
        public void StoreFile(string sourceFileName, string destinationPath)
        {
            //Logger.Information("Storing file \"{0}\" as \"{1}\" in \"App_Data\" folder", sourceFileName, destinationPath)
            var destinationFileName = CombineToPhysicalPath(destinationPath);
            File.Copy(sourceFileName, destinationFileName, true);

            throw new NotImplementedException();
        }

        public void DeleteFile(string path)
        {
            //Logger.Information("Deleting file \"{0}\" from \"App_Data\" folder", path);
            try
            {
                File.Delete(CombineToPhysicalPath(path));
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("File is \"{0}\" not deleted.Occurs problems", path));
            }
           
        }

        public void DeleteDirectory(string path)
        {
            Directory.Delete(CombineToPhysicalPath(path));
        }

        public DateTime GetFileLastWriteTimeUtc(string path)
        {
            return File.GetLastWriteTimeUtc(CombineToPhysicalPath(path));
        }

        public void CreateDirectory(string path)
        {
            Directory.CreateDirectory(CombineToPhysicalPath(path));
        }

        public bool DirectoryExists(string path)
        {
            return Directory.Exists(CombineToPhysicalPath(path));
        }

        public string MapPath(string path)
        {
            return CombineToPhysicalPath(path);
        }

        public string GetVirtualPath(string path)
        {
            return Combine(RootFolder, path);
        }

        public string GetImageUrlFromFullPath(string path)
        {
            if (path.Contains(RootPath))
            {
                string result = path.Replace(RootPath, "");
                result = result.Replace(Path.DirectorySeparatorChar, '/');
                if (result.StartsWith("/"))
                {
                    result = result.Remove(0, 1);
                }
                result = GetVirtualPath(result);
                return result;
            }
            else
            {
                return null;
            }
        }
        public bool CheckThubmnailPermission()
        {
            bool hasWriteAccess = true;
            string dir = "denemexyz";
            try
            {
                CreateDirectory(dir);
            }
            catch (Exception ex)
            {
                hasWriteAccess = false;
            }
            if (DirectoryExists(dir))
            {
                DeleteDirectory(dir);
            }
            return hasWriteAccess;
        }
    }
}

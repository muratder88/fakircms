﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace FakirCms.Domain.FileSystems.Thumbnail
{
    public class ThumbnailFolderRoot : IThumbnailFolderRoot
    {
        public string RootPath
        {
            get { return "~/thumbnail"; }
        }

        public string RootFolder
        {
            get { return HostingEnvironment.MapPath(RootFolder); }
        }
    }
}

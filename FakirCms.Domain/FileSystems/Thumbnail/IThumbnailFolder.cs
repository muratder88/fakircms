﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.FileSystems.Thumbnail
{
    /// <summary>
    /// Abstraction of Thumbnail folder. All virtual paths passed in or returned are relative to "~/thumbnail". 
    /// Expected to work on physical filesystem, but decouples core system from web hosting apis
    /// </summary>
    public interface IThumbnailFolder
    {
        IEnumerable<string> ListFiles(string path);
        IEnumerable<string> ListDirectories(string path);

        string Combine(params string[] paths);

        bool FileExists(string path);


        void StoreFile(string sourceFileName, string destinationPath);
        void DeleteFile(string path);

        DateTime GetFileLastWriteTimeUtc(string path);

        void CreateDirectory(string path);
        bool DirectoryExists(string path);



        string MapPath(string path);
        string GetVirtualPath(string path);

        string GetImageUrlFromFullPath(string path);

        string CreateThumbnail(string sourceImagePath, string destinationPathDir, string thumbFileName,
            System.Drawing.Size thumbSize, bool restrictWidth);
        bool CanGenerateThumbnail(string filePath);
        bool CheckThubmnailPermission();
    }
}

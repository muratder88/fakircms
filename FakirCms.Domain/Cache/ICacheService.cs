﻿using System;
namespace FakirCms.Domain.Cache
{
    interface ICacheService
    {
        T Get<T>(string key);
        T Get<T>(string key, TimeSpan expiration, Func<T> getCacheObject);
        void Remove(string key);
        void Set<T>(string key, T obj, TimeSpan expiration);
    }
}

﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.ClaimMap;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using MySql.Data.Entity;
using FakirCms.Domain.Mapping;


namespace FakirCms.Domain
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class EFDbFakirCms :DbContext
    {
        public EFDbFakirCms()
            : base("EFDbFakirCms")
        {
            //For Migrations
            //SetSqlGenerator("MySql.Data.MySqlClient", new MySql.Data.Entity.MySqlMigrationSqlGenerator());
            Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Haber> Haberler { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<HaberCategory> HaberCategories { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Article> Articles { get; set; }

        //User Management
        public DbSet<AppUser> Users { get; set; }
        public DbSet<AppRole> Roles { get; set; }
        public DbSet<UserClaim> UserClaims { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }

        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            this.Configuration.LazyLoadingEnabled = false;
            modelBuilder.Configurations.Add(new HaberMap());
            modelBuilder.Configurations.Add(new HaberCategoryMap());
            modelBuilder.Configurations.Add(new PageMap());
            modelBuilder.Configurations.Add(new PostMap());
            modelBuilder.Configurations.Add(new TagMap());
            modelBuilder.Configurations.Add(new AuthorMap());
            modelBuilder.Configurations.Add(new ArticleMap());

            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserRoleMap());
            modelBuilder.Configurations.Add(new UserLoginMap());
            modelBuilder.Configurations.Add(new UserClaimMap());


        }
    }

   
}

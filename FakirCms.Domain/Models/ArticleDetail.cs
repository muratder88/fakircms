﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Models
{
    public class ArticleDetail
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Alias { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public int Hits { get; set; }
        public AuthorDetail Author { get; set; }
        public DateTime Updated { get; set; }
        public string Link { get; set; }


        //Seo Properties
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }

       
    }
}

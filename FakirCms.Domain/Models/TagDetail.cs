﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Models
{
    public class TagDetail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string HaberLink { get; set; }
        public string AciklamaLink { get; set; }

    }
}

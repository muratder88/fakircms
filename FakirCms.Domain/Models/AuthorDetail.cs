﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakirCms.Domain.Entities;

namespace FakirCms.Domain.Models
{
    public class AuthorDetail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public DateTime Updated { get; set; }
        public string Link { get; set; }
        public string LastArticleTitle { get; set; }
        public string LastArticleLink { get; set; }
        public DateTime LastArticleDate { get; set; }

        //Seo Properties
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Models
{
    public class HaberDetail
    {
        public HaberDetail()
        {
            City = new CityDetail();
            Town = new TownDetail();
            Cateogory = new CategoryDetail();
        }
        public int id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Image { get; set; }
        public bool PageBreakExist { get; set; }
        public string IntroHtmlText { get; set; }
        public string FullHtmlText { get; set; }
        public string IntroText { get; set; }
        public string FullText { get; set; }
        public bool ImageExist { get; set; }
        public CityDetail City { get; set; }
        public TownDetail Town { get; set; }
        public DateTime Date { get; set; }
        public DateTime Updated { get; set; }
        public string MediumThumbnail { get; set; }
        public string HighThumbnail { get; set; }
        public string SmallThumbnail { get; set; }
        public List<TagDetail> Tags { get; set; }
        public CategoryDetail Cateogory { get; set; }

        public string SpotBigTittle { get; set; }
        public string SpotSmallTitle { get; set; }
        
        //Seo Properties
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
    }
    public class TownDetail
    {
        public string Name { get; set; }
        public string Link { get; set; }
    }
    public class CityDetail
    {
        public string CityName { get; set; }
        public string Link { get; set; }
    }

    public class CategoryDetail
    {
        public string Name { get; set; }
        public string Link { get; set; }
    }

}

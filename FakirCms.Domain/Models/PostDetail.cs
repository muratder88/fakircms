﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Models
{
    public class PostDetail
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Image { get; set; }
        public bool PageBreakExist { get; set; }
        public string IntroHtmlText { get; set; }
        public string FullHtmlText { get; set; }
        public string IntroText { get; set; }
        public string FullText { get; set; }
        public bool ImageExist { get; set; }

        public DateTime Created { get; set; }
        public string UserName { get; set; }
        public int CommentCount { get; set; }

        public AppUser CreatedBy { get; set; }

    }
}

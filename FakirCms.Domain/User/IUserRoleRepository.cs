﻿using System;
namespace FakirCms.Domain.User
{
    public interface IUserRoleRepository
    {
        int Delete(string userId);
        System.Collections.Generic.List<string> FindByUserId(string userId);
        int Insert(FakirCms.Domain.Entities.AppUser user, string roleId);
        int DeleteUserRole(string userId, string roleId);
    }
}

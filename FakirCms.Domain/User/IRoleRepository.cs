﻿
using System;
using System.Linq;
namespace FakirCms.Domain.User
{
    public interface IRoleRepository
    {
        int Delete(string roleId);
        FakirCms.Domain.Entities.AppRole GetRoleById(string roleId);
        FakirCms.Domain.Entities.AppRole GetRoleByName(string roleName);
        string GetRoleId(string roleName);
        string GetRoleNAme(string roleId);
        int Insert(FakirCms.Domain.Entities.AppRole role);
        int Update(FakirCms.Domain.Entities.AppRole role);
        IQueryable<FakirCms.Domain.Entities.AppRole> Roles { get; }
    }
}

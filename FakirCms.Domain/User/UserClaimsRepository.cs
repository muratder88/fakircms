﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FakirCms.Domain.Entities;

namespace FakirCms.Domain.User
{
    public class UserClaimsRepository : FakirCms.Domain.User.IUserClaimsRepository
    {
        private EFDbFakirCms _context;
        public UserClaimsRepository(EFDbFakirCms context)
        {
            _context = context;
        }

        public ClaimsIdentity FindByUserId(string userId)
        {
            ClaimsIdentity claims = new ClaimsIdentity();
            List<UserClaim> rows= _context.UserClaims.ToList();
            rows.ForEach(userClaim =>
            {
                Claim claim = new Claim(userClaim.ClaimType, userClaim.ClaimValue);
                claims.AddClaim(claim);
            });

            return claims;
        }

        public int Delete(string userId)
        {
            UserClaim userClaim = _context.UserClaims.Where(x => x.UserId == userId).FirstOrDefault();
            if (userClaim != null)
            {
                _context.UserClaims.Remove(userClaim);
                return _context.SaveChanges();
            }
            return -1;
        }

        public int Insert(Claim userClaim, string userId)
        {
            UserClaim claim=new UserClaim(){
                ClaimType=userClaim.Type,
                ClaimValue=userClaim.Value,
                UserId=userId
            };

            _context.UserClaims.Add(claim);
            return _context.SaveChanges();
        }

        public int Delete(AppUser user, Claim claim)
        {
            UserClaim userClaim=_context.UserClaims.Where(x => x.UserId == user.Id && x.ClaimValue == claim.Value).FirstOrDefault();
            _context.UserClaims.Remove(userClaim);
            return _context.SaveChanges();
        }
    }
}

﻿using System;
namespace FakirCms.Domain.User
{
    public interface IUserClaimsRepository
    {
        int Delete(FakirCms.Domain.Entities.AppUser user, System.Security.Claims.Claim claim);
        int Delete(string userId);
        System.Security.Claims.ClaimsIdentity FindByUserId(string userId);
        int Insert(System.Security.Claims.Claim userClaim, string userId);
    }
}

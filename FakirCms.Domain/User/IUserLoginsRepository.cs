﻿using System;
namespace FakirCms.Domain.User
{
    public interface IUserLoginsRepository
    {
        int Delete(FakirCms.Domain.Entities.AppUser user, Microsoft.AspNet.Identity.UserLoginInfo login);
        int Delete(string userId);
        System.Collections.Generic.List<Microsoft.AspNet.Identity.UserLoginInfo> FindByUserId(string userId);
        string FindUserIdByLogin(Microsoft.AspNet.Identity.UserLoginInfo userLogin);
        int Insert(FakirCms.Domain.Entities.AppUser user, Microsoft.AspNet.Identity.UserLoginInfo login);
    }
}

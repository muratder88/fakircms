﻿using FakirCms.Domain.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.User
{
    public class UserLoginsRepository : FakirCms.Domain.User.IUserLoginsRepository
    {
        private EFDbFakirCms _context;
        public UserLoginsRepository(EFDbFakirCms context)
        {
            _context = context;
        }
        /// <summary>
        /// Deletes a login from a user in the UserLogins table
        /// </summary>
        /// <param name="user">User to have login deleted</param>
        /// <param name="login">Login to be deleted from user</param>
        /// <returns></returns>
        public int Delete(AppUser user, UserLoginInfo login)
        {
           UserLogin userLogin= _context.UserLogins.Where(x => x.UserId == user.Id && x.LoginProvider == login.LoginProvider && x.ProviderKey == login.ProviderKey).FirstOrDefault();
           if (login != null)
           {
               _context.UserLogins.Remove(userLogin);
               return _context.SaveChanges();
           }
           return -1;
        }

        /// <summary>
        /// Deletes all Logins from a user in the UserLogins table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public int Delete(string userId)
        {

           IEnumerable<UserLogin> list= _context.UserLogins.Where(x => x.UserId == userId);
           foreach (var userLogin in list)
           {
               _context.UserLogins.Remove(userLogin);
           }

           return _context.SaveChanges();
        }

        /// <summary>
        /// Inserts a new login in the UserLogins table
        /// </summary>
        /// <param name="user">User to have new login added</param>
        /// <param name="login">Login to be added</param>
        /// <returns></returns>
        public int Insert(AppUser user, UserLoginInfo login)
        {
            UserLogin userLogin = new UserLogin()
            {
                LoginProvider = login.LoginProvider,
                ProviderKey = login.ProviderKey,
                UserId = user.Id
            };
            _context.UserLogins.Add(userLogin);

            return _context.SaveChanges();
        }

        /// <summary>
        /// Return a userId given a user's login
        /// </summary>
        /// <param name="userLogin">The user's login info</param>
        /// <returns></returns>
        public string FindUserIdByLogin(UserLoginInfo userLogin)
        {
            return _context.UserLogins.Where(x => x.LoginProvider == userLogin.LoginProvider && x.ProviderKey == userLogin.ProviderKey)
                .Select(x=>x.UserId).FirstOrDefault();
        }

        /// <summary>
        /// Returns a list of user's logins
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public List<UserLoginInfo> FindByUserId(string userId)
        {
            List<UserLoginInfo> logins = new List<UserLoginInfo>();
            var rows = _context.UserLogins.Where(x => x.UserId == userId).ToList();
            foreach (var row in rows)
            {
                var login = new UserLoginInfo(row.LoginProvider,row.ProviderKey);
                logins.Add(login);
            }

            return logins;
        }
    }
}

﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.User
{
    public class RoleRepository : FakirCms.Domain.User.IRoleRepository
    {
        private EFDbFakirCms _context;
        public RoleRepository(EFDbFakirCms context)
        {
            _context = context;
        }
        public IQueryable<AppRole> Roles
        {
            get { return _context.Roles; }
        }

        public int Delete(string roleId)
        {
            _context.Roles.Where(x => x.Id == roleId);
            return _context.SaveChanges();
        }

        public int Insert(AppRole role)
        {
            _context.Roles.Add(role);
            return _context.SaveChanges();
        }

        public string GetRoleNAme(string roleId)
        {
           return  _context.Roles.Where(x => x.Id == roleId).Select(x => x.Name).FirstOrDefault();
        }

        public string GetRoleId(string roleName)
        {
            return _context.Roles.Where(x => x.Name == roleName).Select(x=>x.Id).FirstOrDefault();
        }

        public AppRole GetRoleById(string roleId)
        {
            return _context.Roles.Where(x => x.Id == roleId).FirstOrDefault();
        }

        public AppRole GetRoleByName(string roleName)
        {
            return _context.Roles.Where(x => x.Name == roleName).FirstOrDefault();
        }

        public int Update(AppRole role)
        {
            _context.Entry(role).State = EntityState.Modified;
            return _context.SaveChanges();
        }
    }
}

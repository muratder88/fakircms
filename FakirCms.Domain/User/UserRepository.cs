﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.User
{
    public class UserRepository<TUser> : FakirCms.Domain.User.IUserRepository<TUser>
        where TUser : AppUser
    {
        private EFDbFakirCms _context;
        public UserRepository(EFDbFakirCms context)
        {
            _context = context;
        }

        public IQueryable<AppUser> Users { get { return _context.Users; } }
        public string GetUserName(string userId)
        {
            return _context.Users.Where(x => x.Id == userId).Select(x => x.UserName).FirstOrDefault();
        }

        public string GetUserId(string userName)
        {
            return _context.Users.Where(x => x.UserName == userName).Select(x => x.Id).FirstOrDefault();
        }

        public TUser GetUserById(string userId)
        {
           return  (TUser)_context.Users.Where(x => x.Id == userId).FirstOrDefault();
        }
        public List<TUser> GetUserByName(string userName)
        {
            return _context.Users.Where(x => x.UserName == userName).ToList() as List<TUser>;
        }

        public List<TUser> GetUserByEmail(string email)
        {
            return null;
        }
        public string GetPasswordHash(string userId)
        {     

            return _context.Users.Where(x=>x.Id==userId).Select(x=>x.PasswordHash).FirstOrDefault();
        }

        public int SetPasswordHash(string userId, string passwordHash)
        {
           TUser user= _context.Users.Where(x => x.Id == userId).FirstOrDefault() as TUser;
           if (user == null)
               throw new Exception("Kullanıcı Bulunamadı");
           user.PasswordHash = passwordHash;
           return Update(user);
        }

        public string GetSecurityStamp(string userId)
        {
            return _context.Users.Where(x => x.Id == userId).Select(x => x.SecurityStamp).FirstOrDefault();
        }

        public int Insert(TUser user)
        {
            _context.Users.Add(user);
            return _context.SaveChanges();
        }

        public int Delete(string userId)
        {
           TUser user= GetUserById(userId);
           _context.Users.Remove(user);
           return _context.SaveChanges();
        }
        public int Delete(TUser user)
        {
            return Delete(user.Id);
        }

        public int Update(TUser user)
        {
            _context.Entry(user).State = EntityState.Modified;
            return _context.SaveChanges();
        }
    }
}

﻿using System;
using System.Linq;
namespace FakirCms.Domain.User
{
    public interface IUserRepository<TUser>
     where TUser : FakirCms.Domain.Entities.AppUser
    {
        IQueryable<FakirCms.Domain.Entities.AppUser> Users { get; }
        int Delete(string userId);
        int Delete(TUser user);
        string GetPasswordHash(string userId);
        string GetSecurityStamp(string userId);
        System.Collections.Generic.List<TUser> GetUserByEmail(string email);
        TUser GetUserById(string userId);
        System.Collections.Generic.List<TUser> GetUserByName(string userName);
        string GetUserId(string userName);
        string GetUserName(string userId);
        int Insert(TUser user);
        int SetPasswordHash(string userId, string passwordHash);
        int Update(TUser user);
    }
}

﻿using FakirCms.Domain.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.User
{
   public class UserStore<TUser> : IUserLoginStore<TUser>,
        IUserClaimStore<TUser>,
        IUserRoleStore<TUser>,
        IUserPasswordStore<TUser>,
        IUserSecurityStampStore<TUser>,
        IQueryableUserStore<TUser>,
        IUserEmailStore<TUser>,
        IUserPhoneNumberStore<TUser>,
        IUserTwoFactorStore<TUser, string>,
        IUserLockoutStore<TUser, string>,
        IUserStore<TUser>
        where TUser : AppUser
   {
       private IRoleRepository _roleRepository;
       private IUserClaimsRepository _userClaimRepository;
       private IUserLoginsRepository _userLoginRepository;
       private IUserRepository<AppUser> _userRepository;
       private IUserRoleRepository _userRoleRepository;
       public UserStore(IRoleRepository roleRepository, IUserClaimsRepository userClaimRepository,
           IUserLoginsRepository userLoginRepository, IUserRepository<AppUser> userRepository,
           IUserRoleRepository userRoleRepository)
       {
           _roleRepository = roleRepository;
           _userClaimRepository = userClaimRepository;
           _userLoginRepository = userLoginRepository;
           _userRepository = userRepository;
           _userRoleRepository = userRoleRepository;
       }
       public Task AddLoginAsync(TUser user, UserLoginInfo login)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           if (login == null)
           {
               throw new ArgumentNullException("login");
           }

           _userLoginRepository.Insert(user, login);

           return Task.FromResult<object>(null);
       }
       /// <summary>
       /// Returns an TUser based on the Login info
       /// </summary>
       /// <param name="login"></param>
       /// <returns></returns>
       public Task<TUser> FindAsync(UserLoginInfo login)
       {
           if (login == null)
            {
                throw new ArgumentNullException("login");
            }

            var userId = _userLoginRepository.FindUserIdByLogin(login);
            if (userId != null)
            {
                TUser user = _userRepository.GetUserById(userId) as TUser;
                if (user != null)
                {
                    return Task.FromResult<TUser>(user);
                }
            }

            return Task.FromResult<TUser>(null);
       }
       /// <summary>
       /// Returns list of UserLoginInfo for a given TUser
       /// </summary>
       /// <param name="user"></param>
       /// <returns></returns>
       public Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
       {
           List<UserLoginInfo> userLogins = new List<UserLoginInfo>();
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           List<UserLoginInfo> logins = _userLoginRepository.FindByUserId(user.Id);
           if (logins != null)
           {
               return Task.FromResult<IList<UserLoginInfo>>(logins);
           }

           return Task.FromResult<IList<UserLoginInfo>>(null);
       }

       public Task RemoveLoginAsync(TUser user, UserLoginInfo login)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           if (login == null)
           {
               throw new ArgumentNullException("login");
           }

           _userLoginRepository.Delete(user, login);

           return Task.FromResult<Object>(null);
       }

       public Task CreateAsync(TUser user)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           _userRepository.Insert(user);

           return Task.FromResult<object>(null);
       }

       public Task DeleteAsync(TUser user)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           _userRepository.Delete(user);

           return Task.FromResult<object>(null); throw new NotImplementedException();
       }

       public Task<TUser> FindByIdAsync(string userId)
       {
           if (string.IsNullOrEmpty(userId))
           {
               throw new ArgumentException("Null or empty argument: userId");
           }

           TUser result = _userRepository.GetUserById(userId) as TUser;
           if (result != null)
           {
               return Task.FromResult<TUser>(result);
           }

           return Task.FromResult<TUser>(null);
       }

       public Task<TUser> FindByNameAsync(string userName)
       {
           if (string.IsNullOrEmpty(userName))
           {
               throw new ArgumentException("Null or empty argument: userName");
           }

           List<TUser> result = _userRepository.GetUserByName(userName) as List<TUser>;

           // Should I throw if > 1 user?
           if (result != null && result.Count == 1)
           {
               return Task.FromResult<TUser>(result[0]);
           }

           return Task.FromResult<TUser>(null);
       }

       public Task UpdateAsync(TUser user)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           _userRepository.Update(user);

           return Task.FromResult<object>(null);
       }

       public void Dispose()
       {
          
       }

       public Task AddClaimAsync(TUser user, System.Security.Claims.Claim claim)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           if (claim == null)
           {
               throw new ArgumentNullException("user");
           }

           _userClaimRepository.Insert(claim, user.Id);

           return Task.FromResult<object>(null);
       }

       public Task<IList<System.Security.Claims.Claim>> GetClaimsAsync(TUser user)
       {
           ClaimsIdentity identity = _userClaimRepository.FindByUserId(user.Id);

           return Task.FromResult<IList<Claim>>(identity.Claims.ToList());
       }

       public Task RemoveClaimAsync(TUser user, System.Security.Claims.Claim claim)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           if (claim == null)
           {
               throw new ArgumentNullException("claim");
           }

           _userClaimRepository.Delete(user, claim);

           return Task.FromResult<object>(null);
       }

       public Task AddToRoleAsync(TUser user, string roleName)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           if (string.IsNullOrEmpty(roleName))
           {
               throw new ArgumentException("Argument cannot be null or empty: roleName.");
           }

           string roleId = _roleRepository.GetRoleId(roleName);
           if (!string.IsNullOrEmpty(roleId))
           {
               _userRoleRepository.Insert(user, roleId);
           }

           return Task.FromResult<object>(null);
       }

       public Task<IList<string>> GetRolesAsync(TUser user)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           List<string> roles = _userRoleRepository.FindByUserId(user.Id);
           {
               if (roles != null)
               {
                   return Task.FromResult<IList<string>>(roles);
               }
           }

           return Task.FromResult<IList<string>>(null);
       }

       public Task<bool> IsInRoleAsync(TUser user, string roleName)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           if (string.IsNullOrEmpty(roleName))
           {
               throw new ArgumentNullException("role");
           }

           List<string> roles =_userRoleRepository.FindByUserId(user.Id);
           {
               if (roles != null && roles.Contains(roleName))
               {
                   return Task.FromResult<bool>(true);
               }
           }

           return Task.FromResult<bool>(false);
       }

       public Task RemoveFromRoleAsync(TUser user, string roleName)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           if (string.IsNullOrEmpty(roleName))
           {
               throw new ArgumentNullException("role");
           }
           AppRole role= _roleRepository.GetRoleByName(roleName);
           if(role!=null){
               _userRoleRepository.DeleteUserRole(user.Id, role.Id);
           }
           
           return Task.FromResult<object>(null);
       }
       
       /// <summary>
       /// Returns the PasswordHash for a given TUser
       /// </summary>
       /// <param name="user"></param>
       /// <returns></returns>
       public Task<string> GetPasswordHashAsync(TUser user)
       {
           string passwordHash = _userRepository.GetPasswordHash(user.Id);

           return Task.FromResult<string>(passwordHash);
       }
       /// <summary>
       /// Verifies if user has password
       /// </summary>
       /// <param name="user"></param>
       /// <returns></returns>
       public Task<bool> HasPasswordAsync(TUser user)
       {
           var hasPassword = !string.IsNullOrEmpty(_userRepository.GetPasswordHash(user.Id));

           return Task.FromResult<bool>(Boolean.Parse(hasPassword.ToString()));
       }

       public Task SetPasswordHashAsync(TUser user, string passwordHash)
       {
           user.PasswordHash = passwordHash;

           return Task.FromResult<Object>(null);
       }

       public Task<string> GetSecurityStampAsync(TUser user)
       {
           return Task.FromResult(user.SecurityStamp);
       }

       public Task SetSecurityStampAsync(TUser user, string stamp)
       {
           user.SecurityStamp = stamp;

           return Task.FromResult(0);
       }

       public IQueryable<TUser> Users
       {
           get { return _userRepository.Users as IQueryable<TUser>; }
       }
       /// <summary>
       /// Get user by email
       /// </summary>
       /// <param name="email"></param>
       /// <returns></returns>
       public Task<TUser> FindByEmailAsync(string email)
       {
           if (String.IsNullOrEmpty(email))
           {
               throw new ArgumentNullException("email");
           }

           TUser result = _userRepository.GetUserByEmail(email) as TUser;
           if (result != null)
           {
               return Task.FromResult<TUser>(result);
           }

           return Task.FromResult<TUser>(null);
       }

       public Task<string> GetEmailAsync(TUser user)
       {
           return Task.FromResult(user.Email);
       }

       public Task<bool> GetEmailConfirmedAsync(TUser user)
       {
          return Task.FromResult(user.EmailConfirmed);
       }

       public Task SetEmailAsync(TUser user, string email)
       {
           user.Email = email;
           _userRepository.Update(user);
           return Task.FromResult(0);
       }

       public Task SetEmailConfirmedAsync(TUser user, bool confirmed)
       {
           user.EmailConfirmed = confirmed;
           _userRepository.Update(user);

           return Task.FromResult(0);
       }

       public Task<string> GetPhoneNumberAsync(TUser user)
       {
           return Task.FromResult(user.PhoneNumber);
       }

       public Task<bool> GetPhoneNumberConfirmedAsync(TUser user)
       {
           return Task.FromResult(user.PhoneNumberConfirmed);
       }

       public Task SetPhoneNumberAsync(TUser user, string phoneNumber)
       {
           user.PhoneNumber = phoneNumber;
           _userRepository.Update(user);

           return Task.FromResult(0);
       }

       public Task SetPhoneNumberConfirmedAsync(TUser user, bool confirmed)
       {
           user.PhoneNumberConfirmed = confirmed;
           _userRepository.Update(user);

           return Task.FromResult(0);
       }

       public Task<bool> GetTwoFactorEnabledAsync(TUser user)
       {
           return Task.FromResult(user.TwoFactorEnabled);
       }

       public Task SetTwoFactorEnabledAsync(TUser user, bool enabled)
       {
           user.TwoFactorEnabled = enabled;
           _userRepository.Update(user);

           return Task.FromResult(0);
       }

       public Task<int> GetAccessFailedCountAsync(TUser user)
       {
           return Task.FromResult(user.AccessFailedCount);
       }

       public Task<bool> GetLockoutEnabledAsync(TUser user)
       {
           return Task.FromResult(user.LockoutEnabled);
       }
       /// <summary>
       /// Get user lock out end date
       /// </summary>
       /// <param name="user"></param>
       /// <returns></returns>
       public Task<DateTimeOffset> GetLockoutEndDateAsync(TUser user)
       {
           return
                Task.FromResult(user.LockoutEndDateUtc.HasValue
                    ? new DateTimeOffset(DateTime.SpecifyKind(user.LockoutEndDateUtc.Value, DateTimeKind.Utc))
                    : new DateTimeOffset());
       }

       public Task<int> IncrementAccessFailedCountAsync(TUser user)
       {
           user.AccessFailedCount++;
           _userRepository.Update(user);

           return Task.FromResult(user.AccessFailedCount);
       }

       public Task ResetAccessFailedCountAsync(TUser user)
       {
           user.AccessFailedCount = 0;
           _userRepository.Update(user);

           return Task.FromResult(0);
       }

       public Task SetLockoutEnabledAsync(TUser user, bool enabled)
       {
           user.LockoutEnabled = enabled;
           _userRepository.Update(user);

           return Task.FromResult(0);
       }

       public Task SetLockoutEndDateAsync(TUser user, DateTimeOffset lockoutEnd)
       {
           user.LockoutEndDateUtc = lockoutEnd.UtcDateTime;
           _userRepository.Update(user);

           return Task.FromResult(0);
       }

       Task IUserStore<TUser, string>.CreateAsync(TUser user)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }
           _userRepository.Insert(user);
           return Task.FromResult<object>(null);
       }

       Task IUserStore<TUser, string>.DeleteAsync(TUser user)
       {
           if(user != null)
            {
                _userRepository.Delete(user);
            }

            return Task.FromResult<Object>(null);
       }

       Task<TUser> IUserStore<TUser, string>.FindByIdAsync(string userId)
       {
           if (string.IsNullOrEmpty(userId))
           {
               throw new ArgumentException("Null or empty argument: userId");
           }

           TUser result = _userRepository.GetUserById(userId) as TUser;
           if (result != null)
           {
               return Task.FromResult<TUser>(result);
           }
           return Task.FromResult<TUser>(null);
       }

       Task<TUser> IUserStore<TUser, string>.FindByNameAsync(string userName)
       {
           if (string.IsNullOrEmpty(userName))
           {
               throw new ArgumentException("Null or empty argument: userName");
           }

           List<TUser> result = _userRepository.GetUserByName(userName) as List<TUser>;

           // Should I throw if > 1 user?
           if (result != null && result.Count == 1)
           {
               return Task.FromResult<TUser>(result[0]);
           }

           return Task.FromResult<TUser>(null);
       }

       Task IUserStore<TUser, string>.UpdateAsync(TUser user)
       {
           if (user == null)
           {
               throw new ArgumentNullException("user");
           }

           _userRepository.Update(user);

           return Task.FromResult<object>(null); 
       }

      
   }
}

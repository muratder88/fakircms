﻿using FakirCms.Domain.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.User
{
    public class UserRoleRepository : FakirCms.Domain.User.IUserRoleRepository
    {
        private EFDbFakirCms  _context;

        /// <summary>
        /// Constructor that takes a MySQLDatabase instance 
        /// </summary>
        /// <param name="database"></param>
        public UserRoleRepository(EFDbFakirCms context)
        {
            _context = context;
        }

        /// <summary>
        /// Returns a list of user's roles
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public List<string> FindByUserId(string userId)
        {
            
            List<string> roles = new List<string>();
            string commandText = "Select Role.Name from UserRole, Role where UserRole.UserId = @userId and UserRole.RoleId = Role.Id";
            List<MySqlParameter> parameterList = new List<MySqlParameter>();
            parameterList.Add(new MySqlParameter("@userId",userId));

            MySqlParameter[] parameters = parameterList.ToArray();

            var rows = _context.Database.SqlQuery<string>(commandText, parameters);
            foreach (var row in rows)
            {
                roles.Add(row);
            }

            return roles;
        }
        /// <summary>
        /// Delete given role from a user in the UserRoles Table
        /// </summary>
        /// <returns></returns>

        public int DeleteUserRole(string userId,string roleId)
        {
            string commandText = "Delete from UserRole where UserId = @userId and RoleId=@roleId";
            List<MySqlParameter> parameterList = new List<MySqlParameter>();
            parameterList.Add(new MySqlParameter("@userId", userId));
            parameterList.Add(new MySqlParameter("@roleId", roleId));

            MySqlParameter[] parameters = parameterList.ToArray();
            return _context.Database.ExecuteSqlCommand(commandText, parameters);
        }

        /// <summary>
        /// Deletes all roles from a user in the UserRoles table
        /// </summary>
        /// <param name="userId">The user's id</param>
        /// <returns></returns>
        public int Delete(string userId)
        {
            string commandText = "Delete from UserRole where UserId = @userId";
            List<MySqlParameter> parameterList = new List<MySqlParameter>();
            parameterList.Add(new MySqlParameter("@userId", userId));

            MySqlParameter[] parameters = parameterList.ToArray();
            return _context.Database.ExecuteSqlCommand(commandText, parameters);
        }

        /// <summary>
        /// Inserts a new role for a user in the UserRoles table
        /// </summary>
        /// <param name="user">The User</param>
        /// <param name="roleId">The Role's id</param>
        /// <returns></returns>
        public int Insert(AppUser user, string roleId)
        {
            string commandText = "Insert into UserRole (UserId, RoleId) values (@userId, @roleId)";
            List<MySqlParameter> parameterList = new List<MySqlParameter>();
            parameterList.Add(new MySqlParameter("@userId",user.Id));
            parameterList.Add(new MySqlParameter("@roleId", roleId));
           
            MySqlParameter[] parameters = parameterList.ToArray();

            return _context.Database.ExecuteSqlCommand(commandText,parameters);
        }
    }
}

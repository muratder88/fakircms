﻿using System;
namespace FakirCms.Domain.Manager
{
    public interface IUrlRoutingManager
    {
        string GetHaberCityUrl(string CityName);
        string GetHaberDetailUrl(FakirCms.Domain.Entities.Haber haber);
        string GetHaberListUrl(string alias);
        string GetHaberTagUrl(string tagalias);
        string GetHaberTownUrl(string cityalias, string townalias);
        string GetAuthorUrl(string yazarAlias);
        string GetArticleUrl(string yazarAlias, FakirCms.Domain.Entities.Article article);
    }
}

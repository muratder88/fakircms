﻿using FakirCms.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakirCms.Domain.Manager
{
    public class AppRoleManager : RoleManager<AppRole>
    {
        public AppRoleManager(IRoleStore<AppRole, string> roleStore)
            : base(roleStore)
        {

        }
    }

    //public class RoleStore : RoleStore<AppRole>
    //{

    //    public RoleStore(EFDbFakirCms context)
    //        : base(context)
    //    {

    //    }
    //}
}

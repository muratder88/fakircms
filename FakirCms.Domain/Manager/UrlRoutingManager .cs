﻿using FakirCms.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FakirCms.Domain.Manager
{
    public class UrlRoutingManager : FakirCms.Domain.Manager.IUrlRoutingManager
    {
        private UrlHelper _urlHelper;
        public UrlRoutingManager(UrlHelper urlHelper)
        {
            _urlHelper = urlHelper;
        }

        public string GetHaberDetailUrl(Haber haber)
        {
            return _urlHelper.RouteUrl("HaberDetail", new
            {
                alias = haber.Alias,
                id = haber.Id
            });
        }

        public string GetHaberListUrl(string alias)
        {
            return _urlHelper.RouteUrl("HaberList", new
            {
                alias=alias
            });

        }

        public string GetHaberCityUrl(string CityName)
        {
            return _urlHelper.RouteUrl("HaberCity", new
            {
                cityalias = CityName.ToLower()
            });
        }
        public string GetHaberTownUrl(string cityalias, string townalias)
        {
            return _urlHelper.RouteUrl("HaberTown", new
            {
                cityalias = cityalias,
                townalias = townalias

            });
        }

        public string GetHaberTagUrl(string tagalias)
        {
            return _urlHelper.RouteUrl("HaberTag", new { tagalias = tagalias });
        }

        public string GetAuthorUrl(string yazarAlias)
        {
            return _urlHelper.RouteUrl("ArticleList", new { yazar = yazarAlias });
        }

        public string GetArticleUrl(string yazarAlias, Article article)
        {
            return _urlHelper.RouteUrl("ArticleDetail", new { yazar = yazarAlias, alias = article.Alias, id = article.Id });
        }
    }
}

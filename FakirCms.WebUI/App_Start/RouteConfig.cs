﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FakirCms.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");

            routes.MapRoute("Robots.txt", "robots.txt", new { controller = "Home", action = "Robots" }, namespaces: new[] { "FakirCms.WebUI.Controllers" });

            //Haber Routing
            routes.MapRoute(
                name: "HaberDetail",
                url: "haber/{alias}-{id}.html",
                defaults: new
                {
                    controller = "Haber",
                    action = "Detail"
                },
                namespaces: new[] { "FakirCms.WebUI.Controllers" }
                );
            routes.MapRoute(
                name: "HaberList",
                url: "haberler/{alias}/{page}",
                defaults: new
                {
                    controller = "Haber",
                    action = "HaberCategory",
                    page = UrlParameter.Optional
                },
                namespaces: new[] { "FakirCms.WebUI.Controllers" }
                );
            routes.MapRoute(
                name: "HaberCity",
                url: "haber-sehir/{cityalias}/{currentpage}",
                defaults: new
                {
                    controller = "Haber",
                    action = "City",
                    currentpage = UrlParameter.Optional
                },
                 namespaces: new[] { "FakirCms.WebUI.Controllers" }
                );
            routes.MapRoute(
               name: "HaberTown",
               url: "haber-ilce/{cityalias}-{townalias}/{pagenumber}",
               defaults: new
               {
                   controller = "Haber",
                   action = "Town",
                   pagenumber = UrlParameter.Optional,

               },
                namespaces: new[] { "FakirCms.WebUI.Controllers" }
               );
            routes.MapRoute(
                name: "HaberTag",
                url: "haber-etiket/{tagalias}/{currentpage}",
                defaults: new
                {
                    controller = "Haber",
                    action = "Tag",
                    currentpage = UrlParameter.Optional
                },
                namespaces: new[] { "FakirCms.WebUI.Controllers" }
                );

            //Köşe Yazısı Routing
            routes.MapRoute(
                name: "ArticleDetail",
                url: "kose-yazisi/{yazar}/{alias}-{id}.html",
                defaults: new
                {
                    controller = "Article",
                    action = "Detail"
                },
                namespaces: new[] { "FakirCms.WebUI.Controllers" }
                );
            routes.MapRoute(
               name: "ArticleList",
               url: "kose-yazilari/{yazar}/{page}",
               defaults: new
               {
                   controller = "Article",
                   action = "Index",
                   page = UrlParameter.Optional
               },
               namespaces: new[] { "FakirCms.WebUI.Controllers" }
               );
            //Yazar Routing
            routes.MapRoute(
                name:"AuthorList",
                url:"yazarlar/{page}",
                defaults: new
                {
                    controller="Author",
                    action="Index",
                    page=UrlParameter.Optional
                },
                namespaces: new[] { "FakirCms.WebUI.Controllers" }
                );

            routes.MapRoute(name: "Page",
                url: "{alias}.html",
                defaults: new
                {
                    controller = "Page",
                    action = "Index"
                },
               namespaces: new[] { "FakirCms.WebUI.Controllers" }
               );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "FakirCms.WebUI.Controllers" }
            );
        }
    }
}

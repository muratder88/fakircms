﻿using FakirCms.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakirCms.WebUI.Models
{
    public class PopularHaberModel
    {
        public List<HaberDetail> Popular { get; set; }
        public List<HaberDetail> Recent { get; set; }
    }
}
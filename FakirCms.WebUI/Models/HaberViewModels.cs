﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakirCms.WebUI.Models
{
    public class HaberDetailView
    {
        public HaberDetail Haber { get; set; }
        public HaberDetail PrevHaber { get; set; }
        public HaberDetail NextHaber { get; set; }
        public List<HaberDetail> RelatedHabers { get; set; }
    }
    public class HaberCategoryView
    {
        public List<HaberDetail> Haberler { get; set; }
        public HaberCategory Category { get; set; }
    }
}
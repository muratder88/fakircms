﻿using FakirCms.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakirCms.WebUI.Models
{
    public class HomeIndexView
    {
        public List<HaberDetail> Haberler { get; set; }
        public List<HaberDetail> MansetHaberler { get; set; }
        public List<HaberDetail> MainHaberler { get; set; }
        public List<HaberDetail> AltHaberler { get; set; }
        public List<HaberDetail> UstHaberler { get; set; }
    }
}
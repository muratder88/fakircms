﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakirCms.WebUI.Models
{
    public class ArticleIndexModel
    {
        public List<ArticleDetail> Articles { get; set; }
        public AuthorDetail Author { get; set; }
    }

    public class ArtilceDetailModel
    {
        public Article Article { get; set; }
        public Author Author { get; set; }
    }
}
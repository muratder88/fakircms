﻿using FakirCms.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles="SuperAdmin,Admin")]
    public class HomeController : Controller
    {
        private IThumbnailService _thumbnailService;
        public HomeController(IThumbnailService thumbnailService)
        {
            _thumbnailService = thumbnailService;
        }
        public ActionResult Index()
        {
            if (!_thumbnailService.IsWriteable())
            {
                TempData["error"] = "Thumbnail klasörü gerekli dosya izinleri mevcut değil.Thumbnail klasörü izinlerini ayarlayın";
            }
            return View();
        }
	}
}
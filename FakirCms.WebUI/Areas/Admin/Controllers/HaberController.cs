﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakirCms.Domain.Services;
using FakirCms.Domain;
using FakirCms.Domain.Entities;
using FakirCms.WebUI.Areas.Admin.Models;
using System.Net;

using FakirCms.Domain.Rss;

namespace FakirCms.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class HaberController : Controller
    {
        private IHaberService _haberService;
        private ITagService _tagService;
        private IUserService _userService;
        private IThumbnailService _thumbnailService;
        private IHaberCategoryService _haberCategoryService;
        private IDataService _dataService;

        public HaberController(IHaberService haberService, ITagService tagService, IUserService userService,
            IThumbnailService thumbnailService,IHaberCategoryService haberCategoryService,IDataService dataService)
        {
            _haberService = haberService;
            _tagService = tagService;
            _userService = userService;
            _thumbnailService = thumbnailService;
            _haberCategoryService = haberCategoryService;
            _dataService = dataService;
        }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetData()
        {
            var resultData = (from haber in _haberService.items
                              orderby haber.Id descending
                              select new
                              {
                                  Id = haber.Id,
                                  Title = haber.Title,
                                  Publish = haber.State,
                                  Author = haber.CreatedBy.Name,
                                  Date = haber.Date.ToString("yyyy/MM/dd"),
                                  Hits = haber.Hits
                              }).ToArray();
            return Json(new { data = resultData }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            CreateHaberModel model = new CreateHaberModel();
            model.Date = System.DateTime.Now;
            model.CategorySelecList = _haberCategoryService.GetSelectList();
            ViewBag.TagsSelectListItems = _tagService.GetTagSelectListItems();
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateHaberModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id > 0)
                {
                    //make a update
                    Haber haber = _haberService.FindById(model.Id);
                    haber.Title = model.Title;
                    haber.Alias = model.Alias;
                    haber.IntroText = model.IntroText;
                    haber.FullText = model.FullText;
                    haber.State = model.IsActive;
                    haber.Date = model.Date;
                    haber.ImageUrl=model.ImageUrl;
                    haber.City=model.City;
                    haber.MetaTitle = model.MetaTitle;
                    haber.MetaDescription = model.MetaDescription;
                    haber.MetaKeywords = model.MetaKeywords;
                    haber.Town = model.Town;
                    haber.SpotBigTittle=model.SpotBigTittle;
                    haber.SpotSmallTitle=model.SpotSmallTitle;
                    haber.ShowManset=model.ShowManset;
                    haber.ShowFrontPageMain = model.ShowFrontPageMain;
                    haber.ShowFrontPageAlt = model.ShowFrontPageAlt;
                    haber.ShowFrontPageUst = model.ShowFrontPageUst;
                    haber.ShowCategoryManset=model.ShowCategoryManset;
                    haber.HaberCategoryID=model.Category;
                    var result = _haberService.Update(haber);
                    List<Tag> list = null;
                    if (model.Tags != null)
                    {
                        list = _tagService.Tags.Where(x => model.Tags.Contains(x.TagId)).ToList();
                    }
                    var tagResult = _haberService.UpdateTags(haber.Id, list);

                    if (result && tagResult)
                    {
                        TempData["success"] = "Haber başarılı bir şekilde güncellendi";
                       bool IsThumbnailsCreated= _thumbnailService.UpdateHaberThumnails(haber);
                        if(!IsThumbnailsCreated)
                            TempData["error"] = "Haber için Thumbnail oluşturulurken hata meydana geldi";
                        
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["error"] = "Haber eklenirken bir hatayla karşılaşıldı";
                    }
                }
                else
                {
                    Haber haber = new Haber()
                    {
                        Title = model.Title,
                        Alias = model.Alias,
                        FullText = model.FullText,
                        IntroText = model.IntroText,
                        State = model.IsActive,
                        Date = model.Date,
                        ImageUrl = model.ImageUrl,
                        City = model.City,
                        Town = model.Town,
                        MetaKeywords=model.MetaKeywords,
                        MetaTitle=model.MetaTitle,
                        MetaDescription=model.MetaDescription,
                        SpotBigTittle = model.SpotBigTittle,
                        SpotSmallTitle = model.SpotSmallTitle,
                        ShowManset = model.ShowManset,
                        ShowCategoryManset = model.ShowCategoryManset,
                        ShowFrontPageAlt = model.ShowFrontPageAlt,
                        ShowFrontPageUst=model.ShowFrontPageUst,
                        ShowFrontPageMain=model.ShowFrontPageMain,
                        HaberCategoryID = model.Category,
                    };
                    List<Tag> list = null;
                    if (model.Tags != null)
                    {
                        list = _tagService.Tags.Where(x => model.Tags.Contains(x.TagId)).ToList();
                        haber.Tags = list;
                    }
                   
                    var result = _haberService.Create(haber);
                    if (result)
                    {
                        TempData["success"] = "Haber başarılı bir şekilde eklendi";
                        bool IsThumbnailsUpdated = _thumbnailService.CreateHaberThumbnails(haber);
                        if (!IsThumbnailsUpdated)
                            TempData["error"] = "Haber için Thumbnail oluşturulurken hata meydana geldi.";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["error"] = "Haber eklenirken bir hatayla karşılaşıldı";
                    }
                }
               
            }
            model.CategorySelecList = _haberCategoryService.GetSelectList();
            ViewBag.TagsSelectListItems = _tagService.GetTagSelectListItems();
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            
            Haber haber = _haberService.FindById(id);
            if (haber == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                CreateHaberModel model = new CreateHaberModel()
                {
                    Id = haber.Id,
                    Alias = haber.Alias,
                    Created = haber.Created,
                    CreatedByName = haber.CreatedBy.Name,
                    Updated = haber.Updated,
                    UpdatedByName = haber.UpdatedBy.Name,
                    Title = haber.Title,
                    IsActive = haber.State,
                    Date = haber.Date,
                    IntroText = haber.IntroText,
                    FullText = haber.FullText,
                    ImageUrl=haber.ImageUrl,
                    City=haber.City,
                    Town=haber.Town,
                    MetaKeywords = haber.MetaKeywords,
                    MetaTitle = haber.MetaTitle,
                    MetaDescription = haber.MetaDescription,
                    SpotBigTittle=haber.SpotBigTittle,
                    SpotSmallTitle=haber.SpotSmallTitle,
                    ShowManset=haber.ShowManset,
                    ShowCategoryManset=haber.ShowCategoryManset,
                    ShowFrontPageUst=haber.ShowFrontPageUst,
                    ShowFrontPageAlt=haber.ShowFrontPageAlt,
                    ShowFrontPageMain=haber.ShowFrontPageMain,
                    Category=haber.HaberCategoryID,
                    CategorySelecList = _haberCategoryService.GetSelectList()
                };
                //set Tags
                if (haber.Tags != null && haber.Tags.Count() > 0)
                    model.Tags = haber.Tags.Select(x => x.TagId).ToList();
                ViewBag.TagsSelectListItems = _tagService.GetTagSelectListItems();
                return View("Create", model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            Haber haber = _haberService.FindById(id);
            if (haber == null)
            {
                return Json(new { success = false, msg = string.Format("The {0} id'ye sahip haber bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //delete aciklama
                _haberService.Delete(id);
                return Json(new { success = true, msg = "Başarılı bir şekilde silme işlemi gerçekleşti" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Publish(int id)
        {
            Haber haber = _haberService.FindById(id);
            if (haber == null)
            {
                return Json(new { success = false, msg = string.Format("{0} id'ye sahip açıklama bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //update publish property
                haber.State = true;
                _haberService.Update(haber);
                return Json(new { success = true, msg = "Açıklama başarılı bir şekilde yayınlandı" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UnPublish(int id)
        {
            Haber haber = _haberService.FindById(id);
            if (haber == null)
            {
                return Json(new { success = false, msg = string.Format("The {0} id'ye sahip açıklama bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //update publish property
                haber.State = false;
                _haberService.Update(haber);
                return Json(new { success = true, msg = "Açıklama başarılı bir şekilde yayından kaldırıldı" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetDataFromRss()
        {
            string gundemRss="http://rss.hurriyet.com.tr/rss.aspx?sectionId=2";
            string sporRss="http://rss.hurriyet.com.tr/rss.aspx?sectionId=14";
            string kulturRss = "http://rss.hurriyet.com.tr/rss.aspx?sectionId=2451";
            string teknolojiRss = "http://rss.hurriyet.com.tr/rss.aspx?sectionId=2158";
            string ekonomiRss = "http://rss.hurriyet.com.tr/rss.aspx?sectionId=4";
            string dunyaRss = "http://rss.hurriyet.com.tr/rss.aspx?sectionId=3";

            _dataService.AddHaberFromRssToDatabase(gundemRss, 4);
            _dataService.AddHaberFromRssToDatabase(sporRss, 3);
            _dataService.AddHaberFromRssToDatabase(dunyaRss, 5);
            //_dataService.AddHaberFromRssToDatabase(teknolojiRss, 4);
            //_dataService.AddHaberFromRssToDatabase(ekonomiRss, 4);
            _dataService.AddHaberFromRssToDatabase(kulturRss, 6);
            return RedirectToAction("Index");
        }
	}
}
﻿using FakirCms.Domain.Services;
using FakirCms.WebUI.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Areas.Admin.Controllers
{
    public class AccountController : Controller
    {
        private IUserService _service;
        //
        // GET: /Admin/Account/

        public AccountController(IUserService service)
        {
            _service = service;
        }

        public ActionResult Index() { return RedirectToAction("Index", "Home"); }
        public ActionResult Login(string returnUrl)
        {
            if (returnUrl == null)
                returnUrl = "Index";
            ViewBag.returnUrl = returnUrl;
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginModel details, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (_service.Login(details.Name, details.Password))
                {
                    if (string.IsNullOrEmpty(returnUrl))
                        returnUrl = "Index";
                    return Redirect(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Yanlış Kullanıcı Adı veay Şifresi");
                }
            }
            return View(details);
        }

        public ActionResult Logout()
        {
            _service.Logout();
            return RedirectToAction("Index", "Home");
        }  
	}
}
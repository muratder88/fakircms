﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Services;
using FakirCms.WebUI.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class HaberCategoryController : Controller
    {
        private IHaberCategoryService _haberCategoryService;
        public HaberCategoryController(IHaberCategoryService haberCategoryService)
        {
            _haberCategoryService = haberCategoryService;
        }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetData()
        {
            var resultData=(from category in _haberCategoryService.items
                                orderby category.Id descending
                                select new{
                                    Id=category.Id,
                                    Title=category.Title,
                                    Author=category.CreatedBy.Name,
                                    Date=category.Created.ToString("yyyy/MM/dd")
                                }).ToArray();
            return Json(new { data = resultData }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            CreateHaberCategoryModel model = new CreateHaberCategoryModel();
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateHaberCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id > 0)
                {
                    //make a update
                    HaberCategory category = _haberCategoryService.FindById(model.Id);
                    category.Title = model.Title;
                    category.Alias = model.Alias;
                    category.Description = model.Description;
                    category.Tags = model.Tags;
                    var result = _haberCategoryService.Update(category);
                    if (result)
                    {
                        TempData["success"] = "Haber Kategorisi başarılı bir şekilde güncellendi";
                    }
                    else
                    {
                        TempData["error"] = "Haber Kategorisi güncellenirken bir hatayla karşılaşıldı";
                    }
                }
                else
                {
                    HaberCategory category = new HaberCategory();
                    category.Title = model.Title;
                    category.Alias = model.Alias;
                    category.Description = model.Description;
                    category.Tags = model.Tags;

                    var result = _haberCategoryService.Create(category);
                    if (result)
                    {
                        TempData["success"] = "Haber Kategorisi başarılı bir şekilde eklendi";
                    }
                    else
                    {
                        TempData["error"] = "Haber Kategorisi eklenirken bir hatayla karşılaşıldı";
                    }
                }
                return View(model);
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {

            HaberCategory category = _haberCategoryService.FindById(id);
            if (category == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            else
            {
                CreateHaberCategoryModel model = new CreateHaberCategoryModel()
                {
                    Id = category.Id,
                    Alias = category.Alias,
                    Title = category.Title,
                    Description=category.Description,
                    Tags=category.Tags,
                    Created=category.Created,
                    Updated=category.Updated,
                    CreatedByName=category.CreatedBy.Name,
                    UpdatedByName=category.UpdatedBy.Name
                };
                return View("Create", model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            HaberCategory category = _haberCategoryService.FindById(id);
            if (category == null)
            {
                return Json(new { success = false, msg = string.Format("The {0} id'ye sahip haber kategorisi bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //delete aciklama
                _haberCategoryService.Delete(id);
                return Json(new { success = true, msg = "Başarılı bir şekilde silme işlemi gerçekleşti" }, JsonRequestBehavior.AllowGet);
            }
        }

	}
}
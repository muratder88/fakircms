﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakirCms.Domain.Entities;
using FakirCms.Domain.Services;
using System.Collections.Specialized;
using System.Net;
using FakirCms.Domain.Helpers;
using FakirCms.Domain.Exceptions;


namespace FakirCms.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class TagController : Controller
    {
        private ITagService _tagService;
        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }
        public PartialViewResult Index()
        {
            return PartialView("Index");
        }
        public JsonResult GetTags()
        {
            var items = (from tag in _tagService.Tags.ToList()
                         select new
                         {
                             Name = tag.Name,
                             Id = tag.TagId
                         }).ToArray();
            return Json(new { data = items }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSelectTags()
        {
            var tagDataArray = (from tag in _tagService.Tags.ToList()
                                select new
                                {
                                    id=tag.TagId,
                                    text=tag.Name
                                }).ToArray();
            return Json(tagDataArray,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            Tag tag = _tagService.Tags.Where(x => x.TagId == id).FirstOrDefault();
            if (tag == null)
            {
                return Json(new { success = false, msg = string.Format("The {0} id'ye sahip etiket bulunamadı",id) },JsonRequestBehavior.AllowGet);
            }
            else
            {
                //delete tag
                _tagService.Delete(tag);
                return Json(new{success=true,msg="Başarılı bir şekilde silme işlemi gerçekleşti"},JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Tag(string name,string value,int pk)
        {
            if(value.Length>70)
                return Json(new { success = false, msg ="Etiket Adı maksimum 70 karekter uzunlugunda olabilir"}, JsonRequestBehavior.AllowGet);
            if (pk > 0)
            {
                try
                {
                    //update 
                    Tag tag = _tagService.Tags.Where(x => x.TagId == pk).FirstOrDefault();
                    if (tag == null)
                    {
                        return Json(new { success = false, msg = string.Format("The {0} id'ye sahip etiket bulunamadı") }, JsonRequestBehavior.AllowGet);
                    }
                    //dynamically set property value

                    tag.Name = value;
                    tag.Alias = tag.Name.ToAlias();
                    _tagService.Update(tag);
                }
                catch (InsertDuplicateException ex)
                {

                    return Json(new { success = false, msg = ex.Message }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = true, msg = "Başarılı bir şekilde güncelleme işlemi yapılmıştır" },JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    //add new one
                    Tag tag = new Tag()
                    {
                        Name = value,
                        Alias = value.ToAlias()
                    };
                    _tagService.Add(tag);
                }
                catch (InsertDuplicateException ex)
                {
                    return Json(new { success = false, msg = ex.Message }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { success = true, msg = "Başarılı bir şekilde etiket eklenmiştir" }, JsonRequestBehavior.AllowGet);
           
            }
        }
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public PartialViewResult Index(TagListView postedModel)
        //{
        //    return PartialView("_TagListView", GetModelFromPostedModel(postedModel));
        //}
        //private TagListView GetModelFromPostedModel(TagListView postedModel)
        //{
        //    postedModel.Tags = _context.Tags;
        //    postedModel.SetModelProperties();
        //    return postedModel;
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public PartialViewResult Create( )
        //{
        //    TagCreateModel createModel = postedModel.TagModel;
        //    if (!String.IsNullOrEmpty(createModel.TagName))
        //    {
        //        Tag tag = new Tag();
        //        tag.TagId = createModel.TagId;
        //        tag.Name = createModel.TagName;
        //        if (createModel.TagId > 0)
        //        {
        //            //Update
        //            _context.Entry(tag).State = EntityState.Modified;
        //            _context.SaveChanges();
        //            TempData["success"] = "Başarılı Bir Şekilde Güncellendi";
        //        }
        //        else
        //        {
        //            //Add New
        //            _context.Tags.Add(tag);
        //            _context.SaveChanges();
        //            TempData["success"] = "Başarılı Bir Şekilde Eklendi";
        //            postedModel.TagModel.TagName = "";
        //        }
        //    }
        //    else
        //    {
        //        ModelState.AddModelError("","Etiket Adı girilmemiş");
        //    }
        //    return PartialView("_TagListView", GetModelFromPostedModel(postedModel));
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public PartialViewResult Delete(TagListView postedModel,int[] cid)
        //{
        //    if (cid.Count() > 0)
        //    {
        //        foreach (int id in cid)
        //        {
        //            _context.Tags.Remove(_context.Tags.Where(x => x.TagId == id).FirstOrDefault());
        //            _context.SaveChanges();
        //            TempData["success"] = "Başarılı Bir Şekilde Silindi";
        //        }
        //    }
        //    return PartialView("_TagListView", GetModelFromPostedModel(postedModel));
        //}
	}
}
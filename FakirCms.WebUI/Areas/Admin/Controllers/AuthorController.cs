﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakirCms.Domain;
using FakirCms.Domain.Entities;
using FakirCms.Domain.Services;
using FakirCms.WebUI.Areas.Admin.Models;
using System.Net;

namespace FakirCms.WebUI.Areas.Admin.Controllers
{
    public class AuthorController : Controller
    {
        private IAuthorService _authorService;
        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetData()
        {
            var resultData = (from author in _authorService.Items
                              orderby author.Id descending
                              select new
                              {
                                  Id=author.Id,
                                  Image=author.Image,
                                  Name=author.Name,
                                  Created = author.Created.ToString("yyyy/MM/dd"),
                                  Updated = author.Updated.ToString("yyyy/MM/dd")
                              }).ToArray();
            return Json(new { data = resultData }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            CreateAuthorModel model = new CreateAuthorModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateAuthorModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id > 0)
                {
                    //make a update
                    Author author = _authorService.FindById(model.Id);
                    author.Name = model.Name;
                    author.Image = model.Image;
                    author.Description = model.Description;
                    author.Email = model.Email;
                    author.Alias = model.Alias;
                    author.MetaDescription = model.MetaDescription;
                    author.MetaKeywords = model.MetaKeywords;
                    author.MetaTitle = model.MetaTitle;
                    var result = _authorService.Update(author);
                    if (result)
                    {
                        TempData["success"] = "Yazar başarılı bir şekilde güncellendi";
                    }
                    else
                    {
                        TempData["error"] = "Yazar güncellenirken bir hatayla karşılaşıldı";
                    }
                }
                else
                {
                    Author author = new Author();
                    author.Name = model.Name;
                    author.Image = model.Image;
                    author.Description = model.Description;
                    author.Email = model.Email;
                    author.Alias = model.Alias;
                    author.MetaDescription = model.MetaDescription;
                    author.MetaKeywords = model.MetaKeywords;
                    author.MetaTitle = model.MetaTitle;
                    var result = _authorService.Create(author);
                    if (result)
                    {
                        TempData["success"] = "Yazar başarılı bir şekilde eklendi";
                    }
                    else
                    {
                        TempData["error"] = "Yazar eklenirken bir hatayla karşılaşıldı";
                    }
                }
                return View(model);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Author author = _authorService.FindById(id);
            if (author == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            CreateAuthorModel model = new CreateAuthorModel()
            {
                Id = author.Id,
                Alias = author.Alias,
                Name = author.Name,
                Description = author.Description,
                Image=author.Image,
                Email = author.Email,
                MetaTitle = author.MetaTitle,
                MetaDescription = author.MetaDescription,
                MetaKeywords = author.MetaKeywords,
                Created = author.Created,
                Updated = author.Updated,
                CreatedByName = author.CreatedBy.Name,
                UpdatedByName = author.UpdatedBy.Name
            };
            return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            Author author = _authorService.FindById(id);
            if (author == null)
            {
                return Json(new { success = false, msg = string.Format("The {0} id'ye sahip haber kategorisi bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //delete author
                _authorService.Delete(id);
                return Json(new { success = true, msg = "Başarılı bir şekilde silme işlemi gerçekleşti" }, JsonRequestBehavior.AllowGet);
            }
        }
	}
}
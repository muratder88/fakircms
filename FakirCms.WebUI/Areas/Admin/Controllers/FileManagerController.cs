﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class FileManagerController : Controller
    {
        public object RootFolder = @"~\uploads\";
        public ActionResult Index()
        {
            return View();
        }

        [ValidateInput(false)]
        public ActionResult FileManagerPartial()
        {


            return PartialView("_FileManagerPartial", RootFolder);
        }
        [ValidateInput(false)]
        public ActionResult FileManagerEditorPartial()
        {


            return PartialView("_FileManagerEditorPartial", RootFolder);
        }

        public FileStreamResult FileManagerPartialDownload()
        {

            return FileManagerExtension.DownloadFiles("FileManager", @"~\uploads\");
        }

        public ActionResult Editor()
        {
            return View();
        }

        public ActionResult Browser()
        {
            return View();
        }
        public PartialViewResult GetImageBrowser()
        {
            return PartialView("_ImageBrowser", @"~\uploads\");
        }
        [ValidateInput(false)]
        public ActionResult FileManagerBrowserPartial()
        {
           
            return PartialView("_ImageBrowser", @"~\uploads\");
        }
    }

    public class CustomFileSystemProvider : FileSystemProviderBase
    {
        public CustomFileSystemProvider(string RootFolder)
            : base(RootFolder)
        {

        }

        public static void ValidateSiteEdit(FileManagerActionEventArgsBase e)
        {

        }
        public override IEnumerable<FileManagerFile> GetFiles(FileManagerFolder folder)
        {
            return base.GetFiles(folder);
        }

        public override void RenameFile(FileManagerFile file, string name)
        {

            base.RenameFile(file, this.RemoveTurhishCharecters(name));
        }

        public override void RenameFolder(FileManagerFolder folder, string name)
        {
            base.RenameFolder(folder, this.RemoveTurhishCharecters(name));
        }
        public override void CreateFolder(FileManagerFolder parent, string name)
        {
            base.CreateFolder(parent, this.RemoveTurhishCharecters(name));
        }
        public override void UploadFile(FileManagerFolder folder, string fileName, Stream content)
        {
            base.UploadFile(folder, this.RemoveTurhishCharecters(fileName), content);
        }

        private string RemoveTurhishCharecters(string name)
        {

            //Turkçe karekterleri kaldır

            name = name.Replace("ö", "o");
            name = name.Replace("ç", "c");
            name = name.Replace("ş", "s");
            name = name.Replace("ı", "i");
            name = name.Replace("ğ", "g");
            name = name.Replace("ü", "u");

            name = name.Replace("Ö", "O");
            name = name.Replace("Ç", "C");
            name = name.Replace("Ş", "S");
            name = name.Replace("I", "I");
            name = name.Replace("Ğ", "G");
            name = name.Replace("Ü", "U");


            return name;
        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakirCms.Domain.Services;
using FakirCms.Domain.Entities;
using FakirCms.WebUI.Areas.Admin.Models;
using System.Net;
using System.Data.Entity;

namespace FakirCms.WebUI.Areas.Admin.Controllers
{
    //[Authorize(Roles = "SuperAdmin,Admin")]
    public class UserController : Controller
    {
        private IUserService _userService;
        public UserController(IUserService userservice)
        {
            _userService = userservice;
        }
        public ActionResult Index()
        {
            IEnumerable<AppUser> users = _userService.Users;
            var item = (from user in users
                        select new
                        {
                            Id = user.Id,
                            Name = user.Name,
                            LastVisitDate = user.LastVisitDate.ToShortDateString(),
                            Active = user.Active,
                            Roles = _userService.GetUserRoles(user.Id)
                        }).ToArray();
            return View(item);
        }

        public JsonResult GetUsersTableData()
        {
            IEnumerable<AppUser> users = _userService.Users.AsQueryable().Include(x => x.Roles);
            var item = (from user in users
                        select new
                        {
                            Id = user.Id,
                            Name = user.Name,
                            LastVisitDate = user.LastVisitDate.ToShortDateString(),
                            Active = user.Active,
                            Roles = _userService.GetUserRoles(user.Id)
                        }).ToArray();
            return Json(new { data = item }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AppUser user = _userService.Users.Where(x => x.Id == id).FirstOrDefault();
            UserEditModel model = new UserEditModel()
            {
                Id = user.Id,
                Email = user.Email,
                Active = user.Active,
                UserName = user.UserName,
                Name = user.Name,
                Roles = _userService.GetUserRoles(user.Id),

            };

            ViewBag.RoleList = _userService.Roles.Select(x => x.Name).ToList();

            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserEditModel model)
        {
            if (ModelState.IsValid)
            {
                AppUser user = _userService.Users.Where(x => x.Id == model.Id).FirstOrDefault();
                user.Name = model.Name;
                user.UserName = model.UserName;
                user.Email = model.Email;
                user.Active = model.Active;
                //TODO: add user to database and remove user roles and set new roles, make respond whethever operations succeded or failed
                var result = _userService.UpdateUser(user);
                if (result.Succeeded)
                {
                    result = _userService.UpdateUserRoles(user.Id, model.Roles);
                    if (!result.Succeeded)
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError("", error.ToString());
                        }
                    }
                    else
                    {
                        TempData["success"] = "Başarılı bir şekilde kullanıcı bilgileri güncellendi";
                    }
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.ToString());
                    }
                }

            }
            ViewBag.RoleList = _userService.Roles.Select(x => x.Name).ToList();
            return View("Edit", model);
        }
        public ActionResult Create()
        {
            ViewBag.RoleList = _userService.Roles.Select(x => x.Name).ToList();
            return View(new UserCreateModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserCreateModel model)
        {
            if (ModelState.IsValid)
            {
                AppUser user=new AppUser(){
                    Email=model.Email,
                    Name=model.Name,
                    UserName=model.UserName,
                    Active=model.Active,
                    RegisterDate=DateTime.Now,
                    LastVisitDate = DateTime.Now
                };
               var createResult= _userService.CreateUser(user, model.Password);
               if (createResult.Succeeded)
               {
                   if (model.Roles != null && model.Roles.Count() > 0)
                   {
                       //Add Roles
                       foreach (var roleName in model.Roles)
                       {
                          _userService.AddUserToRole(user.UserName, roleName);
                       }    
                   }
                   TempData["success"] = "Kullanıcı başarılı bir şekilde oluşturuldu";
                   return RedirectToAction("Index","User");
               }
               else
               {
                   foreach (var error in createResult.Errors)
                   {
                       ModelState.AddModelError("", error.ToString());
                   }
                   ViewBag.RoleList = _userService.Roles.Select(x => x.Name).ToList();
                   return View("Create", model);
               }
            }
            else
            {
                ViewBag.RoleList = _userService.Roles.Select(x => x.Name).ToList();
                return View("Create", model);
            }
        }

        public ActionResult Delete(string id)
        {
            try
            {
                var result=_userService.DeleteUser(id);
                if (result.Succeeded)
                {
                    TempData["success"] = "Silme işlemi  başarılı bir şekilde gerçekleştirildi";
                }
                else
                {
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError("", item);
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return RedirectToAction("Index");
        }

        public ActionResult ResetPassword(string id)
        {
            AppUser user = _userService.FindUserById(id);
            if(user==null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            UserPasswordResetModel model = new UserPasswordResetModel()
            {
                Id = user.Id
            };

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(UserPasswordResetModel model)
        {
            if (ModelState.IsValid)
            {
                AppUser user = _userService.FindUserById(model.Id);
                if (user == null)
                {
                    ModelState.AddModelError("", "Kullancı bulunamadı sayfayı tekrardan oluşturun veya kullanıcı silinmiş olabilir");
                    return View(model);
                }
                var result = _userService.SetUserPassword(user.Id, model.Password);
                if (result.Succeeded)
                {
                    TempData["success"] = "Kullanıcı şifresi başarılı bir şekilde değiştirildi";
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    foreach (var errorString in result.Errors)
                    {
                        ModelState.AddModelError("", errorString);
                    }
                    return View(model);
                }

            }
            else
            {
                return View(model);
            }   
        }
    }
}
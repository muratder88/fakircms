﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakirCms.Domain;
using FakirCms.Domain.Entities;
using FakirCms.Domain.Services;
using FakirCms.WebUI.Areas.Admin.Models;

namespace FakirCms.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles="SperAdmin,Admin")]
    public class ArticleController : Controller
    {
        private IArticleService _articleService;
        private IAuthorService _authorService;
        public ArticleController(IArticleService articleService,IAuthorService authorService)
        {
            _articleService = articleService;
            _authorService = authorService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetData()
        {
            var resultData = (from article in _articleService.Items
                              orderby article.Id descending
                              select new
                              {
                                  Id = article.Id,
                                  Publish=article.State,
                                  Title = article.Title,
                                  Author = article.Author.Name,
                                  Date = article.Date.ToString("yyyy/MM/dd"),
                                  Hits = article.Hits
                              });
            return Json(new { data = resultData }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            CreateArticleModel model = new CreateArticleModel();
            model.Date = System.DateTime.Now;
            model.AuthorSelecList = _authorService.GetSelectList();
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateArticleModel model) 
        {
            if (ModelState.IsValid)
            {
                if (model.Id > 0)
                {
                    //make a update
                    Article article = _articleService.FindById(model.Id);
                    article.Title = model.Title;
                    article.Alias = model.Alias;
                    article.Content = model.Content;
                    article.Date = model.Date;
                    article.AuthorID = model.AuthorID;
                    article.State = model.Publish;
                    article.MetaTitle = model.MetaTitle;
                    article.MetaDescription = model.MetaDescription;
                    article.MetaKeywords = model.MetaKeywords;
                    var result = _articleService.Update(article);
                    if (result)
                    {
                        TempData["success"] = "Köşe yazısı başarılı bir şekilde güncellendi";
                    }
                    else
                    {
                        TempData["error"] = "Köse yazısı güncellenirken bir hatayla karşılaşıldı";
                        return RedirectToAction("Index", "Article");
                    }
                }
                else
                {
                    Article article = new Article();
                    article.Title = model.Title;
                    article.Alias = model.Alias;
                    article.Content = model.Content;
                    article.Date = model.Date;
                    article.AuthorID = model.AuthorID;
                    article.State = model.Publish;
                    article.MetaTitle = model.MetaTitle;
                    article.MetaDescription = model.MetaDescription;
                    article.MetaKeywords = model.MetaKeywords;
                    var result =_articleService.Create(article);
                    if (result)
                    {
                        TempData["success"] = "Köşe yazısı başarılı bir şekilde eklendi";
                        return RedirectToAction("Index", "Article");
                    }
                    else
                    {
                        TempData["error"] = "Köşe yazısı eklenirken bir hatayla karşılaşıldı";
                    }
                }
               
            }
            model.AuthorSelecList = _authorService.GetSelectList();
            return View(model);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            Article article = _articleService.FindById(id);
            if (article == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotFound);
            }
                CreateArticleModel model = new CreateArticleModel()
                {
                    Id = article.Id,
                    Title = article.Title,
                    Alias = article.Alias,
                    Date = article.Date,
                    MetaDescription = article.MetaDescription,
                    MetaKeywords = article.MetaKeywords,
                    MetaTitle = article.MetaTitle,
                    Content = article.Content,
                    Publish = article.State,
                    
                };
                model.AuthorSelecList = _authorService.GetSelectList();
                return View("Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            Article article =_articleService.FindById(id);
            if (article == null)
            {
                return Json(new { success = false, msg = string.Format("The {0} id'ye sahip köşe yazısı bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //delete aciklama
                _articleService.Delete(id);
                return Json(new { success = true, msg = "Başarılı bir şekilde silme işlemi gerçekleşti" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Publish(int id)
        {
            Article article = _articleService.FindById(id);
            if (article == null)
            {
                return Json(new { success = false, msg = string.Format("{0} id'ye sahip köşe yazısı bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //update publish property
                article.State = true;
                _articleService.Update(article);
                return Json(new { success = true, msg = "Köşe yazısı başarılı bir şekilde yayınlandı" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UnPublish(int id)
        {
            Article article = _articleService.FindById(id);
            if (article == null)
            {
                return Json(new { success = false, msg = string.Format("The {0} id'ye sahip köşe yazısı bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //update publish property
                article.State = false;
                _articleService.Update(article);
                return Json(new { success = true, msg = "Köşe yazısı başarılı bir şekilde yayından kaldırıldı" }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
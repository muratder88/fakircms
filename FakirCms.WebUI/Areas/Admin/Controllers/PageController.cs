﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Exceptions;
using FakirCms.Domain.Services;
using FakirCms.WebUI.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin,Admin")]
    public class PageController : Controller
    {
        private IPageService _pageService;
        private IUserService _userService;
        public PageController(IPageService pageService, IUserService userService)
        {
            _pageService = pageService;
            _userService = userService;
        }
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetData()
        {
            var resultData = (from page in _pageService.items
                              select new
                              {
                                  Id=page.Id,
                                  Title=page.Title,
                                  Publish=page.State,
                                  Author=page.CreatedBy.Name,
                                  Hits=page.Hits
                              }).ToArray();
            return Json(new { data = resultData }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            PageCreateModel model = new PageCreateModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PageCreateModel model)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    if (model.Id > 0)
                    {
                        //make a update
                        Page page = _pageService.FindById(model.Id);
                        page.Title = model.Title;
                        page.Alias = model.Alias;
                        page.Content = model.Content;
                        page.State = model.IsActive;
                        page.MetaTitle = model.MetaTitle;
                        page.MetaDescription = model.MetaDescription;
                        page.MetaKeywords = model.MetaKeywords;
                        page.CustomCss = model.CustomCss;
                        page.CustomJs = model.CustomJs;
                        var result = _pageService.Update(page);
                        if (result)
                        {
                            TempData["success"] = "Sayfa başarılı bir şekilde güncelleştirildi";
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Sayfa güncelleştirilirken bir hatayla oluştu");
                        }
                    }
                    else
                    {
                        //create page
                        Page page = new Page();
                        //check model alias not exist
                        page.Title = model.Title;
                        page.Alias = model.Alias;
                        page.Content = model.Content;
                        page.State = model.IsActive;
                        page.MetaTitle = model.MetaTitle;
                        page.MetaDescription = model.MetaDescription;
                        page.MetaKeywords = model.MetaKeywords;
                        page.CustomJs = model.CustomJs;
                        page.CustomCss = model.CustomCss;
                        var result = _pageService.Create(page);
                        if (result)
                        {
                            TempData["success"] = "Sayfa başarılı bir şekilde oluşturuldu";
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Sayfa oluşturulurken bir hatayla oluştu");
                        }
                    }
                }
                catch (InsertDuplicateException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }

            }
            return View(model);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            Page page = _pageService.FindById(id);
            if (page == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                PageCreateModel model = new PageCreateModel()
                {
                    Id = page.Id,
                    Alias = page.Alias,
                    Created = page.Created,
                    CreatedByName = page.CreatedBy.Name,
                    Updated = page.Updated,
                    UpdatedByName = page.UpdatedBy.Name,
                    Title = page.Title,
                    IsActive = page.State,
                    Content = page.Content,
                    MetaTitle = page.MetaTitle,
                    MetaDescription = page.MetaDescription,
                    MetaKeywords = page.MetaKeywords,
                    CustomCss=page.CustomCss,
                    CustomJs=page.CustomJs
                };
                return View("Create", model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(int id)
        {
            Page page = _pageService.FindById(id);
            if (page == null)
            {
                return Json(new { success = false, msg = string.Format("The {0} id'ye sahip öğe bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //delete aciklama
                _pageService.Delete(id);
                return Json(new { success = true, msg = "Başarılı bir şekilde silme işlemi gerçekleşti" }, JsonRequestBehavior.AllowGet);
            }

        }

         [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Publish(int id)
        {
            Page page = _pageService.FindById(id);
            if (page == null)
            {
                return Json(new { success = false, msg = string.Format("{0} id'ye sahip öğe bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //update publish property
                page.State = true;
                _pageService.Update(page);
                return Json(new { success = true, msg = "Öğe başarılı bir şekilde yayınlandı" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UnPublish(int id)
        {
            Page page = _pageService.FindById(id);
            if (page == null)
            {
                return Json(new { success = false, msg = string.Format("The {0} id'ye sahip öğe bulunamadı", id) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //update publish property
                page.State = false;
                _pageService.Update(page);
                return Json(new { success = true, msg = "Öğe başarılı bir şekilde yayından kaldırıldı" }, JsonRequestBehavior.AllowGet);
            }
        }
	}

	
}
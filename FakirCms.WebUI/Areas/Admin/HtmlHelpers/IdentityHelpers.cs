﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Autofac.Integration.Mvc;
using FakirCms.Domain;
using Autofac;
using FakirCms.Domain.Entities;
using FakirCms.Domain.Manager;
namespace FakirCms.WebUI.Areas.Admin.HtmlHelpers
{
    public static class IdentityHelpers
    {
        public static MvcHtmlString GetUserName(this HtmlHelper html, string roleId)
        {
            AppRoleManager roleManager = AutofacDependencyResolver.Current.RequestLifetimeScope.Resolve<AppRoleManager>();
            AppRole role = roleManager.Roles.Where(x => x.Id == roleId).FirstOrDefault();
            return new MvcHtmlString(role.Name);
        }
    }
}
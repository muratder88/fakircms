﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using FakirCms.WebUI.Areas.Admin.HtmlHelpers;
using FakirCms.WebUI.Areas.Admin.Models;


namespace FakirCms.WebUI.Areas.Admin.HtmlHelpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this System.Web.Mvc.HtmlHelper html,IPagedList list,PagingInfo pagingInfo)
        {
            StringBuilder result = new StringBuilder();
            TagBuilder rowDiv = new TagBuilder("div");
            rowDiv.AddCssClass("row");
            rowDiv.MergeAttribute("id", "AdminTablePagination");

            TagBuilder solDiv = new TagBuilder("div");
            solDiv.AddCssClass("col-xs-6");
            solDiv.AddCssClass("soldiv");
            TagBuilder dataTablesInfo = new TagBuilder("div");
            dataTablesInfo.AddCssClass("dataTables_info");


            result.Append("Total Items:").Append(pagingInfo.TotalItems);
            
            MvcHtmlString itemsPerPage = System.Web.Mvc.Html.SelectExtensions.DropDownList(html, "PagingInfo.ItemsPerPage", new List<SelectListItem>{
                new SelectListItem(){Text="10",Value="10",Selected=(pagingInfo.ItemsPerPage==10)},
                new SelectListItem(){Text="20",Value="20",Selected=(pagingInfo.ItemsPerPage==20)},
                new SelectListItem(){Text="30",Value="30",Selected=(pagingInfo.ItemsPerPage==30)},
                new SelectListItem(){Text="40",Value="40",Selected=(pagingInfo.ItemsPerPage==40)},
                new SelectListItem(){Text="50",Value="50",Selected=(pagingInfo.ItemsPerPage==50)},
                new SelectListItem(){Text="100",Value="100",Selected=(pagingInfo.ItemsPerPage==100)}
            }, new { @class = "filter-input",@id="PagingInfo.ItemsPerPage"});

            result.Append(" Bir Sayfada:").Append(itemsPerPage.ToHtmlString());

            dataTablesInfo.InnerHtml = result.ToString();
            solDiv.InnerHtml = dataTablesInfo.ToString();
            rowDiv.InnerHtml = solDiv.ToString();

            StringBuilder sagdiv = new StringBuilder();
            sagdiv.Append("<div class='col-xs-6 sagdiv'>");
            sagdiv.Append(html.PagedListPager(list, page => page.ToString()).ToHtmlString().ToString());
            sagdiv.Append("</div>");



            rowDiv.InnerHtml += sagdiv.ToString();

            return MvcHtmlString.Create(rowDiv.ToString());
        }
    }
}
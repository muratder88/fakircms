﻿

(function ($) {
    $.widget("custom.ContentIndexView", {
        table: "",
        tableApi:"",
        options: {
            tableId: "",
            tableData: "",
        },
        _setOption: function (name, value) {
            console.log("SetOptions Work");
            $.Widget.prototype._setOption.apply(this, arguments);
        },
        _init: function () {
            var that=this;
            this._on(this.element, {
                "click a.listItemTask": function (event) {
                    var element = event.currentTarget;
                    //get rowData json object
                    rowIndex = $(element).data("rowindex");
                    var rowData = that.table.fnGetData(rowIndex);
                    var task = $(element).data("task");
                    that.tableApi.ajax.reload(null, false);
                    switch (task) {
                        case "publish":
                            that.publish(rowData.Id);
                            break;
                        case "unpublish":
                            that.unpublish(rowData.Id);
                            break;
                        case "delete":
                            that.remove(rowData.Id);
                            break;
                        default:
                            alert("task is undefined or task doesn't exist");
                            break;
                    }
                }
            });
            this._on(this.element, {
                "change .filter-input": function (event) {
                 
                    that.table.fnDraw();
                }
            });
           
        },
        _create: function () {
            var that = this;
            tbl = $(this.element).find("#" + this.options.tableId);

            var table = $(tbl).dataTable({
                "responsive": true,
                "processing":true,
                "autoWidth":false,
                "ajax": {url:"/Admin/Content/GetData",type:"GET"},
                "columns": [
                    { data: "Title", title: "Başlık", type: "string" },
                    {
                        data: "Publish", title: "Aktif",type:"html-num",render: function (data, type, row,meta) {
                            var html = "";
                            if (row.Publish == 0) {
                                html += " <a class='listItemTask' data-task='publish' data-rowindex='" + meta.row + "'>";
                                html += "<img src='/Areas/Admin/Images/publish_x.png'   border='0' alt='Yayınlanmamış'>";
                                html += "<span style='display:none'>" + row.Publish + "</span>";
                                html += "</a>"
                            }
                            else {
                                html += "<a class='listItemTask' data-task='unpublish' data-rowindex='" + meta.row + "'>";
                                html += "<img src='/Areas/Admin/Images/publish_g.png' border='0' alt='Yayınlanmamış' >";
                                html += "<span style='display:none'>" + row.Publish + "</span>";//required for searching
                                html += "</a>";
                            }
                            return html;
                        }
                    },
                    { data: "Access", title: "Erişim", type: "string" },
                    { data: "Section", title: "Bölüm", type: "string" },
                    { data: "Category", title: "Kategori", type: "string" },
                    { data: "Author", title: "Yazar", type: "string", type: "string" },
                    { data: "Created", title: "Tarih", type: "date" },
                    { data: "Hits", title: "izlenim", type: "num" },
                    { data: "Id", title: "Kimlik",text: "center", type: "num" },
                    {
                        data: null, title: "", searchable: false, orderable: false, render: function (data,type,row,meta)
                        {
                            html="";
                            html += " <a class='btn btn-sm btn-default listItemTask' data-task='delete' data-rowindex='" + meta.row + "' href='#'>";
                            html+="<span class='glyphicon glyphicon-trash'></span>";
                            html+=" </a>";
                            html+="<a class='btn btn-sm btn-default' href='#'>";
                            html+=" <span class='glyphicon glyphicon-pencil'></span></a>";
                            return html;
                        }
                    }
                ]
            });
            this.table = table;
            this.tableApi = table.api();
        },
        _showAjaxError: function (jqXHR, textStatus, errorThrown, url) {
            var html = "";
            if (jqXHR.status === 404) {
                html = "<p> Ajax Error:" + url + " url için sayfa bulunamadı";
            } else {
                html = '<h1>Ajax Error</h1>' +
               '<p>The Ajax call returned the following error: ' +
               jqXHR.statusText + '.</p>';
            }
            
            this._createAlertDiv(html);
        },
        //Make alert div prepend to boxbody or modal body
        _createAlertDiv: function (msg) {
            var div = "<div class='alert alert-danger alert-dismissable'><i class='fa fa-ban'></i>";
            div += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"
            div += " <b>Error</b>" + msg + "</div>";
            if (!this.options.modal)
                var boxbody = $(this.element).find(".box-body");
            else
                var boxbody = $(this.element).find(".modal-body");
            $(boxbody).prepend(div);
        },
        refreshTable: function () {
        },
        unpublish:function(id){
            var that = this;
            url = "Admin/Content/unpublish?id=" + id;
            var jqxhr = $.get(url);
            jqxhr.done(function (data) {
                alert(data);
            });
            jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                that._showAjaxError(jqXHR, textStatus, errorThrown, url);
            });
        },
        publish:function(id){
            var that=this;
            url="Admin/Content/Publish?id="+id;
            var jqxhr=$.get(url);
            jqxhr.done(function(data)
            {
                alert(data);
            });
            jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                that._showAjaxError(jqXHR,textStatus,errorThrown,url);
            });
        },
        remove: function (id)
        {
            var that = this;
            url = "Admin/Content/Delete?id=" + id;
            var jqxhr = $.get(url);
            jqxhr.done(function (data) {
                alert(data);
            });
            jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                that._showAjaxError(jqXHR, textStatus, errorThrown,url);
            });
        }
    });
   
    //Section Filtering
    $.fn.dataTableExt.afnFiltering.push(function (settings, data, dataIndex) {
        var sectionVal = $("#Section").val();
        var sectionText = $("#Section").find("option:selected").text()
        sectionVal=parseInt(sectionVal);
        if(sectionVal==0){
            return true;
        }else{
            sectionData =data[3];
            sectionData = sectionData.toLowerCase();
            sectionText = sectionText.toLowerCase();
            if (sectionData == sectionText)
                return true;
            else
                return false;
        }
       
    });
    //Category Filtering
    $.fn.dataTableExt.afnFiltering.push(function (setttings, data, dataIndex) {
        var categoryColumnNumber = 4;
        var value = $("#Category").val();
        var text = $("#Category").find("option:selected").text();
        value = parseInt(value);
        if (value > 0) {
            var category = data[categoryColumnNumber];
            category = category.toLowerCase();
            text=text.toLowerCase();
            if (category == text)
                return true;
            else
                return false;
        } else {
            return true;
        }
        return true;
    });

    //Publish Filtering
    $.fn.dataTableExt.afnFiltering.push(function (settings, data, dataIndex) {
        var publishColumnNumber = 1;
        var inputValue = $("#Publish").val();
        inputValue = parseInt(inputValue);
        if (inputValue > 0) {
            publish = data[publishColumnNumber];
            publish = parseInt(publish);
            if (publish == inputValue)
                return true;
            else
                return false;
        } else {
            return true;
        }
    });
    //User Filtering
    $.fn.dataTableExt.afnFiltering.push(function (settings, data, dataIndex) {
        var userColumnNumber = 5;
        inputValue = $("#User").val();
        inputText = $("#User").find("option:selected").text();
        inputValue = parseInt(inputValue);
        if (inputValue > 0) {
            user = data[userColumnNumber];
            user = user.toLowerCase();
            inputText = inputText.toLowerCase();
            if (inputText == user)
                return true;
            else
                return false;
        } else {
            return true;
        }
    });

   
}(jQuery));













﻿(function ($) {
    "use strict"
    $.widget("page.UserIndexView", {
        table: "",//JQuery DataTable object
        tableApi: "",// created for data tables api functionalitaliy
        options: {
            tableId: "",
            tableDate: "",
        },
        _init: function () {
            //widget reference
            var that = this;
            this._on(this.element, {
                "click a.listItemTask": function (event) {
                    var element = event.currentTarget;
                    //get rowData json object
                    var rowIndex = $(element).data("rowindex");
                    var rowData = that.table.fnGetData(rowIndex);
                    var task = $(element).data("task");
                    that.tableApi.ajax.reload(null, false);
                    switch (task) {
                        case "active":
                            that.active(rowData.Id);
                            break;
                        case "passive":
                            that.passive(rowData.Id);
                            break;
                        case "delete":
                            that.deleteItem(rowData.Id);
                            break;
                        case "edit":
                            that.edit(rowData.Id);
                            break;
                        default:
                            alert("task is undefined or task doesn't exist");
                            break;
                    }
                }
            });
            this._on(this.element, {
                "change .filter-input": function (event) {

                    that.table.fnDraw();
                }
            });
        },
        _create: function () {
            var that = this;

            var tbl = $(this.element).find("#" + this.options.tableId);
            if (tbl === undefined)
                throw Error("Table Id doesn't exist current context");

            var table = $(tbl).dataTable({
                "responsive": true,
                "proccessing": true,
                "autoWidth": false,
                "ajax": { url: "/Admin/User/GetUsersTableData", type: "GET" },
                "columns": [
                    { data: "Name", title: "Kullanıcı Adı", type: "string" },
                    {
                        data: "Active", title: "Aktif", type: "html-num", render: function (data, type, row, meta) {
                            var html = "";
                            if (row.Publish == false) {
                                html += " <a class='listItemTask' data-task='active' data-rowindex='" + meta.row + "'>";
                                html += "<img src='/Areas/Admin/Images/publish_x.png'   border='0' alt='Aktif Değil'>";
                                html += "<span style='display:none'>" + row.Active + "</span>";//required for searching
                                html += "</a>"
                            }
                            else {
                                html += "<a class='listItemTask' data-task='passive' data-rowindex='" + meta.row + "'>";
                                html += "<img src='/Areas/Admin/Images/publish_g.png' border='0' alt='Aktif' >";
                                html += "<span style='display:none'>" + row.Active + "</span>";//required for searching
                                html += "</a>";
                            }
                            return html;
                        }
                    },
                    {
                        data: "Roles", title: "Kullanıcı Rolleri", type: "string", render: function (data, type, row, meta) {
                            var html = "";
                            if (data !== undefined && data !== null) {
                                return data.join();
                            } else {
                                return html;
                            }
                        }
                    },
                    { data: "LastVisitDate", title: "Son Ziyaret Tarihi", type: "date" },
                     {
                         data: null, title: "", searchable: false, orderable: false, render: function (data, type, row, meta) {
                             var editUrl = "/Admin/User/Edit?id=" + row.Id;
                             var deleteUrl = "/Admin/User/Delete?id=" + row.Id;
                             var resetPasswordUrl = "/Admin/User/ResetPassword?id=" + row.Id;
                             var html = "";
                             html += " <a class='btn btn-sm btn-default' href='"+deleteUrl+"'>";
                             html += "<span class='glyphicon glyphicon-trash'></span>";
                             html += " </a>";
                             html += "<a class='btn btn-sm btn-default' data-task='edit'  href='"+editUrl+"'>";
                             html += " <span class='glyphicon glyphicon-pencil'></span></a>";
                             html += "<a class='btn btn-sm btn-danger' data-task='resetPassword'  href='" + resetPasswordUrl + "'>";
                             html += "Şifre Değiştir</a>";
                             return html;
                         }
                     }
                ]
            });
            this.table = table;
            this.tableApi = table.api();
        },
        _showAjaxError: function (jqXHR, textStatus, errorThrown, url) {
            var html = "";
            if (jqXHR.status === 404) {
                html = "<p> Ajax Error:" + url + " url için sayfa bulunamadı";
            } else {
                html = '<h1>Ajax Error</h1>' +
               '<p>The Ajax call returned the following error: ' +
               jqXHR.statusText + '.</p>';
            }

            this._createAlertDiv(html);
        },
        //Make alert div prepend to boxbody or modal body
        _createAlertDiv: function (msg) {
            var div = "<div class='alert alert-danger alert-dismissable'><i class='fa fa-ban'></i>";
            div += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"
            div += " <b>Error</b>" + msg + "</div>";
            if (!this.options.modal)
                var boxbody = $(this.element).find(".box-body");
            else
                var boxbody = $(this.element).find(".modal-body");
            $(boxbody).prepend(div);
        },
        active: function (id) {
            var that = this;
            url = "Admin/Content/Delete?id=" + id;
            var jqxhr = $.get(url);
            jqxhr.done(function (data) {
                alert(data);
            });
            jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                that._showAjaxError(jqXHR, textStatus, errorThrown, url);
            });
        },
        passive: function (id) {
            var that = this;
            url = "Admin/Content/Delete?id=" + id;
            var jqxhr = $.get(url);
            jqxhr.done(function (data) {
                alert(data);
            });
            jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                that._showAjaxError(jqXHR, textStatus, errorThrown, url);
            });
        },

        deleteItem: function (id) {
            var that = this;
            url = "Admin/Content/Delete?id=" + id;
            var jqxhr = $.get(url);
            jqxhr.done(function (data) {
                alert(data);
            });
            jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                that._showAjaxError(jqXHR, textStatus, errorThrown, url);
            });
        },
        edit: function (id) {
            var that = this;
            var url = "Admin/Content/Edit?id=" + id;
            jqxhr = $.get(url);
            jqxhr.done(function (data) {
                alert(data);
            });
            jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                that._showAjaxError(jqXHR, textStatus, errorThrown, url);
            });
        }


    });
}(jQuery));
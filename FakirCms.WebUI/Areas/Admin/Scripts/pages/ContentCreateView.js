﻿/// <reference path="../../../Scripts/jquery-2.1.0-vsdoc.js" />
/// <reference path="../../../Scripts/jquery-ui-1.11.4.js" />


(function ($) {

    $.widget("custom.ContentCreateForm", {
       
        options:{
            txtTitleName: "mn",
            txtAliasName: "mnsdf",
            txtCreatedName: "asdfasdf",
            txtFullTextName: "Fasdfasdf",
            ddlSectionName:"SectionID",
            ddlTags: "Tags",
            ddlCategory: "CategoryID",
            ddlAccess:"Access"

        },
        _create: function () {
            console.log("ContentCreateForm:create() Function works");

           

            var self = this;
            //Add Events
            //Save Button Click Event
            this._on(this.element, {
                "click #btnSave": function (e) {
                    this.submit();
                }
            });

            //Section Dropdown Value Changed Event
            this._on(this.element.find("#"+this.options.ddlSectionName), {
                "change": function (e) {
                    console.log("ddlSection change event")
                    var sectionid = e.currentTarget.value;
                    var url="/admin/content/GetSectionCategories?sectionID="+sectionid;
                    if (sectionid != 0) {
                        //ajax ile bu sectiondaki kategorileri getir
                        
                        $("#" + self.options.ddlCategory).find("option").remove();
                        $("#" + self.options.ddlCategory).append("<option value=0>..Yenileniyor...</option>")
                        //Chosen dropbox can be updated
                        $("#" + self.options.ddlCategory).trigger("chosen:updated");

                        var jqXhr = $.getJSON(url, function (data) {
                            //Json data List<Category> or if id is wrong it returns null
                            if (data !== null) {
                                $("#" + self.options.ddlCategory).find("option").remove();
                               
                                $.each(data, function (index, category) {
                                    //Add #CategoryID dropdownbox options
                                    var optionString = "<option value='" + category.CategoryID + "'>" + category.CategoryTitle + "</option>";
                                    $("#"+self.options.ddlCategory).append(optionString)
                                });
                                //Chosen dropbox can be updated
                                $("#" + self.options.ddlCategory).trigger("chosen:updated");
                            } else {
                                alert("Hatalı id girildi");
                            }
                        })
                        jqXhr.fail(function () {
                            alert(url + " ajax request çalışırken hata oluştu");
                        });
                    }
                   
                }
            });

            //when ContentTitle textbox changed, create alias string for alias textbox
            var titleTextBoxSelect= "input[name='"+self.options.txtTitleName+ "']";
            this._on(this.element.find(titleTextBoxSelect), {
                "change": function (e) {
                    var str = $(e.currentTarget).val();
                    var alias = self._convertToAlias(str);
                    $("input[name='"+self.options.txtAliasName+ "']").val(alias);
                }
            });

        },
        _init:function(){
            console.log("ContentCreateForm:_init()  works");
            $("#accordion").accordion();
            //multiple selecbox
           
                var availableTags = [
                  "ActionScript",
                  "AppleScript",
                  "Asp",
                  "BASIC",
                  "C",
                  "C++",
                  "Clojure",
                  "COBOL",
                  "ColdFusion",
                  "Erlang",
                  "Fortran",
                  "Groovy",
                  "Haskell",
                  "Java",
                  "JavaScript",
                  "Lisp",
                  "Perl",
                  "PHP",
                  "Python",
                  "Ruby",
                  "Scala",
                  "Scheme"
                ];
               

                $("#tags")
                  // don't navigate away from the field on tab when selecting an item
                  .bind("keydown", function (event) {
                      if (event.keyCode === $.ui.keyCode.TAB &&
                          $(this).autocomplete("instance").menu.active) {
                          event.preventDefault();
                      }
                  })
                  .autocomplete({
                      minLength: 0,
                      source: function (request, response) {
                          // delegate back to autocomplete, but extract the last term
                          response($.ui.autocomplete.filter(
                            availableTags, extractLast(request.term)));
                      },
                      focus: function () {
                          // prevent value inserted on focus
                          return false;
                      },
                      select: function (event, ui) {
                          var terms = split(this.value);
                          // remove the current input
                          terms.pop();
                          // add the selected item
                          terms.push(ui.item.value);
                          // add placeholder to get the comma-and-space at the end
                          terms.push("");
                          this.value = terms.join(", ");
                          return false;
                      }
                  });
            

            //Button

            // Dialog
            $("#dialog").dialog();
            //spiner
            var spinner = $("#spinner").spinner();
            $("#" + this.options.txtCreatedName).datetimepicker();

            //Tags dropdownlist
            $("#" + this.options.ddlTags).chosen({
                no_results_text: "Sonuç Bulunamadı",
                allow_single_deselect: true,
                disable_search:false,
                placeholder_text_multiple:"Makale Etiketi Seçin"
            });

            //Category dropdownlist
            $("#" + this.options.ddlCategory).chosen({ disable_search: true });

            //Section Drodownlist
            $("#" + this.options.ddlSectionName).chosen({ disable_search: true });

            //Access dropdowlist
            $("#" + this.options.ddlAccess).chosen({ disable_search: true });

            CKEDITOR.replace(this.options.txtFullTextName, {
                filebrowserBrowseUrl: '/admin/filemanager/editor',
                filebrowserImageWindowWidth: '640',
                filebrowserImageWindowHeight: '600',
                extraPlugins: 'pagebreak'
            });
        },
       
        _convertToAlias: function (str) {
            if(typeof(str)!=="string")
                throw new Error("string olmalı");
            var value = "";
            
            
            //space convert -
            value = str;
            // \s space charecter
            value = value.trim().replace(/[\s]/g, "-");
            value = value.toLowerCase();
            //Turkçe karekterleri kaldır
            //Convert Characters
            value = value.replace(/[ö]/g, "o");
            value = value.replace(/[ç]/g,"c");
            value = value.replace(/[ş]/g, "s");
            value = value.replace(/[ı]/g, "i");
            value = value.replace(/[ğ]/g, "g");
            value = value.replace(/[ü]/g, "u");
           

            //remove non '" special chars
            value = value.replace(/[^a-z0-9^-]/g, "");
            return value;

        }

    });
    
}(jQuery));
/*	days: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi", "Pazar"],
daysShort: ["Pz", "Pzt", "Sal", "Çrş", "Prş", "Cu", "Cts", "Pz"],
daysMin: ["Pz", "Pzt", "Sa", "Çr", "Pr", "Cu", "Ct", "Pz"],
months: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
monthsShort: ["Oca", "Şub", "Mar", "Nis", "May", "Haz", "Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"],
today: "Bugün",
format: "dd.mm.yyyy"*/















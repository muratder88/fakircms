﻿(function ($) {

    $.widget("custom.HaberCreateView", {

        _init: function () {
            var that = this;

            // create maxlength plugin for Textbox has maximum length
            this._initMaxLengthInputs();

            //when title texbox change,create alias
            this._on(this.element, {
                "change #Title": function (event) {
                    var str = $(event.currentTarget).val();
                    var alias = that._convertToAlias(str);
                    $("#Alias").val(alias);
                }
            });
            this._on(this.element, {
                "click #btnTagRefresh": function (event) {
                    var url = "/Admin/Tag/GetSelectTags";
                    var element = event.currentTarget;
                    $(element).html("Yenileniyor....");
                    var jqXhr = $.getJSON(url, function (data) {
                        //Json data List<Tag>
                        if (data !== null) {
                            //remove options
                            var values = $("#Tags").select2("val");
                            $("#Tags").find("option").remove();
                            $("#Tags").select2("destroy");
                            $("#Tags").select2({
                                data: data,
                                placeholder: "Etiket seç",
                                tags: true,
                            }).trigger("change");
                            $("#Tags").select2("val", values);
                        } else {
                            alert("Hata oluştur");
                        }
                        $(element).html("Yenile");
                    });
                    jqXhr.fail(function () {
                        alert(url + " ajax request çalışırken hata oluştu");

                    });
                }
            });
        },

        _create: function () {
            var that = this;
            $("#Tags").select2({
                placeholder: "Etiket seç",
                tags: true,
            });

            $('#ImageBrowserContainer').ImageBox({ targetId: 'ImageUrl', imagePreviewId: 'image-preview' });

            $("#Date").datetimepicker({
                language: "tr",
                format: 'd/m/Y H:i',
                mask: true
            });
            CKEDITOR.replace("IntroText", {
                filebrowserBrowseUrl: '/admin/filemanager/editor',
                filebrowserImageWindowWidth: '640',
                filebrowserImageWindowHeight: '600',
                extraPlugins: 'pagebreak'
            });

            CKEDITOR.replace("FullText", {
                filebrowserBrowseUrl: '/admin/filemanager/editor',
                filebrowserImageWindowWidth: '640',
                filebrowserImageWindowHeight: '600',
                extraPlugins: 'pagebreak'
            });
          

        },
        _initMaxLengthInputs: function () {
            var elements = this.element.find("[data-val-maxlength-max]");
            if ($(elements).size() > 0) {
                $.each(elements, function (index, element) {
                    var maxValue = $(element).data("val-maxlength-max");
                    maxValue = parseInt(maxValue);
                    $(element).maxlength({
                        alwaysShow: true,
                        placement: 'top-left',
                        customMaxAttribute: "data-val-maxlength-max"
                    });
                });
            }
        },
        _convertToAlias: function (str) {
            if (typeof (str) !== "string")
                throw new Error("string olmalı");
            var value = "";
            //space convert -
            value = str;
            // \s space charecter
            value = value.trim().replace(/[\s]/g, "-");
            value = value.toLowerCase();
            //Turkçe karekterleri kaldır
            //Convert Characters
            value = value.replace(/[ö]/g, "o");
            value = value.replace(/[ç]/g, "c");
            value = value.replace(/[ş]/g, "s");
            value = value.replace(/[ı]/g, "i");
            value = value.replace(/[ğ]/g, "g");
            value = value.replace(/[ü]/g, "u");
            //remove non '" special chars
            value = value.replace(/[^a-z0-9^-]/g, "");
            return value;
        }
    });
}(jQuery));
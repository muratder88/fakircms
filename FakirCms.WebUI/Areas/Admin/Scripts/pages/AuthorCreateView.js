﻿(function ($) {
    $.widget("custom.AuthorCreateView", {
        _init: function () {
            var that = this;

            // create maxlength plugin for Textbox has maximum length
            this._initMaxLengthInputs();

            //when title texbox change,create alias
            this._on(this.element, {
                "change #Name": function (event) {
                    var str = $(event.currentTarget).val();
                    var alias = that._convertToAlias(str);
                    $("#Alias").val(alias);
                }
            });
        },

        _create: function () {
            var that = this;

            $('#ImageBrowserContainer').ImageBox({ targetId: 'Image', imagePreviewId: 'image-preview' });
        },
        _initMaxLengthInputs: function () {
            var elements = this.element.find("[data-val-maxlength-max]");
            if ($(elements).size() > 0) {
                $.each(elements, function (index, element) {
                    var maxValue = $(element).data("val-maxlength-max");
                    maxValue = parseInt(maxValue);
                    $(element).maxlength({
                        alwaysShow: true,
                        placement: 'top-left',
                        customMaxAttribute: "data-val-maxlength-max"
                    });
                });
            }
        },
        _convertToAlias: function (str) {
            if (typeof (str) !== "string")
                throw new Error("string olmalı");
            var value = "";
            //space convert -
            value = str;
            // \s space charecter
            value = value.trim().replace(/[\s]/g, "-");
            value = value.toLowerCase();
            //Turkçe karekterleri kaldır
            //Convert Characters
            value = value.replace(/[ö]/g, "o");
            value = value.replace(/[ç]/g, "c");
            value = value.replace(/[ş]/g, "s");
            value = value.replace(/[ı]/g, "i");
            value = value.replace(/[ğ]/g, "g");
            value = value.replace(/[ü]/g, "u");
            //remove non '" special chars
            value = value.replace(/[^a-z0-9^-]/g, "");
            return value;
        }
    });
}(jQuery));
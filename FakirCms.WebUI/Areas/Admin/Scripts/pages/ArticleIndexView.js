﻿
(function ($) {
    "use strict"
    $.widget("page.ArticleIndexView", {
        table: "",
        options: {
            controller: "Article",
            tableId: "AdminTable"
        },
        _init: function () {
            //widger reference
            var that = this;

            //listItemTask Click Event
            this._on(this.element, {
                "click a.listItemTask": function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                    var element = event.currentTarget;
                    //get rowData json object
                    var rowIndex = $(element).data("rowindex");
                    var rowData = that.table.fnGetData(rowIndex);
                    var task = $(element).data("task");

                    switch (task) {
                        case "publish":
                            that.publish(rowData.Id);
                            break;
                        case "unpublish":
                            that.unpublish(rowData.Id);
                            break;
                        case "delete":
                            that.remove(rowData.Id);
                            break;
                        default:
                            alert("task is undefined or task doesn't exist");
                            break;
                    }
                }
            });
            this._on(this.element, {
                "change .filter-input": function (event) {
                    that.table.fnDraw();
                }
            });

        },
        _create: function () {
            var that = this;
            var tbl = $(this.element).find("#" + this.options.tableId);
            var ajaxUrl = "/Admin/" + this.options.controller + "/GetData";
            var table = $(tbl).dataTable({
                "responsive": true,
                "processing": true,
                "autoWidth": false,
                "order": [[5, "desc"]],
                "ajax": { url: ajaxUrl, type: "GET" },
                "columns": [
                    { data: "Title", title: "Başlık", type: "string" },
                    {
                        data: "Publish", title: "Aktif", type: "html-num", render: function (data, type, row, meta) {
                            var html = "";
                            if (row.Publish == 0) {
                                html += " <a class='btn btn-sm btn-default listItemTask' data-task='publish' data-rowindex='" + meta.row + "'>";
                                html += "<img src='/Areas/Admin/Images/publish_x.png'   border='0' alt='Yayınlanmamış'>";
                                html += "<span style='display:none'>" + row.Publish + "</span>";
                                html += "</a>"
                            }
                            else {
                                html += "<a class='btn btn-sm btn-default listItemTask' data-task='unpublish' data-rowindex='" + meta.row + "'>";
                                html += "<img src='/Areas/Admin/Images/publish_g.png' border='0' alt='Yayınlanmamış' >";
                                html += "<span style='display:none'>" + row.Publish + "</span>";//required for searching
                                html += "</a>";
                            }
                            return html;
                        }
                    },
                    { data: "Author", title: "Yazar", type: "string" },
                    { data: "Date", title: "Tarih", type: "date" },
                    { data: "Hits", title: "İzlenim", text: "center", type: "num" },
                    { data: "Id", title: "Kimlik", text: "center", type: "num" },
                    {
                        data: null, title: "", searchable: false, orderable: false, render: function (data, type, row, meta) {
                            var html = "";
                            html += " <a class='btn btn-sm btn-default listItemTask' data-task='delete' data-rowindex='" + meta.row + "' href='#'>";
                            html += "<span class='glyphicon glyphicon-trash'></span>";
                            html += " </a>";
                            html += "<a class='btn btn-sm btn-default' href='/Admin/Haber/Edit?id=" + row.Id + "'>";
                            html += " <span class='glyphicon glyphicon-pencil'></span></a>";
                            return html;
                        }
                    }
                ]
            });
            this.table = table;
        },
        unpublish: function (id) {
            var that = this;
            var url = "/Admin/" + this.options.controller + "/Unpublish?id=" + id;
            var jqxhr = $.ajax({
                "url": url,
                type: "POST",
                dataType: "json",
                data: { "id": id, "__RequestVerificationToken": $('[name="__RequestVerificationToken"]').val() }
            });
            jqxhr.done(function (data) {
                if (data.success === true) {
                    that._showSuccess(data.msg);
                    that.refreshTable();
                } else {
                    that._showError(data.msg);
                }
            });
            jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                that._showAjaxError(jqXHR, textStatus, errorThrown, url);
            });
        },
        publish: function (id) {
            var that = this;
            var url = "/Admin/" + this.options.controller + "/Publish?id=" + id;
            var jqxhr = $.ajax({
                "url": url,
                type: "POST",
                dataType: "json",
                data: { "id": id, "__RequestVerificationToken": $('[name="__RequestVerificationToken"]').val() }
            });
            jqxhr.done(function (data) {
                if (data.success === true) {
                    that._showSuccess(data.msg);
                    that.refreshTable();
                } else {
                    that._showError(data.msg);
                }
            });
            jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                that._showAjaxError(jqXHR, textStatus, errorThrown, url);
            });
        },

        remove: function (id) {
            var that = this;
            var url = "/Admin/" + this.options.controller + "/Delete?id=" + id;
            bootbox.setDefaults({ locale: "tr" });
            bootbox.confirm("Açıklamayı silmek istediğinizden eminmisiniz?", function (result) {
                if (result) {
                    var jqxhr = $.ajax({
                        "url": url,
                        type: "POST",
                        dataType: "json",
                        data: { "id": id, "__RequestVerificationToken": $('[name="__RequestVerificationToken"]').val() }
                    });
                    jqxhr.done(function (data) {
                        if (data.success === true) {
                            that._showSuccess(data.msg);
                            that.refreshTable();
                        } else {
                            that._showError(data.msg);
                        }
                    });
                    jqxhr.fail(function (jqXHR, textStatus, errorThrown) {
                        that._showAjaxError(jqXHR, textStatus, errorThrown, url);
                    });
                }
            });



        },

        refreshTable: function () {
            this.table.api().ajax.reload(null, false);
        },

        //required for showing errors and success
        _initShowResultDiv: function () {
            //check ShowResultDiv exist
            var showResultDiv = $(this.element).find("#ShowResultDiv");
            if (showResultDiv === undefined || showResultDiv.size() <= 0) {
                //create showResultDiv append to modal body
                $("<div id='ShowResultDiv' style='display:none'></div>").prependTo($(this.element));
            } else {
                //clear showResultDiv content
                this.element.find("#ShowResultDiv").empty();
            }
            //for effect hide ShowResultDiv
            this.element.find("#ShowResultDiv").fadeOut();
        },
        _showSuccess: function (msg) {
            //initialize ShowResultDiv
            this._initShowResultDiv();
            var div = "<div class='alert alert-success'>";
            div += "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
            div += "" + msg;
            div += "</div";
            //alert div append to showResutDiv
            $(this.element.find("#ShowResultDiv")).append(div);
            $(this.element.find("#ShowResultDiv")).fadeIn({ duration: "1000" });
        },
        _showError: function (msg) {
            //initialize ShowResultDiv
            this._initShowResultDiv();
            //create alert div
            var div = "<div class='alert alert-danger alert-dismissable'><i class='fa fa-ban'></i>";
            div += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"
            div += " <b>Error</b>" + msg + "</div>";

            //alert div append to showResutDiv
            $(div).appendTo($(this.element).find("#ShowResultDiv"));
            $(this.element.find("#ShowResultDiv")).fadeIn({ duration: "1000" });
        },
        _showAjaxError: function (jqXHR, textStatus, errorThrown, url) {
            var html = "";
            if (jqXHR.status === 404) {
                html = "<p> Ajax Error:" + url + " url için sayfa bulunamadı";
            } else if (jqXHR.status === 500) {
                html = jqXHR.responseText;
            } else {
                html = '<h1>Ajax Error</h1>' +
               '<p>The Ajax call returned the following error: ' +
               jqXHR.statusText + '.</p>';
            }

            this._showError(html);
        },
    });

}(jQuery));
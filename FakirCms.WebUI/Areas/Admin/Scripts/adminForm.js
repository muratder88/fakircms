﻿/// <reference path="../../../Scripts/jquery-2.1.0-vsdoc.js" />
/// <reference path="../../../Scripts/jquery-ui-1.11.4.js" />


(function ($) {

    $.widget("custom.adminTable", {
        event: {
            task: "",
            ajax: false,
            checkcontrol: false,
            id: ""//It is  like cb0,cb1,cb2,ıt required to select combobox 
        },
        options: {
            controller:"Home",
            action: "Index",
            boxchecked: "false",
            cid: [],
            
        },
        _create:function(){
           
            //Add events
            var self = this

            //sorting column add event
            this._on(this.element, {
                "click th#sorting": function (event) {
                   
                    var item = $(event.currentTarget);
                    // Take value from item and set filter order properties
                    this.sortingColumnClick(item);
                    //Submit form 
                    this.submitTask("index");
                }
            });

            //ItemsPerPage Listbox change event
            this._on(this.element, {
                "change #PagingInfo_ItemsPerPage": function (event) {
                    console.log("ItemsPerPage Listbox clicked");


                    $("input#PagingInfo_ItemsPerPage").val($(event.currentTarget).val());
                    this.submitTask("index");
                }
            });

            //pagination a click event
            this._on(this.element, {
                "click  ul.pagination>li>a": function (event) {
                    console.log("Pagination > a clicked");
                    event.preventDefault();
                    var currentPage = $(event.currentTarget).attr("href");
                    if (currentPage !== undefined) {
                        $(event.currentTarget).has
                        $("input#PagingInfo_CurrentPage").val($(event.currentTarget).attr("href"));
                        this.submitTask("index");
                    }
                   
                    
                   
                }
            });

            //SearchFilter events
            //search icon span click event
            this._on(this.element, {
                "click div.searchFilter span": function (event) {
                    console.log("SearcFilter search span clicked");
                    this.submitTask("index");
                }
            });

            //when Search texbox entered, send data with ajax event
            this._on(this.element, {
                "keypress #Filter_Search": function (event) {
                    console.log("Search textbox entered");
                    //entere basıldığında ajaxı kullanarak veri gönder
                    if (event.keyCode == 13) {
                        event.preventDefault();
                        this.submitTask("index");
                    }
                }
            });

            //When .FilterDropdownlist value change, send data with ajax
            this._on(this.element, {
                "change .filterDropdownList": function (event) {
                    self.submitTask("index");
                }
            });

            //when a.taskEvent clicked, take data-task and data-id attribute
            // change cid hidden input and submit form
            this._on(this.element, {
                "click a.taskEvent": function (event) {
                    console.log("taskEvent clicked");
                    event.preventDefault();
                    var element = $(event.currentTarget);

                    this.event.task = element.attr("data-task");
                    this.event.ajax = (element.attr("data-ajax")=="true");
                    this.event.checkcontrol = (element.attr("data-checkcontrol")=="true");
                    this.event.id = element.attr("data-id");
                    
                    //id is id of combobox, if id is exists this ise listItemTask
                    //And Combobox should  be checked 
                    // this events like only one cid events for example publish(cid) ext.
                    if (this.event.id !== undefined) {
                        this._listItemTask();
                    }
                     
                    this._submitEvent();
                }
            });
            
            //Checkbox All click event
            this._on(this.element, {
                "click input:checkbox[name=cbAll]": function (event) {
                    var cblist = $(this.element).find("input:checkbox[name=cid]");

                    if (event.currentTarget.checked == true) {
                        var self = this;
                        $.each(cblist, function (i, val) {
                            //val is cb2,cb3 checkboxs
                            var numStr=$(val).val();
                            var num = parseInt(numStr);
                            self._insertToCid(num);
                        });
                        $(this.element).find("input:checkbox[name=cid]").prop("checked", true);
                    } else {
                        var self = this;
                        $.each(cblist, function (i, val) {
                            //val is cb2,cb3 checkboxs
                            var numStr = $(val).val();
                            var num = parseInt(numStr);
                            self._removeFromCid(num);
                           
                        });
                        $(this.element).find("input:checkbox[name=cid]").prop("checked", false);
                    }
                }
                
            });

            //When checkbox id cid is clicked,(
            this._on(this.element, {
                "click input:checkbox[name=cid]": function (event) {
                    console.log("cid checkbox clicked");
                    var num = parseInt($(event.currentTarget).val())
                    if (event.currentTarget.checked == true) {
                        //add value options cid array
                       
                        this._insertToCid(num);
                    } else {
                        //remove value from options cid array
                        this. _removeFromCid(num);
                    }
                    console.log(event.currentTarget.checked);
                }
            });

        },



        //Events
        //Event TableColumnHeader Clicked
        //Set Filter Hidden Input And Submit
        sortingColumnClick: function (tableCell) {
           console.log("tableCell:"+ tableCell)
            var column = $(tableCell).attr("data-column");
            var sorting = $(tableCell).attr("data-sorting");
            console.log("sortingColumn Clicked");
            //set Filter hidden input
            $(this.element).find("input#Filter_Order").val(column);
            $(this.element).find("input#Filter_Order_Dir").val(sorting);
           
        },
        //cb combobox in list firstly cb can be checked 
        _listItemTask: function () {
            var cb = this.event.id;
            var checbox = $(this.element).find("input#" + cb);
            //checbox  checked set true
            checbox.attr("checked", true);
            //this value insert array
            var id = parseInt(checbox.val());
            this._insertToCid(id);
            this._submitEvent();
        },

        _submitEvent:function(){
            if (this.event.checkcontrol == true) {
                //Check the any checbox selected
                if (this.options.cid.length > 0) {
                    if (this.event.ajax) {
                        this.submitTask(this.event.task);
                    } else {
                        this.submitForm(this.event.task);
                    }
                } else {
                    this._createAlertDiv("Lütfen checboxlardan birini seçiniz");
                }
            } else {
                if (this.event.ajax)
                    this.submitTask(this.event.task)
                else
                    this.submitForm(this.event.task);
                    
            }
        },

        //submit task
        submitTask:function(task){
            this.options.action = task;
            this._submitFormWithAjax();
        },

        //Submit form
        submitForm: function (task) {
            var url = "/Admin/"+this.options.controller+"/" + task;
            $("#AdminForm").attr("action", url);
            $("#AdminForm").submit();         
        },

        _submitFormWithAjax:function(){
            var self = this;
            this.showLoading();
            var url = "/Admin/" + this.options.controller + "/" + this.options.action;
            $.ajax({
                url: url,
                method: "POST",
                dataType: "html",
                data:$("#AdminForm").serialize()
            }).done(function (msg)
            {
                self.hideLoading();
                $(".AdminTable").html(msg);
            }).fail(function (jqXHR, textStatus) {
                self.error(jqXHR, textStatus);
            });
        },

        //Partial view çalışmass içeriği ajaxla getirmek için
        getContent: function () {
            var self = this;
            this.showLoading();
            $.ajax({
                url: "/Admin/Content/Contents",
                method: "GET",
                dataType:"html"
            }).done(function (msg) {
                this.hideLoading();
                $(".AdminTable").html(msg);
            }).fail(function (jqXHR, textStatus) {
                this.hideLoading();
                this.error(jqXHR, textStatus);
            });
        },

        //ajax sonucunda hata oluşursa onu göstermek için
        error: function (jqXhr, textstatus) {
            var msg="<div class='alert alert-danger alert-dismissable'><i class='fa fa-ban'></i>";
            msg+="<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"
            msg+=" <b>Error</b>"+jqXhr.responseText+"</div>";
            $(this.element).prepend(msg);
        },
        _createAlertDiv:function(msg){
            var div="<div class='alert alert-danger alert-dismissable'><i class='fa fa-ban'></i>";
            div+="<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"
            div+=" <b>Error</b>"+msg+"</div>";
            $(this.element).prepend(div);
        },

        //loading divini ilkini ekler ve visible true
        showLoading:function(){
            $(this.element).find(".box").prepend("<div class='overlay'></div><div class='loading-img'></div>");
        },

        //loading kaldırır
        hideLoading: function () {
            $(this.element).find(".box").remove("div.overlay");
            $(this.element).find(".box").remove("div.loading-img");
        },
        _insertToCid: function (number) {
            if(number===undefined || $.type(number)!== "number")
                throw EventException();
            this.options.cid.push(number);
        },
        _removeFromCid: function (number) {
            if(number===undefined && $.type(number)!== "number")
                throw EventException();
            var found = $.inArray(number, this.options.cid);
            if (found >= 0) {
                this.options.cid.splice(found, 1);
            }
        }

    });
    
}(jQuery));











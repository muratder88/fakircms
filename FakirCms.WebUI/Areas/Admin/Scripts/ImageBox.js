﻿/// <reference path="../../../Scripts/jquery-2.1.0-vsdoc.js" />
/// <reference path="../../../Scripts/jquery-ui-1.11.4.js" />


(function ($) {
    var imageBoxInstances = [];
    $.widget("custom.ImageBox", {

        options:{
            targetId: "ImageUrl",
            imagePreviewId:undefined
        },
        _addInstance:function(imageBoxWidget){
            var object = new Object();
            object.uuid = imageBoxWidget.uuid;
            object.widget = imageBoxWidget;
            imageBoxInstances.push(object);
        },
        _getInstance:function(uuid){
        var instance;
            if ($.type(uuid) !== "number")
                uuid = parseInt(uuid);
            if (imageBoxInstances.length > 0) {
                $.each(imageBoxInstances, function (indx, object) {
                    if (object.uuid == uuid) {
                        instance=object.widget;
                    }
                });
            } else {
                throw Error("ImageBox instances count must be bigger than zero");
            }
            return instance;
        },
        _insertUrl:function(ImageUrl,uuid){
            var that = $.custom.ImageBox.prototype._getInstance(uuid);
            that.element.find("#" + that.options.targetId).val(ImageUrl);
            if (that.options.imagePreviewId !== undefined) {
                var imgContainer = that.element.find("#" + that.options.imagePreviewId);
                //check img element exist
                if ($(imgContainer).find("img").size() > 0) {
                    $(imgContainer).find("img").remove();
                }
                $(imgContainer).append("<img src='" + ImageUrl + "' alt='admin' />");
                        
            }
        },
        _init: function () {
            var that=this;
            this._on(this.element, {
                "click #btnAddImage": function () {
                    that.openBrowserWindow();
                }
            });
        },
        _create: function () {
            //if text box value exist,create img element
            var textBoxValue = this.element.find("#" + this.options.targetId).val();
            if (textBoxValue !== undefined) {
                if (this.options.imagePreviewId !== undefined) {
                    var imgContainer = this.element.find("#" + this.options.imagePreviewId);
                    //check img element exist
                    if ($(imgContainer).find("img").size() > 0) {
                        $(imgContainer).find("img").remove();
                    }
                    $(imgContainer).append("<img src='" + textBoxValue + "' alt='admin' />");

                }
            }
            //add readonly attribute to textbox
            if (!$(this.element.find("#" + this.options.targetId)).is("readonly"))
                $(this.element.find("#" + this.options.targetId)).attr("readonly", "readonly");
        },

        openBrowserWindow: function () {
            this._addInstance(this);
            window.open("/Admin/FileManager/Browser?uuid=" + this.uuid, null, "height=200,width=400,status=yes,toolbar=no,menubar=no,location=no");
        }
       

     

    });

}(jQuery));











﻿/// <reference path="../../../Scripts/jquery-2.1.0-vsdoc.js" />
/// <reference path="../../../Scripts/jquery-ui-1.11.4.js" />


(function ($) {

    $.widget("custom.AdminBox", {
        requestResult:"",
        event: {
            controller: "",
            action:"",
            ajax: false,
            method:"post",
            boxchecked: false,
            id: "",//It is  like cb0,cb1,cb2,ıt required to select combobox 
            showResultInPopup:false
        },
        options: {
            controller: "Home",
            action: "Index",
            modal:false,//if this is modal updateBody modal-body else update box-body
            cid: [],
            data:null,

        },
        _setOption:function(name,value){
            console.log("SetOptions Work");
            $.Widget.prototype._setOption.apply(this, arguments);
        },
        _init: function () {
            var that = this;

            var table = $(this.element).find("table");
            table = $(table);
            table.dataTable(
                {
                columnDefs: [
    { "width": "20%", "targets": 0 }
  ],
                    responsive: true
                });
            // when filter-input value change,send form with ajax
            this._on(this.element, {
                "change .filter-input": function () {
                    //Send Ajax 
                    that.event.controller = that.options.controller;
                    that.event.action = that.options.action;
                    that.event.ajax = true;
                    that.event.showResultInPopup = false;
                    that._submitEvent(that.event);
                }
            });

            //.taskEvent button clicked
            this._on(this.element, {
                "click .taskEvent": function (e) {
                    var event = that.event;

                    event = this._getEventFromHtmlElement(e.currentTarget);

                    if (event.boxchecked) {
                        //checked any checbox item selected,if not warn user
                        var boxes = $('input[name=cid]:checked');
                        if ($(boxes).size() <= 0) {
                            alert("Lütfen birini seçin");
                        } else {
                            that._submitEvent(event);
                        }
                    } else {
                        //send data
                        that._submitEvent(event);
                    }
                }
            });

            
            this._on(this.element, {
                "click .listItemTask": function (e) {
                    var event = that.event;

                    event = this._getEventFromHtmlElement(e.currentTarget);

                    //all checkbox set uncheck
                    $(this.element).find("input:checkbox[name=cid]").prop("checked", false);

                    //get checkbox id name
                    var checboxId=$(e.currentTarget).data("cbid");
                    if (!that._isCheckBoxExistAndUnique(checboxId)) {
                        alert("listItemTask data-cbid yok veya birden fazla aynı checkbox aynı id'ye sahip");
                        throw Error("listItemTask data-cbid yok veya birden fazla aynı checkbox aynı id'ye sahip");
                    }
                    //checked given checkbox
                    $(this.element).find("#" + checboxId).prop("checked", true);
                    that._submitEvent(event);
                }
            });

            //pagination a click set value of input#PagingInfo_CurrentPage
            this._on(this.element, {
                "click  ul.pagination>li>a": function (event) {
                    console.log("Pagination > a clicked");
                    event.preventDefault();
                    var currentPage = $(event.currentTarget).attr("href");
                    if (currentPage !== undefined) {
                        var page = $(event.currentTarget).attr("href");
                        try{
                            page=parseInt(page);
                        }catch(err){
                            throw err;
                        }
                        var element=$(that.element).find("input#PagingInfo_CurrentPage")
                        $(element).val(page);
                        //send ajax
                        that.event.controller = that.options.controller;
                        that.event.action = that.options.action;
                        that.event.ajax = true;
                        that.event.showResultInPopup = false;
                        that._submitEvent(that.event);
                    }



                }
            });

            //sorting column add event
           /* this._on(this.element, {
                "click th#sorting": function (event) {
                    var tablecell = $(event.currentTarget);
                    // Take value from item and set filter order properties
                    this._setOrderFilterInputs(tablecell);
                    
                    //Submit form 
                    that.event.controller = that.options.controller;
                    that.event.action = that.options.action;
                    that.event.ajax = true;
                    that.event.showResultInPopup = false;
                    that.event.method = "post";
                    that._submitEvent(that.event);
                }
            });*/
        },
        //Get Event object from element data attirubites.Check event attirubites correct and
        //Required attirubites exist
        //
        //<param name="currentTarget" type="HtmlElement">Element has event data attirubites</param>
        // <returns type="Event">This event objects</returns>
        _getEventFromHtmlElement:function(currentTarget){
           var event=this.event;
            event.action=$(currentTarget).data("task");
            event.boxchecked=$(currentTarget).data("boxchecked");
            event.method=$(currentTarget).data("method");
            event.showResultInPopup=$(currentTarget).data("showresultinpopup");
            event.controller=$(currentTarget).data("controller");
            event.ajax=$(currentTarget).data("ajax");

            //check types
            if ($.type(event.boxchecked) !== "boolean") { event.boxchecked = undefined };
            if ($.type(event.ajax) !== "boolean") { event.ajax = undefined };
            if ($.type(event.showResultInPopup) !== "boolean") { event.showResultInPopup = undefined };
            
            //set default values
            if (event.method === undefined) { event.method = "post" };
            if (event.controller === undefined) { event.controller = this.options.controller };
            if (event.boxchecked == undefined) { event.boxchecked = false };
            if (event.showResultInPopup==undefined) { event.showResultInPopup = undefined };
            
            //Check the required parameters exist
            if (event.action === undefined || event.ajax === undefined || event.boxchecked === undefined || event.showResultInPopup === undefined
                || event.method===undefined || event.controller===undefined) {
                alert("event parametreleri düzgün değil");
                throw Error(".taskEvent click event parametreleri düzgün değil");
            }

            return event;
        },
        _create: function () {
            console.log("Craete Works");
            console.log(this.options.controller);
            console.log(this.options.action);
            console.log(this.options.boxchecked)
        },

        //Check the checkbox is exist and unique or return undefined
        _isCheckBoxExistAndUnique: function (elementId) {
            if (elementId === undefined || $.type(elementId) !== "string")
                alert("AdminBox.isCheckBoxExistAndUnique " + elementId + " undefined yada tipi string değil");
           
            var element = $(this.element).find("#" + elementId);
            if (element === undefined)
                return false;
            if ($(element).size() >1)
                return false;
            return true;
        },
        //string is true return true,false return false otherwise return undefined
        convertStringToBoolean:function(string){
            if (string === undefined)
                return undefined;
            else if (string === "true")
                return true;
            else if (string == "false")
                return false;
            else
                return undefined;
        },
        _submitEvent: function (eventObject) {
            var that = this;
            if(eventObject.ajax){
                if(eventObject.showResultInPopup){
                    var promise = this._submitFormWithAjax(eventObject);
                    promise.done(function () {
                        that._showRequestResultInPopup();
                    });
                }else{
                    var promise = this._submitFormWithAjax(eventObject);
                    promise.done(function () {
                        that._updateBoxBody();
                    });
                }
            }else{
                //ajax yoksa formu gönder
                this._submitForm(eventObject);
            }
        },
        _updateBoxBody: function () {
            if(!this.options.modal)
                var boxbody = $(this.element).find(".box-body");
            else
                var boxbody = $(this.element).find(".modal-body")
            $(boxbody).html(this.requestResult);
        },
        _submitFormWithAjax: function (event) {
        //Burada mutlaka form elementi varmı yada birden fazla olduğu kontrol edilip uyarı verilmeli
            var that = this;
            var dfd = $.Deferred();
            var url = "/Admin/"+event.controller + "/" + event.action;
            that.showLoading();
            $.ajax({
                url: url,
                method: event.method,
                dataType: "html",
                data: $(that.element.find("form")).serialize()
            }).done(function (msg) {
                that.requestResult = msg;
                that.hideLoading();
                dfd.resolve();
            }).fail( function (jqXHR, textStatus, errorThrown) {
                //Hata bildirimi yapılacak
                that._showAjaxError(jqXHR, textStatus, errorThrown, url);
                that.hideLoading();
                dfd.reject();
            });
            return dfd.promise();
        },

        _submitForm:function(event)
        {
            if (event === undefined)
                throw Error("Event parameter is not be undefined");

            var form = $(this.element).find("form")
            if (form !== undefined && $(form).size() > 1) {
                alert("Admin Box birden fazla form barındırıyor.Genel islemler icin ilk form gönderiliyor");
                form = $(form)[0];
            }

            var url="/Admin/"+event.controller+"/"+event.action;
            $(form).attr("action", url);
            $(form).submit();
        },
        _showRequestResultInPopup:function(){
            //Create Popup content div and append to this element
            var ModalContentDiv = $("<div></div>").attr("id", "ModalContentDiv");
            if ($("body").find("#ModalContentDiv").size() > 0) {
                $("#ModalContentDiv").remove();
            }
            $(ModalContentDiv).html(this.requestResult);
            $("body").append(ModalContentDiv);
            $("#ModalContentDiv .modal").modal();
        },
        _showAjaxError: function (jqXHR, textStatus, errorThrown, url) {
            var html = "";
            if (jqXHR.status === 404) {
                html = "<p> Ajax Error:" + url + " url için sayfa bulunamadı";
            } else {
                html = '<h1>Ajax Error</h1>' +
               '<p>The Ajax call returned the following error: ' +
               jqXHR.statusText + '.</p>';
            }
            
            this.createAlertDiv(html);
        },
        createAlertDiv: function (msg) {
            var div = "<div class='alert alert-danger alert-dismissable'><i class='fa fa-ban'></i>";
            div += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"
            div += " <b>Error</b>" + msg + "</div>";
            if (!this.options.modal)
                var boxbody = $(this.element).find(".box-body");
            else
                var boxbody = $(this.element).find(".modal-body");
            $(boxbody).prepend(div);
        },
        //Event TableColumnHeader Clicked
        //Set Filter Hidden Input 
        _setOrderFilterInputs: function (tableCell) {
           console.log("tableCell:"+ tableCell)
            var column = $(tableCell).attr("data-column");
            var sorting = $(tableCell).attr("data-sorting");
            console.log("sortingColumn Clicked");
            //set Filter hidden input
            $(this.element).find("input#Filter_Order").val(column);
            $(this.element).find("input#Filter_Order_Dir").val(sorting);
           
        },
        //loading divini ilkini ekler ve visible true
        showLoading: function () {
            $(this.element).prepend("<div class='overlay'></div><div class='loading-img'></div>");
        },

        //loading kaldırır
        hideLoading: function () {
            var divOverlay = $(this.element).find("div.overlay");
            var divLoading = $(this.element).find("div.loading-img");
            $(divOverlay).remove();
            $(divLoading).remove();
        },
    });

}(jQuery));











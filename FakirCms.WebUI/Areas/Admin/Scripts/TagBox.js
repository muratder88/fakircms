﻿/// <reference path="../../../Scripts/jquery-2.1.0-vsdoc.js" />
/// <reference path="../../../Scripts/jquery-ui-1.11.4.js" />


(function ($) {

    $.widget("custom.TagBox", {
        table: "",
        tableInitComplete: $.deffered,
        dialogTitle: "Etiketler",
        dialogMessage: "Custom Html Content",
        wrapper: $("<div id='bootboxContainer'></div>"),

        _init: function () {
            var that = this;
            this._tableInit();
            this._on(this.element, {
                "click a.listItemTask": function (event) {
                    console.log("listItemTask clicked");
                    var element = event.currentTarget;
                    console.log($(element));
                    console.log($(element).data("task"));
                }
            });

            this._on(this.element, {
                "click #btnAddNewRow": function (event) {
                    var tr = "";
                    tr += "<tr role='row' class='odd'>";
                    tr += "<td><a class='btn btn-sm btn-default listItemEdit' data-task='edit' href='#'>";
                    tr += "<span class='glyphicon glyphicon-pencil'></span></a></td>";
                    tr += "<td class=''>";
                    tr+="<span id='tagName' data-type='text' data-pk='0' data-name='Name' data-url='/Admin/Tag/' data-original-title='Etiket adını girin'>Yeni Etiket</p>";
                    tr+="</td>";
                    tr += "<td> <a class='btn btn-sm btn-default listItemDelete' data-id='0' href='#'>"
                    tr += "<span class='glyphicon glyphicon-trash'></span> </a></td></tr>";
                    var body = $(this.table).find("tbody");
                    $(tr).prependTo(body);
                    console.log("çalıştı");
                }
            });

            this._on(this.element, {
                "click a.listItemEdit": function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                    var element = event.currentTarget;
                    var tr = $(element).parent("tr").first();
                    var editableElement = $(element).parents("tr").first().find("#tagName")
                    $(editableElement).editable({
                        ajaxOptions: { url: "/Admin/Tag/Tag", type: "POST", dataType: 'json' },
                        params: function(params) {
                            params["__RequestVerificationToken"] = $('[name="__RequestVerificationToken"]').val();
                            return params;
                        },
                        success: function (response, newValue) {
                            if (response.status == 'error') that._showError(response.msg); //msg will be shown in editable form
                            if ($.type(response) === "object") {
                                if (response.success == true) {
                                    that._showSuccess(response.msg);
                                    that.table.api().ajax.reload(null, false);
                                } else {
                                    that._showError(response.msg);
                                }
                            } else {
                                that._showError(response.msg);
                            }
                        },
                        error: function (response) {
                            that._showError(response.responseText);
                        }
                    });
                    $(editableElement).editable("toggle");

                }
            });

            this._on(this.element, {
                "click a.listItemDelete": function (event) {
                    event.stopPropagation();
                    event.preventDefault();
                    var element = event.currentTarget;
                    var id = $(element).data("id");
                    var rowindex = $(element).data("rowindex");
                    if (id === undefined) {
                        alert("Item data-id value is undefined");
                    } else {
                        id = parseInt(id);
                        if ($.type(id) !== "number") {
                            alert("Item data-id value must be integer");
                        } else if (id === 0) {
                            //delete added row
                            var tr = $(element).parents("tr").first();
                            $(tr).remove();
                        } else {
                            bootbox.setDefaults({ locale: "tr" });
                            bootbox.confirm("Etiketi silmek istediğinizden eminmisiniz?", function (result) {
                                if (result === true) {
                                    var url = "/Admin/Tag/Delete";
                                    var jxhr = $.ajax({
                                        "url": url,
                                        type: "POST",
                                        dataType: "json",
                                        data: { "id": id, "__RequestVerificationToken": $('[name="__RequestVerificationToken"]').val() }
                                    });
                                    jxhr.done(function (data) {
                                        if (data.success === true) {
                                            that._showSuccess(data.msg);
                                            //remove row
                                            that.table.fnDeleteRow(rowindex);
                                        } else {
                                            that._showError(data.msg);
                                        }
                                    });
                                    jxhr.fail(function (jqXHR, textStatus, errorThrown) {
                                        that._showAjaxError(jqXHR, textStatus, errorThrown, url);
                                    });
                                }
                            });

                        }
                    }
                }
            });


        },
        _create: function () {
            //turn to inline mod
            $.fn.editable.defaults.mode = "inline";
            this._createModal();
        },
        _tableInit: function () {
            var that = this;
            var table = $(this.element).find("#adminTable");
            this.table = $(table).dataTable({
                "responsive": true,
                "proccessing": true,
                "autoWidth": false,
                "ajax": { url: "/Admin/Tag/GetTags", type: "GET" },
                "order": [1, 'asc'],
                "initComplete": function (settings, json) {
                    //show modal
                    var element = $(that.element).find(".bs-example-modal-lg");
                    var modal = $(element).modal();
                    $(modal).on('hide.bs.modal', function () {
                        that._destroyTableAndHtml();
                    });
                },
                "columns": [
                    {
                        data: null, title: "", searchable: false, orderable: false, render: function (data, type, row, meta) {
                            var html = "";
                            html += "<a class='btn btn-sm btn-default listItemEdit' data-task='edit' href='#' >";
                            html += " <span class='glyphicon glyphicon-pencil'></span></a>";
                            return html;
                        }
                    },
                    {
                        data: "Name", title: "Etiket Adı", type: "html", render: function (data, type, row, meta) {
                            var html = "";
                            html += "<span id='tagName' data-type='text' data-pk='" + row.Id + "' data-name='Name' data-url='/Admin/Tag/' data-original-title='Etiket adını girin'>";
                            html += data;
                            html += "</span>";
                            return html;
                        }
                    },
                    {
                        data: null, title: "", searchable: false, orderable: false, render: function (data, type, row, meta) {
                            var html = "";
                            html += " <a class='btn btn-sm btn-default listItemDelete' data-id='"+row.Id+"' data-rowindex='"+meta.row+"' href='#'>";
                            html += "<span class='glyphicon glyphicon-trash'></span>";
                            html += " </a>";
                            return html;
                        }
                    }
                ]
            });
        },
        _showAjaxError: function (jqXHR, textStatus, errorThrown, url) {
            var html = "";
            if (jqXHR.status === 404) {
                html = "<p> Ajax Error:" + url + " url için sayfa bulunamadı";
            } else {
                html = '<h1>Ajax Error</h1>' +
               '<p>The Ajax call returned the following error: ' +
               jqXHR.statusText + '.</p>';
            }

            this._showError(html);
        },
        _destroyTableAndHtml: function () {
            console.log("Modal Kapanıyor işlemleri yapabilirsin destroy table delete container html");
            $(this.element).empty();
            $.Widget.prototype.destroy.call(this);
        },
        //required for showing errors and success
        _initShowResultDiv: function () {
            //check ShowResultDiv exist
            var showResultDiv = $(this.element).find("#ShowResultDiv");
            if (showResultDiv === undefined || showResultDiv.size() <= 0) {
                //create showResultDiv append to modal body
                $("<div id='ShowResultDiv' style='display:none'></div>").prependTo($(this.element).find(".modal-body"));
            } else {
                //clear showResultDiv content
                this.element.find("#ShowResultDiv").empty();
            }
            //for effect hide ShowResultDiv
            this.element.find("#ShowResultDiv").fadeOut();
        },
        _showSuccess: function (msg) {
            //initialize ShowResultDiv
            this._initShowResultDiv();
            var div = "<div class='alert alert-success'>";
            div += "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>";
            div +=""+msg;
            div += "</div";
            //alert div append to showResutDiv
            $(this.element.find("#ShowResultDiv")).append(div);
            $(this.element.find("#ShowResultDiv")).fadeIn({ duration: "1000" });
        },
        _showError: function (msg) {
            //initialize ShowResultDiv
            this._initShowResultDiv();
            //create alert div
            var div = "<div class='alert alert-danger alert-dismissable'><i class='fa fa-ban'></i>";
            div += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>"
            div += " <b>Error</b>" + msg + "</div>";

            //alert div append to showResutDiv
            $(div).appendTo($(this.element).find("#ShowResultDiv"));
            $(this.element.find("#ShowResultDiv")).fadeIn({ duration: "1000" });
        },
        _createModal: function () {
            var html = "";
            html += " <div class='modal fade bs-example-modal-lg' tabindex='-1' role='dialog' aria-labelledby='myLargeModalLabel'>";
            html += "<div class='modal-dialog modal-lg'>";
            html += "<div class='modal-content'>";
            html += "<div class='modal-header'>";//Modal header start
            html += "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
            html += "<h4 class='modal-title' id='myModalLabel'>Etiketler</h4>";
            html += "</div>";//Modal header end
            html += "<div class='modal-body'>";//Modal body start
            html += "<table id='adminTable' class='table table-striped table-bordered dataTable'></table>";
            html += "</div>"//Modal body end
            html += "<div class='modal-footer'>";//Modal footer start
            html += "<button type='button' class='btn btn-danger' data-dismiss='modal'>Kapat</button>";
            html += "<button type='button' id='btnAddNewRow' class='btn btn-success'>Yeni Etiket</button>";
            html += "</div>";//Modal footer end
            html += "</div>";//Modal content end
            html += "</div>";//Modal dialog end
            html + "</div>";//Modal end
            $(this.element).append(html);
        }
    });

}(jQuery));











﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Areas.Admin.Models
{
    public class CreateHaberModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Haber başlığını giriniz")]
        [Display(Name = "Haber Başlığı")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Takma ismi giriniz")]
        [Display(Name = "Takma İsim")]
        public string Alias { get; set; }
        [Required(ErrorMessage = "Giriş metnini giriniz")]
        [Display(Name = "Giriş Metni")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string IntroText { get; set; }
        [Required(ErrorMessage = "Haber Metnini Giriniz")]
        [Display(Name = "Haber Metni")]
        [DataType(DataType.Html)]
        [AllowHtml]
        public string FullText { get; set; }
        [Required(ErrorMessage="Resim Seçin")]
        [DataType(DataType.ImageUrl)]
        [Display(Name="Resim Seç:")]
        public string ImageUrl { get; set; }
        [Display(Name = "Kategori")]
        [Required(ErrorMessage="Haber Kategorisi Seçin")]
        public int Category { get; set; }
        [Display(Name="Şehir:")]
        public string City { get; set; }
        [Display(Name="İlçe")]
        public string Town { get; set; }
        [Required(ErrorMessage = "Haber Tarihi")]
        [Display(Name = "Haber Tarihi")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Haber Durumunu Seçiniz")]
        [Display(Name = "Aktif")]
        public bool IsActive { get; set; }
        [DataType(DataType.DateTime)]
        [Display(Name = "Oluşturulma Tarihi")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }
        [Display(Name = "Yazar")]
        public string CreatedByName { get; set; }
        [Display(Name = "Düzenleyen")]
        public string UpdatedByName { get; set; }
        [Display(Name = "Düzenleme Tarihi")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }
        [Display(Name = "Haber Etiketleri")]
        public List<int> Tags { get; set; }
        [Display(Name = "İzlenim")]
        public int Hits { get; set; }
        [Display(Name = "Anasayfa Manşet")]
        public bool ShowManset { get; set; }
        [Display(Name = "Kategori Manşet")]
        public bool ShowCategoryManset { get; set; }
        [Display(Name = "Spot Başlık")]
        [MaxLength(70,ErrorMessage="Spot Başlığı Maksimum 70 Karakter uzunluğunda olabilir")]
        public string SpotBigTittle { get; set; }
        [Display(Name = "Spot Alt Başlık")]
        [MaxLength(90, ErrorMessage = "Spot Alt Başlığı Maksimum 90 Karakter uzunluğunda olabilir")]
        public string SpotSmallTitle { get; set; }
        [Display(Name = "Ana Safa Manşet Altı:")]
        public bool ShowFrontPageMain { get; set; }
        [Display(Name = "Ana Sayfa Alt Kısım:")]
        public bool ShowFrontPageAlt { get; set; }
         [Display(Name = "Ana Sayfa Üst Kısım:")]
        public bool ShowFrontPageUst { get; set; }

        //Seo
        [Display(Name = "Meta Başlık Etiketi")]
        [MaxLength(70, ErrorMessage = "Meta Başlık Etiketi maksimum 70 karakter uzunluğunda olmalı")]
        public string MetaTitle { get; set; }
        [Display(Name = "Meta Tanıtım Etiketi")]
        [MaxLength(160, ErrorMessage = "Meta Tanıtım Etiketi maksimum 160 karakter uzunluğunda olmalı")]
        public string MetaDescription { get; set; }
        [Display(Name = "Meta Anahtar Kelimeleri Giriniz")]
        public string MetaKeywords { get; set; }

        //SelectList
        public List<SelectListItem> CategorySelecList { get; set; }
        
    }

    public class CreateHaberCategoryModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Kategori başlığını giriniz")]
        [Display(Name = "Kategori Başlığı")]
        [MaxLength(70,ErrorMessage="Kategori başlığı en fazla 70 karekter olabilir")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Takma ismi giriniz")]
        [Display(Name = "Takma İsim")]
        [MaxLength(70, ErrorMessage = "Takma isim  en fazla 70 karekter olabilir")]
        public string Alias { get; set; }
        [Required(ErrorMessage = "Tanıtım metni giriniz")]
        [Display(Name = "Tanıtım")]
        [MaxLength(160, ErrorMessage = "Tanıtım en fazla 160 karakter olabilir")]
        public string Description { get; set; }
        [Display(Name = "Meta Etitketi Giriniz")]
        public string Tags { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }
      

        [Display(Name = "Yazar")]
        public string CreatedByName { get; set; }
        [Display(Name = "Düzenleyen")]
        public string UpdatedByName { get; set; }

    }
}
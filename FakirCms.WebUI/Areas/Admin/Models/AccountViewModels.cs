﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FakirCms.WebUI.Areas.Admin.Models
{
    public class LoginModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [MinLength(6,ErrorMessage="Şifre en az 6 karakter olmalı")]
        [StringLength(13,ErrorMessage="Şifre en fazla 13 karakter olablir")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
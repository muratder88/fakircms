﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Areas.Admin.Models
{
    public class CreateAuthorModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Yazar adını giriniz")]
        [Display(Name = "Yazar Adı")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Takma ismi giriniz")]
        [Display(Name = "Takma İsim")]
        public string Alias { get; set; }
        [Required(ErrorMessage = "Tanıtım Metnini giriniz")]
        [Display(Name = "Tanıtım Metni")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [Required(ErrorMessage = "Resim Seçin")]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Resim Seç:")]
        public string Image { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage="Email adresini giriniz")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Geçersiz email adresi")]
        public string Email { get; set; }
        
        //User updated created informations
        [DataType(DataType.DateTime)]
        [Display(Name = "Oluşturulma Tarihi")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }
        [Display(Name = "Yazar")]
        public string CreatedByName { get; set; }
        [Display(Name = "Düzenleyen")]
        public string UpdatedByName { get; set; }
        [Display(Name = "Düzenleme Tarihi")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }

        //Seo
        [Display(Name = "Meta Başlık Etiketi")]
        [MaxLength(70, ErrorMessage = "Meta Başlık Etiketi maksimum 70 karakter uzunluğunda olmalı")]
        public string MetaTitle { get; set; }
        [Display(Name = "Meta Tanıtım Etiketi")]
        [MaxLength(160, ErrorMessage = "Meta Tanıtım Etiketi maksimum 160 karakter uzunluğunda olmalı")]
        public string MetaDescription { get; set; }
        [Display(Name = "Meta Anahtar Kelimeleri Giriniz")]
        public string MetaKeywords { get; set; }
    }
}
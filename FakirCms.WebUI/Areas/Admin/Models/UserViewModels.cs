﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FakirCms.WebUI.Areas.Admin.Models
{
    public class UserCreateModel
    {
        public string Id { get; set; }
        [Required]
        [Display(Name="Kullanıcı Adı:")]
        public string UserName { get; set; }
        [Required(ErrorMessage="Lütfen adınızı giriniz")]
        [Display(Name="Ad:")]
        public string Name { get; set; }
        [Required(ErrorMessage="Şifreyi giriniz")]
        [DataType(DataType.Password)]
        [StringLength(14,ErrorMessage="{0} must be at least {2} characters log.",MinimumLength=6)]
        [Display(Name="Şifre")]
        public string Password { get; set; }
        [Required(ErrorMessage="Şifre tekrarını giriniz")]
        [DataType(DataType.Password)]
        [Display(Name="Şifre Tekrarı:")]
        [Compare("Password", ErrorMessage = "Şifre ve şifre tekrarı birbiriyle eşleşmiyor.")]
        public string PasswordAgain { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name="Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name="Aktifleştir")]
        public bool Active { get; set; }
        public List<string> Roles { get; set; }
    }
    public class UserPasswordResetModel
    {
        [Required]
        public string Id { get; set; }
        [Required(ErrorMessage="Şifreyi giriniz")]
        [DataType(DataType.Password)]
        [StringLength(14, ErrorMessage = "{0} must be at least {2} characters log.", MinimumLength = 6)]
        [Display(Name = "Şifre")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Şifre tekrarını giriniz")]
        [DataType(DataType.Password)]
        [Display(Name = "Şifre Tekrarı:")]
        [Compare("Password", ErrorMessage = "Şifre ve şifre tekrarı birbiriyle eşleşmiyor.")]
        public string PasswordAgain { get; set; }
    }
    public class UserEditModel
    {
        public string Id { get; set; }
        [Required(ErrorMessage="Adı giriniz")]
        [Display(Name="Ad:")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Kullanıcı adını giriniz")]
        [Display(Name = "Kullanıcı Adı:")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Maili giriniz")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email:")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Aktifleştir:")]
        public bool Active { get; set; }
        public List<string> Roles { get; set; }
    }
}
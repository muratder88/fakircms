﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Areas.Admin.Models
{
    public class CreateArticleModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Yazı başlığını giriniz")]
        [Display(Name = "Yazı Başlığı")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Takma ismi giriniz")]
        [Display(Name = "Takma İsim")]
        public string Alias { get; set; }
        [Required(ErrorMessage = "Giriş metnini giriniz")]
        [Display(Name = "Yazı Metni")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Content { get; set; }
        [Required(ErrorMessage = "Tarihi giriniz")]
        [Display(Name = "Tarih")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [DataType(DataType.DateTime)]
        [Display(Name = "Oluşturulma Tarihi")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }
        [Display(Name = "Oluşturan")]
        public string CreatedByName { get; set; }
        [Display(Name = "Düzenleyen")]
        public string UpdatedByName { get; set; }
        [Display(Name = "Düzenleme Tarihi")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }
        [Display(Name = "Haber Etiketleri")]
        public List<int> Tags { get; set; }
        [Display(Name = "İzlenim")]
        public int Hits { get; set; }
        [Display(Name = "Yayın")]
        public bool Publish { get; set; }
        [Display(Name="Yazar")]
        [Required(ErrorMessage="Yazar seçin")]
        public int AuthorID { get; set; }

        //Seo
        [Display(Name = "Meta Başlık Etiketi")]
        [MaxLength(70, ErrorMessage = "Meta Başlık Etiketi maksimum 70 karakter uzunluğunda olmalı")]
        public string MetaTitle { get; set; }
        [Display(Name = "Meta Tanıtım Etiketi")]
        [MaxLength(160, ErrorMessage = "Meta Tanıtım Etiketi maksimum 160 karakter uzunluğunda olmalı")]
        public string MetaDescription { get; set; }
        [Display(Name = "Meta Anahtar Kelimeleri Giriniz")]
        public string MetaKeywords { get; set; }

        //SelectList
        public List<SelectListItem> AuthorSelecList { get; set; }
    }
}
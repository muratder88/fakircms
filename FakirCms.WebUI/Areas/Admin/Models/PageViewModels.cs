﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Areas.Admin.Models
{
    public class PageCreateModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Sayfa başlığını giriniz")]
        [Display(Name = "Sayfa Başlığı")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Takma ismi giriniz")]
        [Display(Name = "Takma İsim")]
        public string Alias { get; set; }
        [AllowHtml]
        [Required(ErrorMessage = "Sayfa İçeriğini Giriniz")]
        [Display(Name = "Sayfa İçerik")]
        [DataType(DataType.Html)]
        public string Content { get; set; }
        [Required(ErrorMessage = "Sayfa Durumunu Seçiniz")]
        [Display(Name = "Aktif")]
        public bool IsActive { get; set; }
        [DataType(DataType.DateTime)]
        [Display(Name = "Oluşturulma Tarihi")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }
        [Display(Name = "Yazar")]
        public string CreatedByName { get; set; }
        [Display(Name = "Düzenleyen")]
        public string UpdatedByName { get; set; }
        [Display(Name = "Düzenleme Tarihi")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Updated { get; set; }
        [Display(Name = "İzlenim")]
        public int Hits { get; set; }
        public string CustomCss { get; set; }
        public string CustomJs { get; set; }

        //Seo
        [Required(ErrorMessage = "Meta Başlık Etiketini Giriniz")]
        [Display(Name="Meta Başlık Etiketi")]
        [MaxLength(70,ErrorMessage="Meta Başlık Etiketi maksimum 70 karakter uzunluğunda olmalı")]
        public string MetaTitle { get; set; }
        [Required(ErrorMessage = "Meta Tanıtım Etiketini Giriniz")]
        [Display(Name = "Meta Tanıtım Etiketi")]
        [MaxLength(160, ErrorMessage = "Meta Tanıtım Etiketi maksimum 160 karakter uzunluğunda olmalı")]
        public string MetaDescription { get; set; }
        [Required(ErrorMessage="Meta Anahtar Kelimelerini giriniz")]
        [Display(Name="Meta Anahtar Kelimeleri Giriniz")]
        public string MetaKeywords { get; set; }

    }
}
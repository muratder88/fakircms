(function (jQuery) {
    "use strict";
    var check = true;
    jQuery.fn.ccTheme = function (options) {
        return this.each(function () {
            init(jQuery(this));
        });
        function flexslider() {
            $(window).load(function () {
                $('.flexslider').flexslider({
                    animation: "slide",
                    start: function (slider) {
                        $('body').removeClass('loading');
                    }
                });
            });
        }
        function progress() {
            jQuery(document).ready(function () {

                function mixProgress(container) {
                    return container.each(function () {
                        var wrap = jQuery(this);

                        wrap.find('.progress').each(function (i) {
                            var element = jQuery(this);
                            //console.debug(element);
                            var w_att = element.find('.bar', wrap).attr('data-progress');
                            element.find('.bar', wrap).css('width', w_att + '%');

                            setTimeout(function () {
                                element.addClass('start_animation animation' + i);

                                jQuery('.animation' + i).find('.bar', wrap).countTo();
                            }, (i * 250));

                        });
                    });
                }

                var container = jQuery('.prog-wrap');

                mixProgress(container);
            });
            $('.timer').each(count);
            function count(options) {
                var $this = $(this);
                options = $.extend({}, options || {}, $this.data('countToOptions') || {});
                $this.countTo(options);
            }
        }

        function accordion() {
            jQuery("ul.accordion li").each(function () {
                if (jQuery(this).index() > 0) {
                    jQuery(this).children(".accordion-content").css('display', 'none');
                } else {
                    jQuery(this).find(".accordion-title").addClass('active');
                }
                jQuery(this).children(".accordion-title").bind("click", function () {
                    jQuery(this).addClass(function () {
                        if (jQuery(this).hasClass("active")) return "";
                        return "active";
                    });
                    jQuery(this).siblings(".accordion-content").slideDown();
                    jQuery(this).parent().siblings("li").children(".accordion-content").slideUp();
                    jQuery(this).parent().siblings("li").find(".active").removeClass("active");
                });
            });
        }

        function tabs() {
            jQuery("#tabbed-widget .tabs-wrap").hide();
            jQuery("#tabbed-widget ul.posts-taps li:first").addClass("active").show();
            jQuery("#tabbed-widget .tabs-wrap:first").show();
            jQuery("#tabbed-widget  li.tabs").click(function () {
                jQuery("#tabbed-widget ul.posts-taps li").removeClass("active"); jQuery(this).addClass("active");
                jQuery("#tabbed-widget .tabs-wrap").hide(); var activeTab = jQuery(this).find("a").attr("href");
                jQuery(activeTab).slideDown(); return false;
            });
        }
        //animated scroll loading effects		
        function animated() {
            jQuery(document).ready(function () {
                jQuery('.review').addClass("hidden").viewportChecker({
                    classToAdd: 'visible animated fadeIn',
                    offset: 100,
                    callbackFunction: function (elem) {
                        progress();
                    }
                });
            });
        }
        //swiper slider
        function swiperSlider() {
            jQuery(document).ready(function () {
                var container = $("#primary .swiper-container");
                if (container !== undefined) {
                    var swiper = new Swiper('#primary .swiper-container', {
                        pagination: '.swiper-pagination',
                        paginationClickable: true,
                        speed: 600,
                        paginationBulletRender: function (index, className) {
                            return '<span class="' + className + '">' + (index + 1) + '</span>';
                        }
                    });
                }

                var container = $("#ust-haber .slide-container");
                if (container !== undefined) {
                    var swiper = new Swiper('#ust-haber .swiper-container', {
                        scrollbar: '.swiper-scrollbar',
                        scrollbarHide: true,
                        slidesPerView: 4,
                        nextButton: '.swiper-button-next',
                        prevButton: '.swiper-button-prev',
                        spaceBetween: 30,
                        mousewheelControl: true
                        
                    });
                }

            });
        }
        //slim menu
        function slimMenu() {
            jQuery(document).ready(function () {
               jQuery('ul.sf-menu').superfish();
            });
        }
        //Slicknav Menu
        function slickNav() {
            jQuery(document).ready(function () {
                jQuery('ul.sf-menu').slicknav({
                    prependTo: '.navigationdiv'
                });
            });
        }

        //popup		
        function popup() {
            jQuery(document).ready(function () {
                jQuery('.popup-gallery').magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    tLoading: 'Loading image #%curr%...',
                    mainClass: 'mfp-img-mobile',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                    },
                    image: {
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                        titleSrc: function (item) {
                            return item.el.attr('title') + '<small></small>';
                        }
                    }
                });
            });

            jQuery(document).ready(function () {
                jQuery('.popup-photo').magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    tLoading: 'Loading image #%curr%...',
                    mainClass: 'mfp-img-mobile',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                    },
                    image: {
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                        titleSrc: function (item) {
                            return item.el.attr('title') + '<small></small>';
                        }
                    }
                });
            });
        }


        /* ==========================================================================
           exists - Check if an element exists
           ========================================================================== */

        function exists(e) {
            return $(e).length > 0;
        }


        /* ==========================================================================
           handleMobileMenu 
           ========================================================================== */

        function handleMobileMenu() {

            if ($(window).width() > 600) {

                $("#mobile-menu").hide();
                $("#mobile-menu-trigger").removeClass("mobile-menu-opened").addClass("mobile-menu-closed");

            } else {

                if (!exists("#mobile-menu")) {

                    $("#menu").clone().attr({
                        id: "mobile-menu",
                        "class": "fixed container"
                    }).insertAfter("#header");

                    $("#mobile-menu > li > a, #mobile-menu > li > ul > li > a").each(function () {
                        var $t = $(this);
                        if ($t.next().hasClass('sub-menu') || $t.next().is('ul') || $t.next().is('.sf-mega')) {
                            $t.append('<span class="fa fa-angle-down mobile-menu-submenu-arrow mobile-menu-submenu-closed"></span>');
                        }
                    });

                    $(".mobile-menu-submenu-arrow").click(function (event) {
                        var $t = $(this);
                        if ($t.hasClass("mobile-menu-submenu-closed")) {
                            $t.parent().siblings("ul").slideDown(300);
                            $t.parent().siblings(".sf-mega").slideDown(300);
                            $t.removeClass("mobile-menu-submenu-closed fa-angle-down").addClass("mobile-menu-submenu-opened fa-angle-up");
                        } else {
                            $t.parent().siblings("ul").slideUp(300);
                            $t.parent().siblings(".sf-mega").slideUp(300);
                            $t.removeClass("mobile-menu-submenu-opened fa-angle-up").addClass("mobile-menu-submenu-closed fa-angle-down");
                        }
                        event.preventDefault();
                    });

                    $("#mobile-menu li, #mobile-menu li a, #mobile-menu ul").attr("style", "");

                }

            }

        }

        /* ==========================================================================
           showHideMobileMenu
           ========================================================================== */

        function showHideMobileMenu() {

            $("#mobile-menu-trigger").click(function (event) {

                var $t = $(this);
                var $n = $("#mobile-menu");

                if ($t.hasClass("mobile-menu-opened")) {
                    $t.removeClass("mobile-menu-opened").addClass("mobile-menu-closed");
                    $n.slideUp(300);
                } else {
                    $t.removeClass("mobile-menu-closed").addClass("mobile-menu-opened");
                    $n.slideDown(300);
                }
                event.preventDefault();

            });

        }
        /* ==========================================================================
          topMenu 
         ========================================================================== */
        function MainMenu() {
            //topmenu for top-bar navigation
            topmenu.init({
                mainmenuid: "top-nav-id", //menu DIV id
                orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
                classname: 'top-nav cocaMenu', //class added to menu's outer DIV
                contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
            });
            //topmenu for top navigation
            topmenu.init({
                mainmenuid: "top-nav-id", //menu DIV id
                orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
                classname: 'top-nav cocaMenu', //class added to menu's outer DIV
                contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
            });

            // top Navigation for mobile.
            var top_nav_mobile_button = jQuery('#top-nav-mobile');
            var top_nav_cloned;
            var top_nav = jQuery('#top-nav-id > ul');

            top_nav.clone().attr('id', 'top-nav-mobile-id').removeClass().appendTo(top_nav_mobile_button);
            top_nav_cloned = top_nav_mobile_button.find('> ul');
            jQuery('#top-nav-mobile-a').click(function () {
                if (jQuery(this).hasClass('top-nav-close')) {
                    jQuery(this).removeClass('top-nav-close').addClass('top-nav-opened');
                    top_nav_cloned.slideDown(400);
                } else {
                    jQuery(this).removeClass('top-nav-opened').addClass('top-nav-close');
                    top_nav_cloned.slideUp(400);
                }
                return false;
            });
            top_nav_mobile_button.find('a').click(function (event) {
                event.stopPropagation();
            });
        }
        function init($contex) {
            flexslider();
            accordion();
            tabs();
            progress();
            animated();
            popup();
            slimMenu();
            slickNav();
            swiperSlider();



            /* ==========================================================================
               When the window is resized, do
               ========================================================================== */

            jQuery(window).resize(function () {

            });
        }
    } /* END Class */

})(jQuery);

jQuery(document).ready(function () {
    jQuery('body').ccTheme();


});

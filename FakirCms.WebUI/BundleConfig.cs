﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace FakirCms.WebUI
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bootstrap
            bundles.Add(new StyleBundle("~/content/bootstrap")
            .Include("~/Content/bootstrap.css", "~/Content/bootstrap-theme.css"));
            bundles.Add(new ScriptBundle("~/scripts/bootstrap")
                .Include("~/Scripts/bootstrap.js"));

            //jquery
            bundles.Add(new ScriptBundle("~/content/jquery").Include("~/Scripts/jquery-1.9.1.js"));
            //validate
            bundles.Add(new ScriptBundle("~/content/jquery.validate").Include("~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js"));

            //Site Bunldess
            //Theme css
            bundles.Add(new StyleBundle("~/content/css/themecss")
            .Include("~/Content/css/style.css")
            .Include("~/Content/css/responsive.css"));

            //layout
            bundles.Add(new StyleBundle("~/content/css/layout").Include("~/Content/css/layout.css"));

            //cutom css
            bundles.Add(new StyleBundle("~/content/css/custom").Include("~/Content/css/custom.css"));

            //font awesome
            bundles.Add(new StyleBundle("~/content/css/font-awesome.min.css").Include("~/Content/css/font-awesome.min.css"));
            //magnific popup
            bundles.Add(new StyleBundle("~/content/css/magnific-popup.css").Include("~//Content/css/magnific-popup.css"));
            //social likes
            bundles.Add(new ScriptBundle("~/scripts/plugins/social-likes")
                .Include("~/Scripts/plugins/social-likes.min.js"));

            bundles.Add(new StyleBundle("~/content/css/social-likes")
            .Include("~/Content/css/social-likes_flat.css"));

            //SuperFish Menu
            bundles.Add(new StyleBundle("~/content/css/superfish").Include("~/Content/css/superfish.css"));
            bundles.Add(new ScriptBundle("~/scripts/plugins/superfish").Include("~/Scripts/plugins/superfish.js"));
            //Slicknav menu
            bundles.Add(new StyleBundle("~/content/css/slicknav").Include("~/Content/css/slicknav.css"));
            bundles.Add(new ScriptBundle("~/scripts/plugins/slicknav").Include("~/Scripts/plugins/jquery.slicknav.js"));


            //Admin Bundless
            bundles.Add(new ScriptBundle("~/admin/scripts/jquery-ui").Include(
                "~/Areas/Admin/Scripts/jquery-ui-1.10.3.js"));
            //Admin LTE
            bundles.Add(new StyleBundle("~/admin/content/css/theme")
            .Include("~/Areas/Admin/Content/css/theme.css"));
            bundles.Add(new StyleBundle("~/admin/content/css/adminLTE")
                .Include("~/Areas/Admin/Content/css/AdminLTE.css"));

            bundles.Add(new ScriptBundle("~/admin/scripts/AdminLTE/app").Include("~/Areas/Admin/Scripts/AdminLTE/app.js"));
            bundles.Add(new ScriptBundle("~/admin/scripts/AdminLTE/dashboard")
            .Include("~/Areas/Admin/Scripts/AdminLTE/dashboard.js"));
            bundles.Add(new ScriptBundle("~/admin/scripts/AdminLTE/demo.js")
                .Include("~/Areas/Admin/Scripts/AdminLTE/demo"));
            //plugins

            //max-length
            bundles.Add(new ScriptBundle("~/admin/scripts/plugins/maxlength").Include("~/Areas/Admin/Scripts/plugins/maxlength/bootstrap-maxlength.js"));
            //tagsinput
            bundles.Add(new ScriptBundle("~/admin/scripts/plugins/tagsinput").Include("~/Areas/Admin/Scripts/plugins/tagsinput/bootstrap-tagsinput.js"));
            bundles.Add(new StyleBundle("~/admin/content/css/tagsinput").Include("~/Areas/Admin/Content/css/tagsinput/bootstrap-tagsinput.css"));
            //bootbox
            bundles.Add(new ScriptBundle("~/admin/scripts/plugins/bootbox").Include("~/Areas/Admin/Scripts/plugins/bootbox/bootbox.min.js"));
            //dataTables
            bundles.Add(new StyleBundle("~/admin/content/css/datatables")
                .Include("~/Areas/Admin/Content/css/datatables/dataTables.bootstrap.css")
                .Include("~/Areas/Admin/Content/css/datatables/dataTables.responsive.css"));
            bundles.Add(new ScriptBundle("~/admin/scripts/plugins/datatables")
                .Include("~/Areas/Admin/Scripts/plugins/datatables/jquery.dataTables.js")
                .Include("~/Areas/Admin/Scripts/plugins/datatables/dataTables.bootstrap.js")
                .Include("~/Areas/Admin/Scripts/plugins/datatables/dataTables.responsive.min.js"));
            //select2
            bundles.Add(new ScriptBundle("~/admin/scripts/plugins/select2")
                .Include("~/Areas/Admin/Scripts/plugins/select2/select2.full.min.js"));
            bundles.Add(new StyleBundle("~/admin/content/css/select2")
                .Include("~/Areas/Admin/Content/css/select2/select2.min.css"));
            //editable
            bundles.Add(new ScriptBundle("~/admin/scripts/plugins/editable")
                .Include("~/Areas/Admin/Scripts/plugins/editable/bootstrap-editable.js"));
            //ckeditor
            bundles.Add(new ScriptBundle("~/admin/scripts/plugins/ckeditor")
                .Include("~/Areas/Admin/Scripts/plugins/ckeditor/ckeditor.js"));


        }
    }
}
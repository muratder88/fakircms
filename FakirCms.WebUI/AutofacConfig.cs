﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using FakirCms.Domain;
using System.Web;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.Owin;
using System.Web.Routing;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using FakirCms.Domain.Entities;
using FakirCms.Domain.Manager;
using FakirCms.Domain.Services;
using FakirCms.Domain.Repositories;
using FakirCms.Domain.FileSystems.VirtualPath;
using FakirCms.Domain.FileSystems.Thumbnail;
using System.Reflection;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using FakirCms.Domain.User;
using FakirCms.Domain.Rss;

[assembly: OwinStartup(typeof(FakirCms.WebUI.AutofacConfig))]
namespace FakirCms.WebUI
{
    public class AutofacConfig
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            var builder = new ContainerBuilder();

            // REGISTER DEPENDENCIES
            builder.RegisterType<EFDbFakirCms>().AsSelf().InstancePerRequest();

            //Registers web abstractions with dependency injection.
            builder.Register(c => new HttpContextWrapper(HttpContext.Current)).As<HttpContextBase>().InstancePerRequest();

            // HttpContext properties
            builder.Register(c => c.Resolve<HttpContextBase>().Request).As<HttpRequestBase>().InstancePerRequest();

            builder.Register(c => c.Resolve<HttpRequestBase>().RequestContext).As<RequestContext>().InstancePerRequest();
            // MVC types
            builder.Register(c => new UrlHelper(c.Resolve<RequestContext>())).As<UrlHelper>().InstancePerRequest();



           //Asp.net Identity Repository registered
            builder.RegisterType<UserRepository<AppUser>>().As<IUserRepository<AppUser>>().InstancePerRequest();
            builder.RegisterType<RoleRepository>().As<IRoleRepository>().InstancePerRequest();
            builder.RegisterType<UserClaimsRepository>().As<IUserClaimsRepository>().InstancePerRequest();
            builder.RegisterType<UserLoginsRepository>().As<IUserLoginsRepository>().InstancePerRequest();
            builder.RegisterType<UserRoleRepository>().As<IUserRoleRepository>().InstancePerRequest();
            //Asp.net Identity manager registered
            builder.RegisterType<AppUserManager>().AsSelf().InstancePerRequest();
            builder.Register<IAuthenticationManager>(c => HttpContext.Current.GetOwinContext().Authentication);
            builder.RegisterType<UserStore<AppUser>>().As<IUserStore<AppUser>>().InstancePerRequest();
            builder.RegisterType<RoleStore<AppRole>>().As<IRoleStore<FakirCms.Domain.Entities.AppRole, string>>().InstancePerRequest();
            builder.RegisterType<AppRoleManager>().AsSelf().InstancePerRequest();

            //Manager Registered
            builder.RegisterType<UrlRoutingManager>().As<IUrlRoutingManager>().InstancePerRequest();

            //Services Registered
            builder.RegisterType<PostService>().As<IPostService>().InstancePerRequest();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerRequest();
            builder.RegisterType<TagService>().As<ITagService>().InstancePerRequest();
            builder.RegisterType<HaberService>().As<IHaberService>().InstancePerRequest();
            builder.RegisterType<HaberCategoryService>().As<IHaberCategoryService>().InstancePerRequest();
            builder.RegisterType<ImageEditorService>().As<IImageEditorService>().InstancePerRequest();
            builder.RegisterType<ThumbnailService>().As<IThumbnailService>().InstancePerRequest();
            builder.RegisterType<PageService>().As<IPageService>().InstancePerRequest();
            builder.RegisterType<AuthorService>().As<IAuthorService>().InstancePerRequest();
            builder.RegisterType<ArticleService>().As<IArticleService>().InstancePerRequest();

            builder.RegisterType<DataService>().As<IDataService>().InstancePerRequest();

            //Repositories Registered
            builder.RegisterType<PostRepository>().As<PostRepository>().InstancePerRequest();
            builder.RegisterType<TagRepository>().As<ITagRepository>().InstancePerRequest();
            builder.RegisterType<HaberRepository>().As<IHaberRepository>().InstancePerRequest();
            builder.RegisterType<PageRepository>().As<IPageRepository>().InstancePerRequest();
            builder.RegisterType<HaberCategoryRepository>().As<IHaberCategoryRepository>().InstancePerRequest();
            builder.RegisterType<AuthorRepository>().As<IAuthorRepository>().InstancePerRequest();
            builder.RegisterType<ArticleRepository>().As<IArticleRepository>().InstancePerRequest();


            //FileSystem Registered
            builder.RegisterType<DefaultVirtualPathProvider>().As<IVirtualPathProvider>().InstancePerRequest();
            builder.RegisterType<ThumbnailFolder>().As<IThumbnailFolder>().InstancePerRequest();
            builder.RegisterType<ThumbnailFolderRoot>().As<IThumbnailFolderRoot>().InstancePerRequest();


            //Controller registered
            builder.RegisterControllers(Assembly.GetExecutingAssembly());


            // BUILD THE CONTAINER
            var container = builder.Build();

            // REPLACE THE MVC DEPENDENCY RESOLVER WITH AUTOFAC
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // REGISTER WITH OWIN
            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();

            ConfigureAuth(app);

        }
        public void ConfigureAuth(IAppBuilder app)
        {
            //app.CreatePerOwinContext<EFDbNewsWebSite>(EFDbNewsWebSite.Create);
            //app.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });
        }
    }
}

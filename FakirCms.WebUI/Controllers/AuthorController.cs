﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakirCms.Domain.Services;
using FakirCms.Domain.Entities;
using FakirCms.Domain.Models;

namespace FakirCms.WebUI.Controllers
{
    public class AuthorController : Controller
    {
        private IAuthorService _authorService;
        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }
        public ActionResult Index()
        {
            List<Author> authorlist = _authorService.Items.ToList();
            List<AuthorDetail> detailedAuthorList = _authorService.GetAuthorDetailList(authorlist); 

            return View(detailedAuthorList);
        }
	}
}
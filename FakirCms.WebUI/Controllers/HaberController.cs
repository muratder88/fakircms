﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Models;
using FakirCms.Domain.Services;
using FakirCms.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace FakirCms.WebUI.Controllers
{
    public class HaberController : Controller
    {
        private IHaberService _haberService;
        private ITagService _tagService;
        private IHaberCategoryService _haberCategoryService;
        public HaberController(IHaberService haberService, ITagService tagService,
            IHaberCategoryService haberCategoryService)
        {
            _haberService = haberService;
            _tagService = tagService;
            _haberCategoryService = haberCategoryService;
        }

        public ActionResult HaberCategory(string alias,int page = 1)
        {
            if (string.IsNullOrEmpty(alias))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            HaberCategory category = _haberCategoryService.FindByAlias(alias);
            if (category == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var haberler = _haberService.GetPublishedCategoryHabersByCategoryAlias(alias);
            var OnePageOfHabers = haberler.ToPagedList(page, 20); // will only contain 25 products max because of the pageSize
            //this is for paging IPagedLis<Haber> required
            ViewBag.OnePageOfHabers = OnePageOfHabers;
            List<HaberDetail> onePageDetailedHaberList = new List<HaberDetail>();
            OnePageOfHabers.ToList().ForEach(haber =>
            {
                HaberDetail haberdetail = _haberService.GetHaberDetails(haber);
                onePageDetailedHaberList.Add(haberdetail);
            });
            HaberCategoryView model = new HaberCategoryView()
            {
                Haberler = onePageDetailedHaberList,
                Category=category
            };
            return View(model);
        }


        public ActionResult Detail(string alias, int id)
        {
            Haber haber = _haberService.FindById(id);
            if (haber == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            HaberDetailView model = new HaberDetailView();
            HaberDetail detailedHaber = _haberService.GetHaberDetails(haber);
            model.Haber = detailedHaber;

            Haber nextHaber = _haberService.GetNextHaber(haber);
            model.NextHaber = (nextHaber == null) ? null : _haberService.GetHaberDetails(nextHaber);

            Haber prevHaber = _haberService.GetPreviousHaber(haber);
            model.PrevHaber = (prevHaber == null) ? null : _haberService.GetHaberDetails(prevHaber);

            //get relatedHabers
            List<Haber> relatedHabers = _haberService.GetRelatedHabers(haber);
            List<HaberDetail> detailedRelatedHaberList = new List<HaberDetail>();
            if (relatedHabers != null)
            {
                relatedHabers.ForEach(h =>
                {
                    detailedRelatedHaberList.Add(_haberService.GetHaberDetails(h));
                });
            }
            model.RelatedHabers = detailedRelatedHaberList;
            //ıncrease hits
            _haberService.IncreaseHits(haber.Id);
            return View(model);
        }
        public ActionResult Tag(string tagalias, int pagenumber = 1)
        {
            if (string.IsNullOrEmpty(tagalias))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Tag tag = _tagService.FindByAlias(tagalias);
            if (tag == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            var tagHabers = _haberService.FindAllHabersByTagAlias(tagalias);
            var OnePageOfHabers = tagHabers.ToPagedList(pagenumber, 20);
            //this is for paging IPagedLis<Haber> required
            ViewBag.OnePageOfHabers = OnePageOfHabers;

            List<HaberDetail> onePageDetailedHaberList = new List<HaberDetail>();
            OnePageOfHabers.ToList().ForEach(haber =>
            {
                HaberDetail haberdetail = _haberService.GetHaberDetails(haber);
                onePageDetailedHaberList.Add(haberdetail);
            });

            ViewBag.Alias = tagalias;
            ViewBag.Name = tag.Name;
            return View(onePageDetailedHaberList);
        }
	}
}
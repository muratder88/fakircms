﻿using FakirCms.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using FakirCms.Domain.Entities;
using FakirCms.WebUI.Models;

namespace FakirCms.WebUI.Controllers
{
    public class ArticleController : Controller
    {
        private IAuthorService _authorService;
        private IArticleService _articleService;
        public ArticleController(IAuthorService authorService,IArticleService articleService)
        {
            _authorService = authorService;
            _articleService = articleService;
        }
        public ActionResult Index(string yazar,int page=1)
        {
            if(string.IsNullOrEmpty(yazar))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Author author = _authorService.FindByAlias(yazar);
            if (author == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            List<Article> articles=_articleService.GetPublishedArticles(author).ToList();

            
            ArticleIndexModel model = new ArticleIndexModel()
            {
                Articles=_articleService.GetArticleDetailList(articles),
                Author=_authorService.GetAuthorDetail(author)
            };
            return View(model);
        }

        public ActionResult Detail(string yazar,string alias,int id)
        {
            if (string.IsNullOrEmpty(yazar) || string.IsNullOrEmpty(alias))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Article article = _articleService.GetPublishedAuthorArticle(yazar, id);
            
            if (article == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            ArtilceDetailModel model = new ArtilceDetailModel()
            {
                Author = article.Author,
                Article = article,
            };
            return View(model);
        }
	}
}
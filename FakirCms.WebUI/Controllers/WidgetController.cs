﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Models;
using FakirCms.Domain.Services;
using FakirCms.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Controllers
{
    public class WidgetController : Controller
    {
        private IHaberService _haberService;
        private IAuthorService _authorService;
        public WidgetController(IHaberService haberService,IAuthorService authorService)
        {
            _haberService = haberService;
            _authorService = authorService;
        }


        public PartialViewResult TopHabers()
        {
            List<Haber> popularHabers = _haberService.GetPopularHabers().Take(6).ToList();
            List<Haber> recentHabers = _haberService.GetRecentHabers().Take(6).ToList();
            PopularHaberModel model = new PopularHaberModel()
            {
                Popular=_haberService.GetHaberDetailList(popularHabers),
                Recent=_haberService.GetHaberDetailList(recentHabers)
            };
            return PartialView(model);
        }

        public PartialViewResult Authors()
        {
            List<Author> authors = _authorService.Items.Take(6).ToList();
            List<AuthorDetail> detailedAuthor = _authorService.GetAuthorDetailList(authors);

            return PartialView(detailedAuthor);
        }

	}
}
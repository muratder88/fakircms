﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Controllers
{
    public class PageController : Controller
    {
       private IPageService _pageService;
        public PageController(IPageService pageService)
        {
            _pageService = pageService;
        }

        public ActionResult Index(string alias)
        {
           Page page= _pageService.FindByAlias(alias);
           if (page == null)
               return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
           if (!page.State)
               return new HttpStatusCodeResult(HttpStatusCode.NotFound);

           _pageService.IncreaseHits(page.Id);
            return View(page);
        }
	}
	
}
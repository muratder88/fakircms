﻿using FakirCms.Domain.Entities;
using FakirCms.Domain.Models;
using FakirCms.Domain.Services;
using FakirCms.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FakirCms.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private IHaberService _haberService;
        

        public HomeController(IHaberService haberService)
        {
            _haberService = haberService;
           
        }
        public ActionResult Index()
        {
            List<Haber> haberler = _haberService.items.Where(x => x.State == true).OrderByDescending(x => x.Date).Take(16).ToList();
            List<Haber> mansetHaberler = _haberService.GetFrontPageMansetHabers().Take(15).ToList();
            List<Haber> mainHabers = _haberService.GetFrontPageMainHabers().Take(9).ToList();
            List<Haber> altHabers = _haberService.GetFrontPageAltHabers().Take(5).ToList();
            List<Haber> ustHabers = _haberService.GetFrontPageUstHabers().Take(5).ToList();
            
            HomeIndexView model = new HomeIndexView()
            {
                Haberler =_haberService.GetHaberDetailList(haberler),
                MansetHaberler=_haberService.GetHaberDetailList(mansetHaberler),
                MainHaberler=_haberService.GetHaberDetailList(mainHabers),
                AltHaberler=_haberService.GetHaberDetailList(altHabers),
                UstHaberler=_haberService.GetHaberDetailList(ustHabers)
            };
            return View(model);
        }
        public ActionResult Robots()
        {
            Response.ContentType = "text/plain";
            return View();
        }
	}
}